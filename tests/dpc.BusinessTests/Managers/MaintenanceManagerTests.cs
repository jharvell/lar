﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using dpc.Data;
using System.Collections.Generic;
using dpc.Data.Interfaces;
using Moq;
using System.Linq.Expressions;
using dpc.Business;
using System.Linq;

namespace dpc.BusinessTests.Managers
{
	[TestClass]
	public class MaintenanceManagerTests
	{
		#region Brand

		Mock<IBrandRepository> mockBrandRepository;
		Mock<UnitOfWork> mockUnitOfWork;
		MaintenanceManager manager;

		[TestInitialize()]
		public void Setup()
		{
			mockBrandRepository = new Mock<IBrandRepository>();

			mockUnitOfWork = new Mock<UnitOfWork>();
			mockUnitOfWork.SetupGet(u => u.BrandRepository).Returns(mockBrandRepository.Object);

			manager = new MaintenanceManager(mockUnitOfWork.Object);
		}

		[TestMethod()]
		public void GetBrandById_ValidParam_ReturnsString()
		{
			//Arrange
			int id = 1;
			Brand brand = new Brand();
			brand.Description = "Test";
			
			mockBrandRepository.Setup(c => c.GetByID(It.IsAny<int>())).Returns(brand);

			//Act
			var result = manager.GetBrandById(id);

			//Assert
			Assert.AreEqual(result.Description, brand.Description);
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentException))]
		public void GetBrandById_InvalidParam_ReturnsArgException()
		{
			//Arrange  
			int id = 0;
			Brand brand = new Brand();
			
			mockBrandRepository.Setup(c => c.GetByID(It.IsAny<int>())).Returns(brand);

			//Act
			manager.GetBrandById(id);

			//Assert
			//Not Needed
		}

		[TestMethod()]
		public void InsertBrand_ValidParam_ReturnsNewObject()
		{
			//Arrange  
			int id = 2;
			var brand = new Brand() { BrandOwnerId = 1, Description = "test", Abbreviation = "t", UpdatedOn = DateTime.Now, UpdatedBy = "ddavis" };
			
			mockBrandRepository.Setup(c => c.Insert(brand)).Returns((Brand e) =>
			{
				e.BrandId = id;
				return e;
			});

			//Act
			var newInt = manager.InsertBrand(brand);

			//Assert
			Assert.AreEqual(id, newInt);
			mockUnitOfWork.Verify(x => x.Save());
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentException))]
		public void InsertBrand_InvalidParam_ReturnsArgException()
		{
			//Arrange  
			Brand brand = null;
			
			mockBrandRepository.Setup(x => x.Update(It.IsAny<Brand>()));

			//Act
			manager.InsertBrand(brand);

			//Assert
			//Not Needed
		}

		[TestMethod()]
		public void UpdateBrand_ValidParam_ReturnsSuccess()
		{
			//Arrange  
			var brand = new Brand();
			
			mockBrandRepository.Setup(x => x.Update(It.IsAny<Brand>()));

			//Act
			manager.UpdateBrand(brand);

			//Assert
			mockBrandRepository.Verify(x => x.Update(brand));
			mockUnitOfWork.Verify(x => x.Save());
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentException))]
		public void UpdateBrand_InvalidParam_ReturnsArgException()
		{
			//Arrange  
			Brand brand = null;
			
			mockBrandRepository.Setup(x => x.Update(It.IsAny<Brand>()));

			//Act
			manager.UpdateBrand(brand);

			//Assert
			//Not Needed
		}

		[TestMethod]
		public void DeleteBrand_ValidParam_ReturnsSuccess()
		{
			//Arrange
			int id = 1;
			
			mockBrandRepository.Setup(r => r.Delete(It.IsAny<int>()));

			//Act
			manager.DeleteBrand(id);

			//Assert
			mockBrandRepository.Verify(x => x.Delete(id));
			mockUnitOfWork.Verify(x => x.Save());
		}

		[TestMethod()]
		[ExpectedException(typeof(ArgumentException))]
		public void DeleteBrand_InvalidParam_ReturnsArgException()
		{
			//Arrange  
			int id = 0;
			
			mockBrandRepository.Setup(r => r.Delete(It.IsAny<int>()));

			//Act
			manager.DeleteBrand(id);

			//Assert
			//Not Needed
		}

		[TestMethod()]
		public void GetBrandList_ValidObject_ReturnsValidList()
		{
			//Arrange 
			var returnList = new List<Brand>();
			returnList.Add(new Brand { BrandId = 3, Description = "test3" });
			returnList.Add(new Brand { BrandId = 2, Description = "test2" });
			returnList.Add(new Brand { BrandId = 1, Description = "test1" });
			
			mockBrandRepository.Setup(c => c.Get(It.IsAny<Expression<Func<Brand, bool>>>(), null, "")).Returns(returnList);

			//Act
			var result = manager.GetBrandList();

			//Assert
			mockBrandRepository.Verify(x => x.Get(It.IsAny<Expression<Func<Brand, bool>>>(), null, ""), Times.Exactly(1));
			Assert.AreEqual(3, result.Count());
			Assert.AreEqual("test1", result.First().Description);
			Assert.AreEqual("1", result.First().Value);
		}

		#endregion Brand
	}
}
