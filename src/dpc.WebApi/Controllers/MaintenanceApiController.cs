﻿using dpc.Business;
using dpc.Business.Interfaces;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using dpc.Data;
using dpc.Data.CustomModels;
using dpc.Web.ViewModel;
using System.Web;
using System;

namespace dpc.WebApi
{
	public class MaintenanceApiController : ApiController
	{

		private readonly IMaintenanceManager _maintenanceManager;

		#region constructors

		public MaintenanceApiController()
		{
			_maintenanceManager = new MaintenanceManager(new UnitOfWork());
		}

		public MaintenanceApiController(IMaintenanceManager maintenanceManager)
		{
			_maintenanceManager = maintenanceManager;
		}

		#endregion constructors

		#region Brand

		[HttpPost]
		[ActionName("PostInsertBrand")]
		public HttpResponseMessage PostInsertBrand([FromBody] BrandViewModel data)
		{
			if (_maintenanceManager.isBrandExist(data.BrandOwnerId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Brand already exists");
			}

			var vmBrand = new BrandViewModel();
			try
			{
				var brand = new Brand();
				brand.BrandOwnerId = data.BrandOwnerId;
				brand.Description = data.Description;
				brand.Abbreviation = data.Abbreviation;
				brand.UpdatedOn = DateTime.Now;
				//TODO: Jacek testing ControllerContext.RequestContext.Principal.Identity.Name
				brand.UpdatedBy = ControllerContext.RequestContext.Principal.Identity.Name; // HttpContext.Current.User.Identity.Name;

				_maintenanceManager.InsertBrand(brand);

				vmBrand.BrandId = brand.BrandId;
			}
			catch (Exception ex)
			{
				//TODO: Add try/catch to ApiControllers
				//TODO: log error with Elmah - see Jacek to make generic

				//return to .js
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, ex);
			}

			return Request.CreateResponse(HttpStatusCode.OK, vmBrand);
		}

		[HttpPost]
		[ActionName("PostUpdateBrand")]
		public HttpResponseMessage PostUpdateBrand([FromBody] BrandViewModel data)
		{
			if (_maintenanceManager.isBrandExist(data.BrandOwnerId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Brand already exists");
			}

			try
			{
				var brand = _maintenanceManager.GetBrandById(data.BrandId);
				brand.BrandId = data.BrandId;
				brand.BrandOwnerId = data.BrandOwnerId;
				brand.Description = data.Description;
				brand.Abbreviation = data.Abbreviation;
				brand.UpdatedOn = DateTime.Now;
				brand.UpdatedBy = ControllerContext.RequestContext.Principal.Identity.Name; // HttpContext.Current.User.Identity.Name;  //TODO: Jacek testing!

				_maintenanceManager.UpdateBrand(brand);
			}
			catch (Exception ex)
			{
				//return to .js
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, ex);
			}

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteBrand")]
		public HttpResponseMessage PostDeleteBrand([FromBody] BrandViewModel data)
		{
			if (_maintenanceManager.isBrandInProgramLine(data.BrandId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Brand exists in a Program Line");
			}

			try
			{
				_maintenanceManager.DeleteBrand(data.BrandId);
			}
			catch (Exception ex)
			{
				//return to .js
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, ex);
			}

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion Brand

		#region BrandOwner

		[HttpPost]
		[ActionName("PostInsertBrandOwner")]
		public HttpResponseMessage PostInsertBrandOwner([FromBody] BrandOwnerViewModel data)
		{
			if (_maintenanceManager.isBrandOwnerExist(data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Brand Owner already exists");
			}

			var brandOwner = new BrandOwner();
			brandOwner.Description = data.Description;
			brandOwner.Abbreviation = data.Abbreviation;
			brandOwner.UpdatedOn = DateTime.Now;
			brandOwner.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertBrandOwner(brandOwner);

			var vmBrandOwner = new BrandOwnerViewModel();
			vmBrandOwner.BrandOwnerId = brandOwner.BrandOwnerId;

			return Request.CreateResponse(HttpStatusCode.OK, vmBrandOwner);
		}

		[HttpPost]
		[ActionName("PostUpdateBrandOwner")]
		public HttpResponseMessage PostUpdateBrandOwner([FromBody] BrandOwnerViewModel data)
		{
			if (_maintenanceManager.isBrandOwnerExist(data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Brand Owner already exists");
			}

			var brandOwner = _maintenanceManager.GetBrandOwnerById(data.BrandOwnerId);
			brandOwner.BrandOwnerId = data.BrandOwnerId;
			brandOwner.Description = data.Description;
			brandOwner.Abbreviation = data.Abbreviation;
			brandOwner.UpdatedOn = DateTime.Now;
			brandOwner.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateBrandOwner(brandOwner);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteBrandOwner")]
		public HttpResponseMessage PostDeleteBrandOwner([FromBody] BrandOwnerViewModel data)
		{
			if (_maintenanceManager.isBrandOwnerInProgramHeader(data.BrandOwnerId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Brand Owner exists in a Program Header");
			}
			if (_maintenanceManager.isBrandOwnerInBrand(data.BrandOwnerId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Brand Owner exists in Brand");
			}

			_maintenanceManager.DeleteBrandOwner(data.BrandOwnerId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion BrandOwner

		#region ContainerType

		[HttpPost]
		[ActionName("PostInsertContainerType")]
		public HttpResponseMessage PostInsertContainerType([FromBody] ContainerTypeViewModel data)
		{
			if (_maintenanceManager.isContainerTypeExist(data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Container Type already exists");
			}

			var containerType = new ContainerType();
			containerType.Description = data.Description;
			containerType.Abbreviation = data.Abbreviation;
			containerType.UpdatedOn = DateTime.Now;
			containerType.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertContainerType(containerType);

			var vmContainerType = new ContainerTypeViewModel();
			vmContainerType.ContainerTypeId = containerType.ContainerTypeId;

			return Request.CreateResponse(HttpStatusCode.OK, vmContainerType);
		}

		[HttpPost]
		[ActionName("PostUpdateContainerType")]
		public HttpResponseMessage PostUpdateContainerType([FromBody] ContainerTypeViewModel data)
		{
			if (_maintenanceManager.isContainerTypeExist(data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Container Type already exists");
			}

			var containerType = _maintenanceManager.GetContainerTypeById(data.ContainerTypeId);
			containerType.ContainerTypeId = data.ContainerTypeId;
			containerType.Description = data.Description;
			containerType.Abbreviation = data.Abbreviation;
			containerType.UpdatedOn = DateTime.Now;
			containerType.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateContainerType(containerType);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteContainerType")]
		public HttpResponseMessage PostDeleteContainerType([FromBody] ContainerTypeViewModel data)
		{
			if (_maintenanceManager.isContainerTypeInPackagingType(data.ContainerTypeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to delete, Container Type used in Packaging Type");
			}
			if (_maintenanceManager.isContainerTypeInQuantityRange(data.ContainerTypeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to delete, Container Type used in Quantity Range");
			}

			_maintenanceManager.DeleteContainerType(data.ContainerTypeId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion ContainerType

		#region Feature

		[HttpPost]
		[ActionName("PostInsertFeature")]
		public HttpResponseMessage PostInsertFeature([FromBody] FeatureViewModel data)
		{
			if (_maintenanceManager.isFeatureExist(data.ProductCategoryId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Feature already exists");
			}

			var feature = new Feature();
			feature.ProductCategoryId = data.ProductCategoryId;
			feature.Description = data.Description;
			feature.Abbreviation = data.Abbreviation;
			feature.UpdatedOn = DateTime.Now;
			feature.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertFeature(feature);

			var vmFeature = new FeatureViewModel();
			vmFeature.FeatureId = feature.FeatureId;

			return Request.CreateResponse(HttpStatusCode.OK, vmFeature);
		}

		[HttpPost]
		[ActionName("PostUpdateFeature")]
		public HttpResponseMessage PostUpdateFeature([FromBody] FeatureViewModel data)
		{
			if (_maintenanceManager.isFeatureExist(data.ProductCategoryId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Feature already exists");
			}

			var feature = _maintenanceManager.GetFeatureById(data.FeatureId);
			feature.FeatureId = data.FeatureId;
			feature.ProductCategoryId = data.ProductCategoryId;
			feature.Description = data.Description;
			feature.Abbreviation = data.Abbreviation;
			feature.UpdatedOn = DateTime.Now;
			feature.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateFeature(feature);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteFeature")]
		public HttpResponseMessage PostDeleteFeature([FromBody] FeatureViewModel data)
		{
			if (_maintenanceManager.isFeatureInProductTypeToFeature(data.FeatureId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Feature used in Feature Option");
			}

			_maintenanceManager.DeleteFeature(data.FeatureId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion Feature

		#region FeatureToOption

		[HttpPost]
		[ActionName("PostInsertFeatureToOption")]
		public HttpResponseMessage PostInsertFeatureToOption([FromBody] FeatureToOptionViewModel data)
		{
			if (_maintenanceManager.isFeatureToOptionExist(data.ProductTypeId, data.FeatureId, data.Description))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Feature Option already exists");
			}

			var featureToOption = new FeatureToOptionModel();
			featureToOption.ProductTypeId = data.ProductTypeId;
			featureToOption.FeatureId = data.FeatureId;
			featureToOption.Description = data.Description;
			featureToOption.UpdatedOn = DateTime.Now;
			featureToOption.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertFeatureToOption(featureToOption);

			var vmFeatureToOption = new FeatureToOptionViewModel();
			vmFeatureToOption.FeatureToOptionId = featureToOption.FeatureToOptionId;

			return Request.CreateResponse(HttpStatusCode.OK, vmFeatureToOption);
		}

		[HttpPost]
		[ActionName("PostUpdateFeatureToOption")]
		public HttpResponseMessage PostUpdateFeatureToOption([FromBody] FeatureToOptionViewModel data)
		{
			if (_maintenanceManager.isFeatureToOptionExist(data.ProductTypeId, data.FeatureId, data.Description))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Feature Option already exists");
			}

			var featureToOption = _maintenanceManager.GetFeatureToOptionById(data.FeatureToOptionId);
			featureToOption.FeatureToOptionId = data.FeatureToOptionId;
			featureToOption.Description = data.Description;
			featureToOption.UpdatedOn = DateTime.Now;
			featureToOption.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateFeatureToOption(featureToOption);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteFeatureToOption")]
		public HttpResponseMessage PostDeleteFeatureToOption([FromBody] FeatureToOptionViewModel data)
		{
			if (_maintenanceManager.isFeatureToOptionInProductSizeToFeatureToOptionCost(data.FeatureToOptionId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Feature Option used in Option Cost");
			}

			_maintenanceManager.DeleteFeatureToOption(data.FeatureToOptionId);  //Delete from main table

			//Delete ProductTypeToFeature from many-to-many table if there are no FeatureToOption linked
			if (!_maintenanceManager.isProductTypeToFeatureExist(data.ProductTypeToFeatureId))
			{
				_maintenanceManager.DeleteProductTypeToFeature(data.ProductTypeToFeatureId);  //Delete from many-to-many table
			}

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion FeatureToOption

		#region MfgCost

		[HttpPost]
		[ActionName("PostInsertMfgCost")]
		public HttpResponseMessage PostInsertMfgCost([FromBody] MfgCostViewModel data)
		{
			if (_maintenanceManager.isMfgCostExist(data.ProductTypeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, MfgCost already exists");
			}

			var mfgCost = new MfgCost();
			mfgCost.ProductTypeId = data.ProductTypeId;
			mfgCost.VLandOverhead = data.VLandOverhead;
			mfgCost.FixedConversionCost = data.FixedConversionCost;
			mfgCost.Depreciation = data.Depreciation;
			mfgCost.Scrap = data.Scrap;
			mfgCost.UpdatedOn = DateTime.Now;
			mfgCost.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertMfgCost(mfgCost);

			var vmMfgCost = new MfgCostViewModel();
			vmMfgCost.MfgCostId = mfgCost.MfgCostId;

			return Request.CreateResponse(HttpStatusCode.OK, vmMfgCost);
		}

		[HttpPost]
		[ActionName("PostUpdateMfgCost")]
		public HttpResponseMessage PostUpdateMfgCost([FromBody] MfgCostViewModel data)
		{
			var mfgCost = _maintenanceManager.GetMfgCostById(data.MfgCostId);
			mfgCost.MfgCostId = data.MfgCostId;
			mfgCost.ProductTypeId = data.ProductTypeId;
			mfgCost.VLandOverhead = data.VLandOverhead;
			mfgCost.FixedConversionCost = data.FixedConversionCost;
			mfgCost.Depreciation = data.Depreciation;
			mfgCost.Scrap = data.Scrap;
			mfgCost.UpdatedOn = DateTime.Now;
			mfgCost.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateMfgCost(mfgCost);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteMfgCost")]
		public HttpResponseMessage PostDeleteMfgCost([FromBody] MfgCostViewModel data)
		{
			_maintenanceManager.DeleteMfgCost(data.MfgCostId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion MfgCost

		#region PackagingCost
		[HttpPost]
		[ActionName("PostInsertPackagingCost")]
		public HttpResponseMessage PostInsertPackagingCost([FromBody] PackagingCostViewModel data)
		{
			if (_maintenanceManager.isPackagingCostExist(data.ProductSizeId, data.PackagingTypeId, data.QuantityRangeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Bag Cost already exists");
			}

			var packagingCost = new PackagingCost();
			packagingCost.PackagingCostId = data.PackagingCostId;
			packagingCost.ProductSizeId = data.ProductSizeId;
			packagingCost.PackagingTypeId = data.PackagingTypeId;
			packagingCost.QuantityRangeId = data.QuantityRangeId;
			packagingCost.Cost = data.Cost;
			packagingCost.UpdatedOn = DateTime.Now;
			packagingCost.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertPackagingCost(packagingCost);

			var vmPackagingCost = new PackagingCostViewModel();
			vmPackagingCost.PackagingCostId = packagingCost.PackagingCostId;

			return Request.CreateResponse(HttpStatusCode.OK, vmPackagingCost);
		}

		[HttpPost]
		[ActionName("PostUpdatePackagingCost")]
		public HttpResponseMessage PostUpdatePackagingCost([FromBody] PackagingCostViewModel data)
		{
			var packagingCost = _maintenanceManager.GetPackagingCostById(data.PackagingCostId);
			packagingCost.PackagingCostId = data.PackagingCostId;
			packagingCost.ProductSizeId = data.ProductSizeId;
			packagingCost.PackagingTypeId = data.PackagingTypeId;
			packagingCost.QuantityRangeId = data.QuantityRangeId;
			packagingCost.Cost = data.Cost;

			_maintenanceManager.UpdatePackagingCost(packagingCost);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeletePackagingCost")]
		public HttpResponseMessage PostDeletePackagingCost([FromBody] PackagingCostViewModel data)
		{
			_maintenanceManager.DeletePackagingCost(data.PackagingCostId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion PackagingCost

		#region PackagingType

		[HttpPost]
		[ActionName("PostInsertPackagingType")]
		public HttpResponseMessage PostInsertPackagingType([FromBody] PackagingTypeViewModel data)
		{
			if (_maintenanceManager.isPackagingTypeExist(data.ContainerTypeId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Packaging Type already exists");
			}

			var packagingType = new PackagingType();
			packagingType.ContainerTypeId = data.ContainerTypeId;
			packagingType.Description = data.Description;
			packagingType.Abbreviation = data.Abbreviation;
			packagingType.UpdatedOn = DateTime.Now;
			packagingType.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertPackagingType(packagingType);

			var vmPackagingType = new PackagingTypeViewModel();
			vmPackagingType.PackagingTypeId = packagingType.PackagingTypeId;

			return Request.CreateResponse(HttpStatusCode.OK, vmPackagingType);
		}

		[HttpPost]
		[ActionName("PostUpdatePackagingType")]
		public HttpResponseMessage PostUpdatePackagingType([FromBody] PackagingTypeViewModel data)
		{
			if (_maintenanceManager.isPackagingTypeExist(data.ContainerTypeId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Packaging Type already exists");
			}

			var packagingType = _maintenanceManager.GetPackagingTypeById(data.PackagingTypeId);
			packagingType.PackagingTypeId = data.PackagingTypeId;
			packagingType.ContainerTypeId = data.ContainerTypeId;
			packagingType.Description = data.Description;
			packagingType.Abbreviation = data.Abbreviation;

			_maintenanceManager.UpdatePackagingType(packagingType);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeletePackagingType")]
		public HttpResponseMessage PostDeletePackagingType([FromBody] PackagingTypeViewModel data)
		{
			if (_maintenanceManager.isPackagingTypeInPackagingCost(data.PackagingTypeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Packaging Type used in Packaging Cost");
			}

			_maintenanceManager.DeletePackagingType(data.PackagingTypeId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion PackagingType

		#region PackStyle
		[HttpPost]
		[ActionName("PostInsertPackStyle")]
		public HttpResponseMessage PostInsertPackStyle([FromBody] PackStyleViewModel data)
		{
			if (_maintenanceManager.isPackStyleExist(data.ProductTypeId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Pack Style already exists");
			}

			var packStyle = new PackStyle();
			packStyle.ProductTypeId = data.ProductTypeId;
			packStyle.Description = data.Description;
			packStyle.Abbreviation = data.Abbreviation;
			packStyle.UpdatedOn = DateTime.Now;
			packStyle.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertPackStyle(packStyle);

			var vmPackStyle = new PackStyleViewModel();
			vmPackStyle.PackStyleId = packStyle.PackStyleId;

			return Request.CreateResponse(HttpStatusCode.OK, vmPackStyle);
		}

		[HttpPost]
		[ActionName("PostUpdatePackStyle")]
		public HttpResponseMessage PostUpdatePackStyle([FromBody] PackStyleViewModel data)
		{
			if (_maintenanceManager.isPackStyleExist(data.ProductTypeId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Pack Style already exists");
			}

			PackStyle packStyle = _maintenanceManager.GetPackStyleById(data.PackStyleId);
			packStyle.PackStyleId = data.PackStyleId;
			packStyle.ProductTypeId = data.ProductTypeId;
			packStyle.Description = data.Description;
			packStyle.Abbreviation = data.Abbreviation;
			packStyle.UpdatedOn = DateTime.Now;
			packStyle.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdatePackStyle(packStyle);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeletePackStyle")]
		public HttpResponseMessage PostDeletePackStyle([FromBody] PackStyleViewModel data)
		{
			if (_maintenanceManager.isPackStyleInProgramLine(data.PackStyleId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Pack Style exists in a Program Line");
			}

			_maintenanceManager.DeletePackStyle(data.PackStyleId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion PackStyle

		#region ProductCategory

		[HttpPost]
		[ActionName("PostInsertProductCategory")]
		public HttpResponseMessage PostInsertProductCategory([FromBody] ProductCategoryViewModel data)
		{
			if (_maintenanceManager.isProductCategoryExist(data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Product Category already exists");
			}

			var productCategory = new ProductCategory();
			productCategory.Description = data.Description;
			productCategory.Abbreviation = data.Abbreviation;
			productCategory.UpdatedOn = DateTime.Now;
			productCategory.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertProductCategory(productCategory);

			var vmProductCategory = new ProductCategoryViewModel();
			vmProductCategory.ProductCategoryId = productCategory.ProductCategoryId;

			return Request.CreateResponse(HttpStatusCode.OK, vmProductCategory);
		}

		[HttpPost]
		[ActionName("PostUpdateProductCategory")]
		public HttpResponseMessage PostUpdateProductCategory([FromBody] ProductCategoryViewModel data)
		{
			if (_maintenanceManager.isProductCategoryExist(data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Product Category already exists");
			}

			var productCategory = _maintenanceManager.GetProductCategoryById(data.ProductCategoryId);
			productCategory.ProductCategoryId = data.ProductCategoryId;
			productCategory.Description = data.Description;
			productCategory.Abbreviation = data.Abbreviation;
			productCategory.UpdatedOn = DateTime.Now;
			productCategory.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateProductCategory(productCategory);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteProductCategory")]
		public HttpResponseMessage PostDeleteProductCategory([FromBody] ProductCategoryViewModel data)
		{
			if (_maintenanceManager.isProductCategoryInProductType(data.ProductCategoryId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to delete, Product Category used in Product Type");
			}
			if (_maintenanceManager.isProductCategoryInFeature(data.ProductCategoryId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to delete, Product Category used in Feature");
			}
			if (_maintenanceManager.isProductCategoryInQuantityRange(data.ProductCategoryId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to delete, Product Category used in Quantity Range");
			}

			_maintenanceManager.DeleteProductCategory(data.ProductCategoryId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion ProductCategory

		#region ProductSize

		[HttpPost]
		[ActionName("PostInsertProductSize")]
		public HttpResponseMessage PostInsertProductSize([FromBody] ProductSizeViewModel data)
		{
			if (_maintenanceManager.isProductSizeExist(data.ProductTypeId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Product Size already exists");
			}

			var productSize = new ProductSize();
			productSize.ProductTypeId = data.ProductTypeId;
			productSize.Description = data.Description;
			productSize.Abbreviation = data.Abbreviation;
			productSize.UpdatedOn = DateTime.Now;
			productSize.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertProductSize(productSize);

			var vmProductSize = new ProductSizeViewModel();
			vmProductSize.ProductSizeId = productSize.ProductSizeId;

			return Request.CreateResponse(HttpStatusCode.OK, vmProductSize);
		}

		[HttpPost]
		[ActionName("PostUpdateProductSize")]
		public HttpResponseMessage PostUpdateProductSize([FromBody] ProductSizeViewModel data)
		{
			if (_maintenanceManager.isProductSizeExist(data.ProductTypeId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Product Size already exists");
			}

			var productSize = _maintenanceManager.GetProductSizeById(data.ProductSizeId);
			productSize.ProductSizeId = data.ProductSizeId;
			productSize.ProductTypeId = data.ProductTypeId;
			productSize.Description = data.Description;
			productSize.Abbreviation = data.Abbreviation;
			productSize.UpdatedOn = DateTime.Now;
			productSize.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateProductSize(productSize);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteProductSize")]
		public HttpResponseMessage PostDeleteProductSize([FromBody] ProductSizeViewModel data)
		{
			if (_maintenanceManager.isProductSizeInPackagingCost(data.ProductSizeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Product Size used in Packaging Cost");
			}
			if (_maintenanceManager.isProducSizeInProductSizeToFeatureToOptionCost(data.ProductSizeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Product Size used in Option Cost");
			}

			_maintenanceManager.DeleteProductSize(data.ProductSizeId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion ProductSize

		#region ProductSizeToFeatureToOptionCost
		[HttpPost]
		[ActionName("PostInsertProductSizeToFeatureToOptionCost")]
		public HttpResponseMessage PostInsertProductSizeToFeatureToOptionCost([FromBody] ProductSizeToFeatureToOptionCostViewModel data)
		{
			if (_maintenanceManager.isProductSizeToFeatureToOptionCostExist(data.ProductSizeId, data.FeatureToOptionId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Option Cost already exists");
			}

			var productSizeToFeatureToOptionCost = new ProductSizeToFeatureToOptionCost();
			productSizeToFeatureToOptionCost.ProductSizeId = data.ProductSizeId;
			productSizeToFeatureToOptionCost.FeatureToOptionId = data.FeatureToOptionId;
			productSizeToFeatureToOptionCost.Cost = data.Cost;
			productSizeToFeatureToOptionCost.UpdatedOn = DateTime.Now;
			productSizeToFeatureToOptionCost.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertProductSizeToFeatureToOptionCost(productSizeToFeatureToOptionCost);

			var vmProductSizeToFeatureToOptionCost = new ProductSizeToFeatureToOptionCostViewModel();
			vmProductSizeToFeatureToOptionCost.ProductSizeToFeatureToOptionCostId = productSizeToFeatureToOptionCost.ProductSizeToFeatureToOptionCostId;

			return Request.CreateResponse(HttpStatusCode.OK, vmProductSizeToFeatureToOptionCost);
		}

		[HttpPost]
		[ActionName("PostUpdateProductSizeToFeatureToOptionCost")]
		public HttpResponseMessage PostUpdateProductSizeToFeatureToOptionCost([FromBody] ProductSizeToFeatureToOptionCostViewModel data)
		{
			var productSizeToFeatureToOptionCost = _maintenanceManager.GetProductSizeToFeatureToOptionCostById(data.ProductSizeToFeatureToOptionCostId);
			productSizeToFeatureToOptionCost.ProductSizeToFeatureToOptionCostId = data.ProductSizeToFeatureToOptionCostId;
			productSizeToFeatureToOptionCost.ProductSizeId = data.ProductSizeId;
			productSizeToFeatureToOptionCost.FeatureToOptionId = data.FeatureToOptionId;
			productSizeToFeatureToOptionCost.Cost = data.Cost;
			productSizeToFeatureToOptionCost.UpdatedOn = DateTime.Now;
			productSizeToFeatureToOptionCost.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateProductSizeToFeatureToOptionCost(productSizeToFeatureToOptionCost);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteProductSizeToFeatureToOptionCost")]
		public HttpResponseMessage PostDeleteProductSizeToFeatureToOptionCost([FromBody] ProductSizeToFeatureToOptionCostViewModel data)
		{
			if (_maintenanceManager.IsProductSizeToFeatureToOptionCostInProgramFeatureCost(data.ProductSizeToFeatureToOptionCostId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Cost exists in a Program Line");
			}

			_maintenanceManager.DeleteProductSizeToFeatureToOptionCost(data.ProductSizeToFeatureToOptionCostId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion ProductSizeToFeatureToOptionCost

		#region ProductType

		[HttpPost]
		[ActionName("PostInsertProductType")]
		public HttpResponseMessage PostInsertProductType([FromBody] ProductTypeViewModel data)
		{
			if (_maintenanceManager.isProductTypeExist(data.ProductCategoryId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Product Type already exists");
			}

			var productType = new ProductType();
			productType.ProductCategoryId = data.ProductCategoryId;
			productType.Description = data.Description;
			productType.Abbreviation = data.Abbreviation;
			productType.UpdatedOn = DateTime.Now;
			productType.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertProductType(productType);

			var vmProductType = new ProductTypeViewModel();
			vmProductType.ProductTypeId = productType.ProductTypeId;

			return Request.CreateResponse(HttpStatusCode.OK, vmProductType);
		}

		[HttpPost]
		[ActionName("PostUpdateProductType")]
		public HttpResponseMessage PostUpdateProductType([FromBody] ProductTypeViewModel data)
		{
			if (_maintenanceManager.isProductTypeExist(data.ProductCategoryId, data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Product Type already exists");
			}

			var productType = _maintenanceManager.GetProductTypeById(data.ProductTypeId);
			productType.ProductTypeId = data.ProductTypeId;
			productType.ProductCategoryId = data.ProductCategoryId;
			productType.Description = data.Description;
			productType.Abbreviation = data.Abbreviation;
			productType.UpdatedOn = DateTime.Now;
			productType.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateProductType(productType);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteProductType")]
		public HttpResponseMessage PostDeleteProductType([FromBody] ProductTypeViewModel data)
		{
			if (_maintenanceManager.isProductTypeInProductSize(data.ProductTypeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Product Type used in Product Size");
			}
			if (_maintenanceManager.isProductTypeInProductTypeToFeature(data.ProductTypeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Product Type used in Feature Option");
			}
			if (_maintenanceManager.isProductTypeInPackStyle(data.ProductTypeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Product Type used in Pack Style");
			}
			if (_maintenanceManager.isProductTypeInMfgCost(data.ProductTypeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Product Type used in Mfg Cost");
			}

			_maintenanceManager.DeleteProductType(data.ProductTypeId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion ProductType

		#region QuantityRange

		[HttpPost]
		[ActionName("PostInsertQuantityRange")]
		public HttpResponseMessage PostInsertQuantityRange([FromBody] QuantityRangeViewModel data)
		{
			if (_maintenanceManager.isQuantityRangeExist(data.ProductCategoryId, data.ContainerTypeId, data.Minimum, data.Maximum))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Quantity Range already exists");
			}

			var quantityRange = new QuantityRange();
			quantityRange.ProductCategoryId = data.ProductCategoryId;
			quantityRange.ContainerTypeId = data.ContainerTypeId;
			quantityRange.Minimum = data.Minimum;
			quantityRange.Maximum = data.Maximum;
			quantityRange.UpdatedOn = DateTime.Now;
			quantityRange.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertQuantityRange(quantityRange);

			var vmQuantityRange = new QuantityRangeViewModel();
			vmQuantityRange.QuantityRangeId = quantityRange.QuantityRangeId;

			return Request.CreateResponse(HttpStatusCode.OK, vmQuantityRange);
		}

		[HttpPost]
		[ActionName("PostUpdateQuantityRange")]
		public HttpResponseMessage PostUpdateQuantityRange([FromBody] QuantityRangeViewModel data)
		{
			var quantityRange = _maintenanceManager.GetQuantityRangeById(data.QuantityRangeId);
			quantityRange.QuantityRangeId = data.QuantityRangeId;
			quantityRange.ProductCategoryId = data.ProductCategoryId;
			quantityRange.ContainerTypeId = data.ContainerTypeId;
			quantityRange.Minimum = data.Minimum;
			quantityRange.Maximum = data.Maximum;
			quantityRange.UpdatedOn = DateTime.Now;
			quantityRange.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateQuantityRange(quantityRange);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteQuantityRange")]
		public HttpResponseMessage PostDeleteQuantityRange([FromBody] QuantityRangeViewModel data)
		{
			if (_maintenanceManager.isQuantityRangeInPackagingCost(data.QuantityRangeId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Quantity Range used in Packaging Cost");
			}

			_maintenanceManager.DeleteQuantityRange(data.QuantityRangeId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion QuantityRange

		#region Variant

		[HttpPost]
		[ActionName("PostInsertVariant")]
		public HttpResponseMessage PostInsertVariant([FromBody] VariantViewModel data)
		{
			if (_maintenanceManager.isVariantExist(data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Create, Variant already exists");
			}

			var variant = new Variant();
			variant.Description = data.Description;
			variant.Abbreviation = data.Abbreviation;
			variant.UpdatedOn = DateTime.Now;
			variant.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.InsertVariant(variant);

			var vmVariant = new VariantViewModel();
			vmVariant.VariantId = variant.VariantId;

			return Request.CreateResponse(HttpStatusCode.OK, vmVariant);
		}

		[HttpPost]
		[ActionName("PostUpdateVariant")]
		public HttpResponseMessage PostUpdateVariant([FromBody] VariantViewModel data)
		{
			if (_maintenanceManager.isVariantExist(data.Description, data.Abbreviation))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Update, Variant already exists");
			}

			var variant = _maintenanceManager.GetVariantById(data.VariantId);
			variant.VariantId = data.VariantId;
			variant.Description = data.Description;
			variant.Abbreviation = data.Abbreviation;
			variant.UpdatedOn = DateTime.Now;
			variant.UpdatedBy = HttpContext.Current.User.Identity.Name;

			_maintenanceManager.UpdateVariant(variant);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostDeleteVariant")]
		public HttpResponseMessage PostDeleteVariant([FromBody] VariantViewModel data)
		{
			if (_maintenanceManager.isVariantInProgramLine(data.VariantId))
			{
				return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Unable to Delete, Variant exists in a Program Line");
			}

			_maintenanceManager.DeleteVariant(data.VariantId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		#endregion Variant
	}
}
