﻿using dpc.Business;
using dpc.Business.Interfaces;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using dpc.Data;
using dpc.Data.CustomModels;
using dpc.Web.ViewModel;
using AutoMapper;

namespace dpc.WebApi
{
	public class ProgramApiController : ApiController
	{
		private readonly IProgramManager _programManager;

		#region constructors

		public ProgramApiController()
		{
			_programManager = new ProgramManager(new UnitOfWork());
		}

		public ProgramApiController(IProgramManager programManager)
		{
			_programManager = programManager;
		}

		#endregion constructors

		[HttpPost]
		[ActionName("PostDeleteItem")]
		public HttpResponseMessage PostDeleteItem([FromBody] ProgramLineViewModel data)
		{
			_programManager.DeleteProgramLine(data.ProgramLineId);

			//Update calculations for ProgramHeader only
			_programManager.UpdateProgramHeaderCalcs(data.ProgramHeaderId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostInsertItem")]
		public HttpResponseMessage PostInsertItem([FromBody] ProgramLineViewModel data)
		{
			var model = Mapper.Map<ProgramLine>(data);
			_programManager.InsertProgramLine(model);

			//Loop through FeatureList and save each Feature with option selected to table
			foreach (var featureToOptionId in data.FeatureToOptionIds)
			{
				var featureObj = _programManager.GetProductSizeToFeatureToOptionCost(data.ProductSizeId, featureToOptionId);

				_programManager.InsertProgramFeatureCost(model.ProgramLineId, featureObj.ProductSizeToFeatureToOptionCostId, featureObj.Cost);
			}

			//Update calculations
			_programManager.UpdateProgramLineCalcs(model.ProgramLineId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}

		[HttpPost]
		[ActionName("PostUpdateItem")]
		public HttpResponseMessage PostUpdateItem([FromBody] ProgramLineViewModel data)
		{
			var model = Mapper.Map<ProgramLine>(data);
			_programManager.UpdateProgramLine(model);

			//Delete features from ProgramFeatureCost table then insert with new data below
			_programManager.DeleteProgramFeatureCostByProgramLineId(data.ProgramLineId);

			//Loop through FeatureList and update each Feature with option selected to table
			foreach (var featureToOptionId in data.FeatureToOptionIds)
			{
				var featureObj = _programManager.GetProductSizeToFeatureToOptionCost(data.ProductSizeId, featureToOptionId);

				_programManager.InsertProgramFeatureCost(model.ProgramLineId, featureObj.ProductSizeToFeatureToOptionCostId, featureObj.Cost);
			}

			//Update calculations
			_programManager.UpdateProgramLineCalcs(model.ProgramLineId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}
		
		[HttpPost]
		[ActionName("PostUpdateProgramHeader")]
		public HttpResponseMessage PostUpdateProgramHeader([FromBody] ProgramHeaderViewModel data)
		{
			//Get ProgramHeader to update
			var model = _programManager.GetProgramHeaderById(data.ProgramHeaderId);

			//Update model with new values
			model.ProgramHeaderId = data.ProgramHeaderId;
			model.ProductCategoryId = data.ProductCategoryId;
			model.BusinessUnit = data.BusinessUnit;
			model.Title = data.Title;
			model.BrandOwnerId = data.BrandOwnerId;
			model.Status = data.Status.ToString();
			model.RequestedShipDate = data.RequestedShipDate;
			model.ContractEndDate = data.ContractEndDate;
			model.Comments = data.Comments;
			model.TransitionType = data.TransitionType;
			model.Discounts = data.Discounts;
			model.FeesPromotions = data.FeesPromotions;
			model.BrokerFees = data.BrokerFees;
			model.ReturnsDamage = data.ReturnsDamage;
			model.Pallets = data.Pallets;
			model.FreightTerms = data.FreightTerms;
			model.FreightPerPad = data.FreightPerPad;
			model.OceanFreight = data.OceanFreight;
			model.NewProgram = data.NewProgram;
			model.ProgramFee = data.ProgramFee;
			model.ProgramFeeDescription = data.ProgramFeeDescription;

			_programManager.UpdateProgramHeader(model);

			_programManager.UpdateProgramFeatureCostInProgramLines(model.ProgramHeaderId);
			
			//Update ProgramHeader & ProgramLine calculations
			_programManager.UpdateAllCalcs(model.ProgramHeaderId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}
		
		[ActionName("GetGridProgramLineById")]
		public IHttpActionResult GetGridProgramLineById(int id)
		{
			ProgramLineModel programLine = _programManager.GetGridProgramLineById(id);

			if (programLine == null)
			{
				return Json(new { }); // Returns an empty object
			}

			return Ok(programLine);  // Returns an OkNegotiatedContentResult
		}

		[HttpPost]
		[ActionName("PostDeleteProgram")]
		public HttpResponseMessage PostDeleteProgram([FromBody] ProgramHeaderViewModel data)
		{
			//NOTE: Database setup to do a cascading delete from ProgramHeader to ProgramLines
			_programManager.DeleteProgramHeader(data.ProgramHeaderId);

			return Request.CreateResponse(HttpStatusCode.OK, "success");
		}
	}
}
