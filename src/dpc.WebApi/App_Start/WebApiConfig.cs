﻿using System.Web.Http;

namespace dpc.WebApi
{
	public static class WebApiConfig
    {
        //****************************************************************************************//
        // NOTE: this config file is only used if called from url (not from within same Solution.
        //****************************************************************************************//
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
