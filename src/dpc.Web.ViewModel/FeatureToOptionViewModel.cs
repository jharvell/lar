﻿using System.Collections.Generic;
using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class FeatureToOptionViewModel
	{
		public int FeatureToOptionId { get; set; }

        public int ProductTypeToFeatureId { get; set; }

        [DisplayName("Option")]
        public string Description { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

        public int FeatureId { get; set; }

		[DisplayName("Feature")]
		public string FeatureDescription { get; set; }

		[DisplayName("Abbreviation")]
		public string FeatureAbbreviation { get; set; }

		public IEnumerable<SelectViewModel> FeatureList { get; set; }

		public int ProductTypeId { get; set; }

		[DisplayName("Product Type")]
		public string ProductTypeDescription { get; set; }

		[DisplayName("Abbreviation")]
		public string ProductTypeAbbreviation { get; set; }

		public IEnumerable<SelectViewModel> ProductTypeList { get; set; }

		public int ProductCategoryId { get; set; }

		[DisplayName("Product Category")]
		public string ProductCategoryDescription { get; set; }

		[DisplayName("Abbreviation")]
		public string ProductCategoryAbbreviation { get; set; }

		public IEnumerable<SelectViewModel> ProductCategoryList { get; set; }
	}
}