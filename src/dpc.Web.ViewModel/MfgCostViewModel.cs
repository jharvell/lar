﻿using System.Collections.Generic;
using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class MfgCostViewModel
	{
		public int MfgCostId { get; set; }

		[DisplayName("VL and Overhead")]
		public double VLandOverhead { get; set; }

		[DisplayName("Fixed Conversion Cost")]
		public double FixedConversionCost { get; set; }

		public double Depreciation { get; set; }

		public double Scrap { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

		public int ProductTypeId { get; set; }

		[DisplayName("Abbreviation")]
		public string ProductTypeAbbreviation { get; set; }

		[DisplayName("Product Type")]
		public string ProductTypeDescription { get; set; }

		public IEnumerable<SelectViewModel> ProductTypeList { get; set; }

		public int ProductCategoryId { get; set; }

		[DisplayName("Abbreviation")]
		public string ProductCategoryAbbreviation { get; set; }

		[DisplayName("Product Category")]
		public string ProductCategoryDescription { get; set; }

		public IEnumerable<SelectViewModel> ProductCategoryList { get; set; }
	}
}