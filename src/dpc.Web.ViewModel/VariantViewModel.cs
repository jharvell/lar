﻿namespace dpc.Web.ViewModel
{
	public class VariantViewModel
	{
		public int VariantId { get; set; }

		public string Description { get; set; }
		
		public string Abbreviation { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }
	}
}