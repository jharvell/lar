﻿using dpc.Data.CustomModels;
using System.Collections.Generic;
using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class ProductTypeViewModel
	{
		public int ProductTypeId { get; set; }

		public int ProductCategoryId { get; set; }
        
		public string Description { get; set; }
        
		public string Abbreviation { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

		[DisplayName("Product Category")]
		public string ProductCategoryDescription { get; set; }

		public string ProductCategoryAbbreviation { get; set; }

		public IEnumerable<SelectModel> ProductCategoryList { get; set; }
	}
}