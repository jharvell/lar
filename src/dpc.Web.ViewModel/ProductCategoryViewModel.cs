﻿using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class ProductCategoryViewModel
	{
		public int ProductCategoryId { get; set; }

		[DisplayName("Product Category")]
		public string Description { get; set; }

		[DisplayName("Abbreviation")]
		public string Abbreviation { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }
	}
}