﻿using dpc.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dpc.Web.ViewModel
{
	public class ProgramHeaderViewModel
	{
		[DisplayName("Program")]
		public int ProgramHeaderId { get; set; }
		
		public int ProductCategoryId { get; set; }

		public int ApprovalType { get; set; }

		[DisplayName("Business Unit")]
		public string BusinessUnit { get; set; }

		[Required]
		public string Title { get; set; }

		public int BrandOwnerId { get; set; }

		[DisplayName("Brand Owner")]
		public string BrandOwnerDescription { get; set; }

		public IEnumerable<SelectViewModel> BrandOwnerList { get; set; }

		public EnumProgramStatus Status { get; set; }

		[DataType(DataType.Date)]
		[DisplayName("Request Ship")]
		public DateTime? RequestedShipDate { get; set; }

		[DataType(DataType.Date)]
		[DisplayName("Contract End")]
		public DateTime? ContractEndDate { get; set; }

		[DataType(DataType.MultilineText)]
		public string Comments { get; set; }

		public string TransitionType { get; set; }

		[DisplayName("Transition Type")]
		public IEnumerable<SelectViewModel> TransitionTypeList { get; set; }

		public double? Discounts { get; set; }

		[DisplayName("Fees / Promotions")]
		public double? FeesPromotions { get; set; }

		[DisplayName("Broker Fees")]
		public double? BrokerFees { get; set; }

		[DisplayName("Returns, Damage, etc.")]
		public double? ReturnsDamage { get; set; }

		public bool Pallets { get; set; }

		[DisplayName("Pallets")]
		public IEnumerable<SelectViewModel> PalletsList { get; set; }

		public string FreightTerms { get; set; }

		[DisplayName("Freight Terms")]
		public IEnumerable<SelectViewModel> FreightTermsList { get; set; }

		[DisplayName("Freight Per Pad")]
		public double? FreightPerPad { get; set; }

		[DisplayName("Ocean Freight per Pad")]
		public double? OceanFreight { get; set; }

		public bool NewProgram { get; set; }
		
		[DisplayName("New Program")]
		public IEnumerable<SelectViewModel> NewProgramList { get; set; }

		[DisplayName("Unique Program Fee/Cost")]
		public double? ProgramFee { get; set; }

		[DisplayName("Fee Description")]
		public string ProgramFeeDescription { get; set; }

		public double? Pieces { get; set; }

		public double? Bags { get; set; }

		public double? Cases { get; set; }

		public double? ContributionMargin { get; set; }

		public double? ContributionMarginPercent { get; set; }

		public double? ContributionMarginPerPiece { get; set; }

		public double? VariableMargin { get; set; }

		public double? VariableMarginPercent { get; set; }

		public double? VariableMarginPerPiece { get; set; }

		public double? NetSales { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

		public System.DateTime CreatedOn { get; set; }

		public string CreatedBy { get; set; }

		public Nullable<System.DateTime> ApprovalSubmittedOn { get; set; }

		public string ApprovalSubmittedBy { get; set; }

		public Nullable<System.DateTime> ApprovedOn { get; set; }

		public bool isProgramLinesExist { get; set; }

		public bool isProgramSubmitted { get; set; }

		public bool isProgramApproved { get; set; }

		public bool isProgramRejected { get; set; }

		public int ProgramLineCount { get; set; }

		public bool ShowLRRdetailReport { get; set; }

		public bool ShowLRRfinancialReport { get; set; }

		public bool ShowCalculationsReport { get; set; }
	}
}