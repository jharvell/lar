﻿using System.Collections.Generic;
using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class PackStyleViewModel
	{
		public int PackStyleId { get; set; }

		public string Abbreviation { get; set; }
		
		public string Description { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

		public int ProductTypeId { get; set; }

		[DisplayName("Abbreviation")]
		public string ProductTypeAbbreviation { get; set; }

		[DisplayName("Product Type")]
		public string ProductTypeDescription { get; set; }

		public IEnumerable<SelectViewModel> ProductTypeList { get; set; }

		public int ProductCategoryId { get; set; }

		[DisplayName("Abbreviation")]
		public string ProductCategoryAbbreviation { get; set; }

		[DisplayName("Product Category")]
		public string ProductCategoryDescription { get; set; }

		public IEnumerable<SelectViewModel> ProductCategoryList { get; set; }
	}
}