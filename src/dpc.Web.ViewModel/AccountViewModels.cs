﻿using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace dpc.Web.ViewModel
{
	public class ForgotViewModel
	{
		[Required]
		[Display(Name = "Email")]
		public string Email { get; set; }
	}

	public class LoginViewModel
	{
		[Required]
		[Display(Name = "UserName")]
		public string UserName { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Password")]
		public string Password { get; set; }

		[Display(Name = "Remember me?")]
		public bool RememberMe { get; set; }
	}
	
	public class ResetPasswordViewModel
	{
		[Required]
		[EmailAddress]
		[Display(Name = "Email")]
		public string Email { get; set; }

		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Password")]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Confirm password")]
		[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
		public string ConfirmPassword { get; set; }

		public string Code { get; set; }

		private string _passwordRequiredLength;
		public string PasswordRequiredLength
		{
			get { return _passwordRequiredLength = ConfigurationManager.AppSettings["PasswordRequiredLength"]; }
		}

		private string _maxFailedAccessAttemptsBeforeLockout;
		public string MaxFailedAccessAttemptsBeforeLockout
		{
			get { return _maxFailedAccessAttemptsBeforeLockout = ConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"]; }
		}

		private string _maxDaysBeforePasswordChange;
		public string MaxDaysBeforePasswordChange
		{
			get { return _maxDaysBeforePasswordChange = ConfigurationManager.AppSettings["MaxDaysBeforePasswordChange"]; }
		}

		private string _maxReusePreviousPasswords;
		public string MaxReusePreviousPasswords
		{
			get { return _maxReusePreviousPasswords = ConfigurationManager.AppSettings["MaxReusePreviousPasswords"]; }
		}

		private string _defaultAccountLockoutTimeSpan;
		public string DefaultAccountLockoutTimeSpan
		{
			get { return _defaultAccountLockoutTimeSpan = ConfigurationManager.AppSettings["DefaultAccountLockoutTimeSpan"]; }
		}
	}

	public class ForgotPasswordViewModel
	{
		[Required]
		[EmailAddress]
		[Display(Name = "Email")]
		public string Email { get; set; }
	}
}
