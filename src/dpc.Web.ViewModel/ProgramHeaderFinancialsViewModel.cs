﻿using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class ProgramHeaderFinanacialsViewModel
	{
		[DisplayName("Program")]
		public int ProgramHeaderId { get; set; }

		private double? _pieces;

		public double? Pieces
		{
			get { return _pieces / 1000; }
			set { _pieces = value; }
		}

		private double? _bags;
		public double? Bags
		{
			get { return _bags / 1000; }
			set { _bags = value; }
		}

		private double? _cases;

		public double? Cases
		{
			get { return _cases / 1000; }
			set { _cases = value; }
		}

		private double? _netSales;

		public double? NetSales
		{
			get { return _netSales / 1000; }
			set { _netSales = value; }
		}

		private double? _contributionMargin;

		public double? ContributionMargin
		{
			get { return _contributionMargin / 1000; }
			set { _contributionMargin = value; }
		}

		public double? ContributionMarginPercent { get; set; }

		public double? ContributionMarginPerPiece { get; set; }

		private double? _variableMargin;
		public double? VariableMargin
		{
			get { return _variableMargin / 1000; }
			set { _variableMargin = value; }
		}

		public double? VariableMarginPercent { get; set; }

		public double? VariableMarginPerPiece { get; set; }
	}
}