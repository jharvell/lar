﻿using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class ContainerTypeViewModel
    {
        public int ContainerTypeId { get; set; }

		[DisplayName("Container Type")]
		public string Description { get; set; }  

        public string Abbreviation { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }
	}
}