﻿using System.Collections.Generic;
using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class PackagingTypeViewModel
	{
		public int PackagingTypeId { get; set; }

        public int ContainerTypeId { get; set; }

        public string Description { get; set; }

        public string Abbreviation { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

        [DisplayName("Container Type")]
        public string ContainerTypeDescription { get; set; }

        [DisplayName("Abbreviation")]
        public string ContainerTypeAbbreviation { get; set; }

        public IEnumerable<SelectViewModel> ContainerTypeList { get; set; }
    }
}