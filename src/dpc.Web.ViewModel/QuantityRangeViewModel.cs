﻿using System.Collections.Generic;
using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class QuantityRangeViewModel
    {
        public int QuantityRangeId { get; set; }

        public int ProductCategoryId { get; set; }

        public int ContainerTypeId { get; set; }

        public int Minimum { get; set; }

        public int Maximum { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

		[DisplayName("Container")]
        public string ContainerTypeDescription { get; set; }

        public string ContainerTypeAbbreviation { get; set; }

		public IEnumerable<SelectViewModel> ContainerTypeList { get; set; }

		[DisplayName("Product Category")]
		public string ProductCategoryDescription { get; set; }

		public string ProductCategoryAbbreviation { get; set; }

        public IEnumerable<SelectViewModel> ProductCategoryList { get; set; }
	}
}