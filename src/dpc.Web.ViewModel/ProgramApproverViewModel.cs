﻿using System;

namespace dpc.Web.ViewModel
{
	public class ProgramApproverViewModel
	{
		public string UserId { get; set; }

		public string Email { get; set; }

		public string UserName { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public int ProgramApprovalId { get; set; }

		public bool? Approved { get; set; }

		public string Comment { get; set; }

		public System.DateTime? CommentOn { get; set; }

		public bool isReadOnly { get; set; }
	}
}