﻿namespace dpc.Web.ViewModel
{
	public class SelectViewModel
	{
		public string Description { get; set; }

		public string Value { get; set; }
	}
}