﻿using System.Collections.Generic;
using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class FeatureViewModel
	{
		public int FeatureId { get; set; }

		public int ProductCategoryId { get; set; }
		
		public string Description { get; set; }

		public string Abbreviation { get; set; }

		[DisplayName("Product Category")]
		public string ProductCategoryDescription { get; set; }

		[DisplayName("Abbreviation")]
		public string ProductCategoryAbbreviation { get; set; }

		public IEnumerable<SelectViewModel> ProductCategoryList { get; set; }
	}
}