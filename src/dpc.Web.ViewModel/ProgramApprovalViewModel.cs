﻿using System;
using System.Collections.Generic;
using dpc.Data;

namespace dpc.Web.ViewModel
{
	public class ProgramApprovalViewModel
	{
		public ProgramHeaderViewModel ProgramHeader { get; set; }

		public IEnumerable<ProgramApproverViewModel> ApproverList { get; set; }

		public int ProgramApprovalId { get; set; }

		public bool? Approved { get; set; }

		public string Comment { get; set; }
	}
}