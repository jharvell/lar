﻿using System.Collections.Generic;

namespace dpc.Web.ViewModel
{
	public class ProgramLineExtViewModel
	{
		public ProgramHeaderViewModel ProgramHeader { get; set; }

		public ProgramLineViewModel ProgramLine { get; set; }

		public IEnumerable<FeatureViewModel> FeatureList { get; set; }

		public IEnumerable<int> FeatureToOptionId { get; set; }

		public System.Data.DataTable FeatureOptions { get; set; }

		public bool isApprovalSubmitted { get; set; }
	}
}