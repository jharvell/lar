﻿namespace dpc.Web.ViewModel
{
	public class FeatureOptionViewModel
	{
		public string FeatureAbbreviation { get; set; }

		public string FeatureDescription { get; set; }

		public string FeatureToOptionDescription { get; set; }
	}
}