﻿using System.Collections.Generic;
using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class ProgramLineViewModel
	{
		[DisplayName("Program Line")]
		public int ProgramLineId { get; set; }

		public int ProgramHeaderId { get; set; }

		public int ProductTypeId { get; set; }

		[DisplayName("Product")]
		public string ProductTypeAbbreviation { get; set; }

		public string ProductTypeDescription { get; set; }

		public int ProductSizeId { get; set; }

		[DisplayName("Size")]
		public string ProductSizeDescription { get; set; }

		public int ContainerTypeId { get; set; }

		[DisplayName("Selling Unit")]
		public string ContainerTypeDescription { get; set; }

		public int PackagingTypeId { get; set; }

		[DisplayName("Pack Style")]
		public string PackagingTypeDescription { get; set; }

		public int BagTypeToPackagingTypeId { get; set; }

		[DisplayName("Secondary Packaging Type")]
		public string BagTypeToPackagingTypeDescription { get; set; }

		public int CaseTypeToPackagingTypeId { get; set; }

		[DisplayName("Shipper Type")]
		public string CaseTypeToPackagingTypeDescription { get; set; }

		public int BrandId { get; set; }

		[DisplayName("Brand")]
		public string BrandDescription { get; set; }

		public int PackStyleId { get; set; }

		public string PackStyleDescription { get; set; }

		public int VariantId { get; set; }

		[DisplayName("Variant")]
		public string VariantDescription { get; set; }

		public string CustomSKU { get; set; }

		[DisplayName("Bag Count")]
		public int BagCount { get; set; }

		[DisplayName("Bags Per Case")]
		public int BagsPerCase { get; set; }

		[DisplayName("Annual Cases")]
		public int AnnualCases { get; set; }

		[DisplayName("Case Selling Price")]
		public double CaseSellingPrice { get; set; }
		
		public double Pieces { get; set; }

		[DisplayName("Piece Price")]
		public double PiecePrice { get; set; }

		[DisplayName("Contribution Margin")]
		public double ContributionMargin { get; set; }

		[DisplayName("Contribution Margin %")]
		public double ContributionMarginPercent { get; set; }

		[DisplayName("Variable Margin")]
		public double VariableMargin { get; set; }

		[DisplayName("Variable Margin %")]
		public double VariableMarginPercent { get; set; }

		[DisplayName("Net Sales")]
		public double NetSales { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

		[DisplayName("Case Count")]
		public int CaseCount { get; set; }

		[DisplayName("Gross Sales")]
		public double? GrossSales { get; set; }

		public double? Discounts { get; set; }

		public double? Fees { get; set; }

		[DisplayName("All Other")]
		public double? Other { get; set; }

		[DisplayName("Broker Fees")]
		public double? BrokerFees { get; set; }

		public double? Scrap { get; set; }

		[DisplayName("Total Bags")]
		public double? TotalBags { get; set; }

		[DisplayName("Total Boxes")]
		public double? TotalBoxes { get; set; }

		public double? Plates { get; set; }

		public double? Pallets { get; set; }

		[DisplayName("VL and Overhead")]
		public double? VlOverhead { get; set; }

		[DisplayName("Ocean Freight")]
		public double? OceanFreight { get; set; }

		[DisplayName("Customer Freight")]
		public double? CustFreight { get; set; }

		[DisplayName("Depreciation")]
		public double? Depr { get; set; }

		[DisplayName("Bag Cost")]
		public double? BagCost { get; set; }

		[DisplayName("Box Cost")]
		public double? BoxCost { get; set; }

		public double? MaterialPerPad { get; set; }

		public double? VLPerPad { get; set; }

		public double? FixedCost { get; set; }

		public IEnumerable<FeatureOptionViewModel> FeatureOptions { get; set; }

		public int[] FeatureToOptionIds { get; set; }
	}
}