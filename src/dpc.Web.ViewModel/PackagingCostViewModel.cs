﻿using System.Collections.Generic;
using System.ComponentModel;

namespace dpc.Web.ViewModel
{
	public class PackagingCostViewModel
    {
		public int PackagingCostId { get; set; }

		public double Cost { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

        public int QuantityRangeId { get; set; }

        [DisplayName("Annual Quantity Range")]
        public string QuantityRangeItem { get; set; }

        public IEnumerable<SelectViewModel> QuantityRangeList { get; set; }

        public int PackagingTypeId { get; set; }

        [DisplayName("Packaging")]
        public string PackagingTypeDescription { get; set; }

        [DisplayName("Abbreviation")]
        public string PackagingTypeAbbreviation { get; set; }

        public IEnumerable<SelectViewModel> PackagingTypeList { get; set; }

        public int ContainerTypeId { get; set; }

        [DisplayName("Container")]
        public string ContainerTypeDescription { get; set; }

        [DisplayName("Abbreviation")]
        public string ContainerTypeAbbreviation { get; set; }

        public IEnumerable<SelectViewModel> ContainerTypeList { get; set; }

        public int ProductSizeId { get; set; }

        [DisplayName("Product Size")]
        public string ProductSizeDescription { get; set; }

        [DisplayName("Abbreviation")]
        public string ProductSizeAbbreviation { get; set; }

        public IEnumerable<SelectViewModel> ProductSizeList { get; set; }

        public int ProductTypeId { get; set; }

		[DisplayName("Product Type")]
		public string ProductTypeDescription { get; set; }

		[DisplayName("Abbreviation")]
		public string ProductTypeAbbreviation { get; set; }

		public IEnumerable<SelectViewModel> ProductTypeList { get; set; }

		public int ProductCategoryId { get; set; }

		[DisplayName("Product Category")]
		public string ProductCategoryDescription { get; set; }

		[DisplayName("Abbreviation")]
		public string ProductCategoryAbbreviation { get; set; }

		public IEnumerable<SelectViewModel> ProductCategoryList { get; set; }
	}
}