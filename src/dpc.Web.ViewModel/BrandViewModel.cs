﻿using dpc.Data.CustomModels;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace dpc.Web.ViewModel
{
	public class BrandViewModel
	{
		public int BrandId { get; set; }

		public int BrandOwnerId { get; set; }

		[Required]
		public string Description { get; set; }
		
		[Required]
		public string Abbreviation { get; set; }

		//public System.DateTime UpdatedOn { get; set; }  //NOT NEEDED

		//public string UpdatedBy { get; set; }  //NOT NEEDED

		//public BrandOwner BrandOwner { get; set; }  //HOW TO USE OBJECT TO MAP TO MODEL INSTEAD OF 2 PROP BELOW ??

		[DisplayName("Brand Owner")]
		public string BrandOwnerDescription { get; set; }

		public string BrandOwnerAbbreviation { get; set; }

		public IEnumerable<SelectModel> BrandOwnerList { get; set; }
	}
}