//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dpc.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class PackagingType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PackagingType()
        {
            this.PackagingCost = new HashSet<PackagingCost>();
            this.ProgramLine = new HashSet<ProgramLine>();
            this.ProgramLine1 = new HashSet<ProgramLine>();
        }
    
        public int PackagingTypeId { get; set; }
        public int ContainerTypeId { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    
        public virtual ContainerType ContainerType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PackagingCost> PackagingCost { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProgramLine> ProgramLine { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProgramLine> ProgramLine1 { get; set; }
    }
}
