﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class FeatureRepository : GenericRepository<Feature>, IFeatureRepository
    {
		public FeatureRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<FeatureModel> GetFeatureInfo()
		{
			return (from f in _context.Feature
					join pc in _context.ProductCategory on f.ProductCategoryId equals pc.ProductCategoryId
					select new { f, pc }).Select(p => new FeatureModel
					{
						FeatureId = p.f.FeatureId,
						Abbreviation = p.f.Abbreviation,
						Description = p.f.Description,
						ProductCategoryId = p.pc.ProductCategoryId,
						ProductCategoryAbbreviation = p.pc.Abbreviation,
						ProductCategoryDescription = p.pc.Description
					});
        }

        public IEnumerable<SelectModel> GetFeatureListByProductType(int productTypeId)
        {
            return (from f in _context.Feature
                    join pttf in _context.ProductTypeToFeature on f.FeatureId equals pttf.FeatureId
                    join pt in _context.ProductType on pttf.ProductTypeId equals pt.ProductTypeId
                    select new { f, pttf, pt }).Where(p => p.pt.ProductTypeId == productTypeId)
                    .Select(x => new SelectModel
                    {
                        Description = x.f.Description,
                        Value = x.f.FeatureId.ToString()
                    });
        }

        public IEnumerable<SelectModel> GetFeatureListByProductCategory(int productCategoryId)
        {
            return (from f in _context.Feature
                    join pc in _context.ProductCategory on f.ProductCategoryId equals pc.ProductCategoryId
                    select new { f, pc }).Where(p => p.f.ProductCategoryId == productCategoryId)
                    .Select(x => new SelectModel
                    {
                        Description = x.f.Description,
                        Value = x.f.FeatureId.ToString()
                    });
        }

        public bool isFeatureExist(int productCategoryId, string description, string abbreviation)
        {
			return _context.Feature.Where(x => x.ProductCategoryId == productCategoryId &&
										x.Description.ToLower() == description.ToLower() &&
										x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
		}
    }
}
