﻿using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class ProductTypeToFeatureRepository : GenericRepository<ProductTypeToFeature>, IProductTypeToFeatureRepository
	{
		public ProductTypeToFeatureRepository(dpcEntities context)
			: base(context)
		{
		}

        public ProductTypeToFeature GetProductTypeToFeatureByKeys(int productTypeId, int featureId)
        {
            var result = _context.ProductTypeToFeature
                .Where(p => p.ProductTypeId == productTypeId && p.FeatureId == featureId)
                .FirstOrDefault();

	        return result;

        }
    }
}
