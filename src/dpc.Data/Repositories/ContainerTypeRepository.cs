﻿using dpc.Data.Interfaces;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class ContainerTypeRepository : GenericRepository<ContainerType>, IContainerTypeRepository
	{
		public ContainerTypeRepository(dpcEntities context)
			: base(context)
		{
		}

		public bool isContainerTypeExist(string description, string abbreviation)
		{
			return _context.ContainerType.Where(x => x.Description.ToLower() == description.ToLower() && x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
		}
	}
}
