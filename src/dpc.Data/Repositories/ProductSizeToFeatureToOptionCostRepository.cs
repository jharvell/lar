﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
    public class ProductSizeToFeatureToOptionCostRepository : GenericRepository<ProductSizeToFeatureToOptionCost>, IProductSizeToFeatureToOptionCostRepository
    {
		public ProductSizeToFeatureToOptionCostRepository(dpcEntities context)
			: base(context)
		{
		}

        public IEnumerable<ProductSizeToFeatureToOptionCostModel> GetProductSizeToFeatureToOptionCostInfo()
        {
            return (from pstftoc in _context.ProductSizeToFeatureToOptionCost
                    join fto in _context.FeatureToOption on pstftoc.FeatureToOptionId equals fto.FeatureToOptionId
                    join pttf in _context.ProductTypeToFeature on fto.ProductTypeToFeatureId equals pttf.ProductTypeToFeatureId
                    join f in _context.Feature on pttf.FeatureId equals f.FeatureId
                    join pt in _context.ProductType on pttf.ProductTypeId equals pt.ProductTypeId
                    join ps in _context.ProductSize on pstftoc.ProductSizeId equals ps.ProductSizeId
                    join pc in _context.ProductCategory on f.ProductCategoryId equals pc.ProductCategoryId
                    select new { pstftoc, fto, pttf, f, ps, pt, pc }).Select(x => new ProductSizeToFeatureToOptionCostModel
                    {
                        ProductSizeToFeatureToOptionCostId = x.pstftoc.ProductSizeToFeatureToOptionCostId,
                        Cost = x.pstftoc.Cost,
                        FeatureToOptionId = x.fto.FeatureToOptionId,
                        FeatureToOptionDescription = x.fto.Description,
                        FeatureId = x.f.FeatureId,
                        FeatureDescription = x.f.Description,
                        FeatureAbbreviation = x.f.Abbreviation,
                        ProductSizeId = x.ps.ProductSizeId,
                        ProductSizeDescription = x.ps.Description,
                        ProductSizeAbbreviation = x.ps.Abbreviation,
                        ProductTypeId = x.pt.ProductTypeId,
                        ProductTypeDescription = x.pt.Description,
                        ProductTypeAbbreviation = x.pt.Abbreviation,
                        ProductCategoryId = x.pc.ProductCategoryId,
                        ProductCategoryDescription = x.pc.Description,
                        ProductCategoryAbbreviation = x.pc.Abbreviation
                    });
        }

        public ProductSizeToFeatureToOptionCostModel GetProductSizeToFeatureToOptionCostInfoById(int productSizeToFeatureToOptionCostId)
        {
            IEnumerable<ProductSizeToFeatureToOptionCostModel> productSizeToFeatureToOptionCostInfo = GetProductSizeToFeatureToOptionCostInfo();

            return productSizeToFeatureToOptionCostInfo
                .Where(p => p.ProductSizeToFeatureToOptionCostId == productSizeToFeatureToOptionCostId)
                .FirstOrDefault();
        }

        public bool isProductSizeToFeatureToOptionCostExist(int productSizeId, int featureToOptionId)
        {
			return _context.ProductSizeToFeatureToOptionCost.Where(x => x.ProductSizeId == productSizeId && x.FeatureToOptionId == featureToOptionId).Any();
        }
    }
}
