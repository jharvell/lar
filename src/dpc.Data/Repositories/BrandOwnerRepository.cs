﻿using dpc.Data.Interfaces;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class BrandOwnerRepository : GenericRepository<BrandOwner>, IBrandOwnerRepository
	{
		public BrandOwnerRepository(dpcEntities context)
			: base(context)
		{
		}

		public bool isBrandOwnerInProgramHeader(int brandOwnerId)
		{
			return _context.ProgramHeader.Where(x => x.BrandOwnerId == brandOwnerId).Any();
		}

		public bool isBrandOwnerExist(string description, string abbreviation)
		{
			return _context.BrandOwner.Where(x => x.Description.ToLower() == description.ToLower() &&
										     x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
		}
	}
}
