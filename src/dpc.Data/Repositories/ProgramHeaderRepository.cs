﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class ProgramHeaderRepository : GenericRepository<ProgramHeader>, IProgramHeaderRepository
	{
		public ProgramHeaderRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<ProgramHeaderModel> GetProgramHeaderInfo()
		{
			return (from ph in _context.ProgramHeader
					join bo in _context.BrandOwner on ph.BrandOwnerId equals bo.BrandOwnerId
					select new { ph, bo }).Select(x => new ProgramHeaderModel
					{
						ProgramHeaderId = x.ph.ProgramHeaderId,
						Status = x.ph.Status,
						Title = x.ph.Title,
						BusinessUnit = x.ph.BusinessUnit,
						BrandOwnerDescription = x.bo.Description,
						RequestedShipDate = x.ph.RequestedShipDate,
						ContractEndDate = x.ph.ContractEndDate
					});
		}

		public ProgramHeaderModel GetProgramHeaderInfoById(int programHeaderId)
		{
			return (from ph in _context.ProgramHeader
					join bo in _context.BrandOwner on ph.BrandOwnerId equals bo.BrandOwnerId
					where ph.ProgramHeaderId == programHeaderId
					select new { ph, bo }).Select(x => new ProgramHeaderModel
					{
						ProgramHeaderId = x.ph.ProgramHeaderId,
						Status = x.ph.Status,
						Title = x.ph.Title,
						BusinessUnit = x.ph.BusinessUnit,
						BrandOwnerDescription = x.bo.Description,
						RequestedShipDate = x.ph.RequestedShipDate,
						ContractEndDate = x.ph.ContractEndDate,
						Comments = x.ph.Comments,
						CreatedOn = x.ph.CreatedOn,
						CreatedBy = x.ph.CreatedBy
					}).FirstOrDefault();
		}

		IEnumerable<SelectModel> IProgramHeaderRepository.GetTransitionTypeList()
		{
			var transitionType = new List<SelectModel>
			{
				new SelectModel {Description = "Hard", Value = "Hard"},
				new SelectModel {Description = "Soft", Value = "Soft"}
			};

			return transitionType;
		}

		IEnumerable<SelectModel> IProgramHeaderRepository.GetYesNoList()
		{
			var yesNo = new List<SelectModel>
			{
				new SelectModel {Description = "Yes", Value = "True"},
				new SelectModel {Description = "No", Value = "False"}
			};

			return yesNo;
		}

		IEnumerable<SelectModel> IProgramHeaderRepository.GetFreightTermsList()
		{
			var freightTerms = new List<SelectModel>
			{
				new SelectModel {Description = "Domtar TL", Value = "Domtar TL"},
				new SelectModel {Description = "Domtar LTL", Value = "Domtar LTL"},
				new SelectModel {Description = "Customer Pickup", Value = "Customer Pickup"}
			};

			return freightTerms;
		}
		
		public void UpdateProgramHeaderCalcs(int programHeaderId)
		{
			var paramProgramHeaderId = new SqlParameter
			{
				ParameterName = "programHeaderId",
				Value = programHeaderId
			};

			var result = _context.Database.ExecuteSqlCommand("exec usp_ProgramHeaderCalcs @programHeaderId ", paramProgramHeaderId);
		}
		public void UpdateAllCalcs(int programHeaderId)
		{
			var paramProgramHeaderId = new SqlParameter
			{
				ParameterName = "programHeaderId",
				Value = programHeaderId
			};

			var result = _context.Database.ExecuteSqlCommand("exec usp_UpdateAllCalcs @programHeaderId ", paramProgramHeaderId);
		}
	}
}
