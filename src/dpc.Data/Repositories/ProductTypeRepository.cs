﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class ProductTypeRepository : GenericRepository<ProductType>, IProductTypeRepository
	{
		public ProductTypeRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<ProductTypeModel> GetProductTypeInfo()
		{
			return (from pt in _context.ProductType
					join pc in _context.ProductCategory on pt.ProductCategoryId equals pc.ProductCategoryId
					select new { pt, pc }).Select(p => new ProductTypeModel
					{
						ProductTypeId = p.pt.ProductTypeId,
						Abbreviation = p.pt.Abbreviation,
						Description = p.pt.Description,
						ProductCategoryId = p.pc.ProductCategoryId,
						ProductCategoryAbbreviation = p.pc.Abbreviation,
						ProductCategoryDescription = p.pc.Description
					});
		}

        public bool isProductTypeExist(int productCategoryId, string description, string abbreviation)
        {
			return _context.ProductType.Where(x => x.ProductCategoryId == productCategoryId &&
										x.Description.ToLower() == description.ToLower() &&
										x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
        }
    }
}
