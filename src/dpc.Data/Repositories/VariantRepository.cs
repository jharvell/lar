﻿using dpc.Data.Interfaces;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class VariantRepository : GenericRepository<Variant>, IVariantRepository
	{
		public VariantRepository(dpcEntities context)
			: base(context)
		{
		}

		public bool isVariantInProgramLine(int variantId)
		{
			return _context.ProgramLine.Where(x => x.VariantId == variantId).Any();
		}

		public bool isVariantExist(string description, string abbreviation)
		{
			return _context.Variant.Where(x => x.Description.ToLower() == description.ToLower() &&
										     x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
		}
	}
}
