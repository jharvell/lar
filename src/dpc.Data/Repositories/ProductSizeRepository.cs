﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class ProductSizeRepository : GenericRepository<ProductSize>, IProductSizeRepository
	{
		public ProductSizeRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<ProductSizeModel> GetProductSizeInfo()
		{

			return (from ps in _context.ProductSize
				join pt in _context.ProductType on ps.ProductTypeId equals pt.ProductTypeId
				join pc in _context.ProductCategory on pt.ProductCategoryId equals pc.ProductCategoryId
				select new { ps, pt, pc }).Select(p => new ProductSizeModel
				{
					ProductSizeId = p.ps.ProductSizeId,
					Abbreviation = p.ps.Abbreviation,
					Description = p.ps.Description,
					ProductTypeId = p.pt.ProductTypeId,
					ProductTypeAbbreviation = p.pt.Abbreviation,
					ProductTypeDescription = p.pt.Description,
					ProductCategoryId = p.pc.ProductCategoryId,
					ProductCategoryAbbreviation = p.pc.Abbreviation,
					ProductCategoryDescription = p.pc.Description
				});
		}

		public ProductSizeModel GetProductSizeInfoById(int productSizeId)
		{
			IEnumerable<ProductSizeModel> productSizeInfo = GetProductSizeInfo();

			return productSizeInfo.Where(p => p.ProductSizeId == productSizeId).SingleOrDefault();
        }

        public bool isProductSizeExist(int productTypeId, string description, string abbreviation)
        {
			return _context.ProductSize.Where(x => x.ProductTypeId == productTypeId &&
										   x.Description.ToLower() == description.ToLower() &&
										   x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
        }
    }
}
