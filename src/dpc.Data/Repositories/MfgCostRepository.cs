﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class MfgCostRepository : GenericRepository<MfgCost>, IMfgCostRepository
	{
		public MfgCostRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<MfgCostModel> GetMfgCostInfo()
		{
			return (from mc in _context.MfgCost
					join pt in _context.ProductType on mc.ProductTypeId equals pt.ProductTypeId
					join pc in _context.ProductCategory on pt.ProductCategoryId equals pc.ProductCategoryId
					select new { mc, pt, pc }).Select(x => new MfgCostModel
					{
						MfgCostId = x.mc.MfgCostId,
						VLandOverhead = x.mc.VLandOverhead,
						FixedConversionCost = x.mc.FixedConversionCost,
						Depreciation = x.mc.Depreciation,
						Scrap = x.mc.Scrap,
						ProductTypeId = x.pt.ProductTypeId,
						ProductTypeAbbreviation = x.pt.Abbreviation,
						ProductTypeDescription = x.pt.Description,
						ProductCategoryId = x.pc.ProductCategoryId,
						ProductCategoryAbbreviation = x.pc.Abbreviation,
						ProductCategoryDescription = x.pc.Description
					});
		}

		public bool isMfgCostExist(int productTypeId)
		{
			return _context.MfgCost.Where(x => x.ProductTypeId == productTypeId).Any();
		}
	}
}
