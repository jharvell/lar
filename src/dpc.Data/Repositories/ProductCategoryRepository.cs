﻿using dpc.Data.Interfaces;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class ProductCategoryRepository : GenericRepository<ProductCategory>, IProductCategoryRepository
	{
		public ProductCategoryRepository(dpcEntities context)
			: base(context)
		{
		}

        public bool isProductCategoryExist(string description, string abbreviation)
        {
			return _context.ProductCategory.Where(x => x.Description.ToLower() == description.ToLower() && x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
        }
    }
}
