﻿using dpc.Data.Interfaces;

namespace dpc.Data.Repositories
{
	public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
	{
		public CustomerRepository(dpcEntities context)
			: base(context)
		{
		}
	}
}
