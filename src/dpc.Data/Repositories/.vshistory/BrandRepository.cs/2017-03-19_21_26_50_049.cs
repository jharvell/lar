﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class BrandRepository : GenericRepository<Brand>, IBrandRepository
	{
		public BrandRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<BrandModel> GetBrandInfo()
		{
			return (from b in _context.Brand
					join bo in _context.BrandOwner on b.BrandOwnerId equals bo.BrandOwnerId
					select new { b, bo }).Select(x => new BrandModel
					{
						BrandId = x.b.BrandId,
						Description = x.b.Description,
						Abbreviation = x.b.Abbreviation,
						BrandOwnerId = x.bo.BrandOwnerId,
						BrandOwnerDescription = x.bo.Description,
						BrandOwnerAbbreviation = x.bo.Abbreviation
					});
		}

		public bool isBrandInProgramLine(int brandId)
		{
			return _context.ProgramLine.Where(x => x.BrandId == brandId).Any();
		}

		public bool isBrandExist(int brandOwnerId, string description, string abbreviation)
		{
			return _context.Brand.Where(x => x.BrandOwnerId == brandOwnerId && 
										x.Description.ToLower() == description.ToLower() && 
										x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
		}
	}
}
