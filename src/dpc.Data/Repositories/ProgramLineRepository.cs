﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class ProgramLineRepository : GenericRepository<ProgramLine>, IProgramLineRepository
	{
		public ProgramLineRepository(dpcEntities context)
			: base(context)
		{
		}

		public ProgramLineModel GetGridProgramLineById(int programLineId)
		{
			var result = (from pl in _context.ProgramLine
						  join pt in _context.ProductType on pl.ProductTypeId equals pt.ProductTypeId
						  join pks in _context.PackStyle on pl.PackStyleId equals pks.PackStyleId
						  join ps in _context.ProductSize on pl.ProductSizeId equals ps.ProductSizeId
						  join pkt in _context.PackagingType on pl.BagTypeToPackagingTypeId equals pkt.PackagingTypeId
						  join ct in _context.ContainerType on pl.ContainerTypeId equals ct.ContainerTypeId
						  join b in _context.Brand on pl.BrandId equals b.BrandId
						  join v in _context.Variant on pl.VariantId equals v.VariantId
						  where pl.ProgramLineId == programLineId
						  select new { pl, pt, pks, ps, pkt, ct, b, v })
						  .Select(p => new ProgramLineModel
						  {
							  ProgramLineId = p.pl.ProgramLineId,
							  ProductTypeAbbreviation = p.pt.Abbreviation,
							  ProductTypeId = p.pt.ProductTypeId,
							  ProductTypeDescription = p.pt.Description,
							  PackagingTypeId = p.pkt.PackagingTypeId,
							  PackagingTypeDescription = p.pkt.Description,
							  ProductSizeId = p.ps.ProductSizeId,
							  ProductSizeDescription = p.ps.Description,
							  PackStyleId = p.pks.PackStyleId,
							  PackStyleDescription = p.pks.Description,
							  ContainerTypeId = p.ct.ContainerTypeId,
							  ContainerTypeDescription = p.ct.Description,
							  CustomSKU = p.pl.CustomSKU,
							  BagTypeToPackagingTypeId = p.pl.BagTypeToPackagingTypeId,
							  CaseTypeToPackagingTypeId = p.pl.CaseTypeToPackagingTypeId,
							  BrandId = p.b.BrandId,
							  BrandDescription = p.b.Description,
							  VariantId = p.v.VariantId,
							  VariantDescription = p.v.Description,
							  BagCount = p.pl.BagCount,
							  BagsPerCase = p.pl.BagsPerCase,
							  AnnualCases = p.pl.AnnualCases,
							  CaseSellingPrice = p.pl.CaseSellingPrice
						  }).FirstOrDefault();

			return result;
		}

		public IEnumerable<ProgramLineModel> GetProgramLinesByProgramHeaderId(int programHeaderId)
		{
			string sql = "SELECT pl.ProgramLineId, pt.Abbreviation AS ProductTypeAbbreviation, ps.Description AS ProductSizeDescription, "
					+ "          pks.Description as PackStyleDescription, ct.Description AS ContainerTypeDescription, pl.CustomSKU, "
					+ "          b.Description as BrandDescription, v.Description as VariantDescription, "
					+ "          (SELECT pkt.description "
					+ "           FROM PackagingType pkt "
					+ "           WHERE pkt.PackagingTypeId = pl.BagTypeToPackagingTypeId) AS BagTypeToPackagingTypeDescription, "
					+ "          (SELECT pkt.description "
					+ "			  FROM PackagingType pkt "
					+ "			  WHERE pkt.PackagingTypeId = pl.CaseTypeToPackagingTypeId) AS CaseTypeToPackagingTypeDescription, "
					+ "          pl.BagCount, pl.BagsPerCase, pl.AnnualCases, pl.CaseSellingPrice, pl.Pieces, pl.PiecePrice, pl.ContributionMargin, "
					+ "		     pl.ContributionMarginPercent, pl.VariableMargin, pl.VariableMarginPercent, pl.NetSales, pl.CaseCount, "
					+ "          pl.GrossSales, pl.Discounts, pl.Fees, pl.Other, pl.BrokerFees, pl.Scrap, "
					+ "          pl.TotalBags, pl.TotalBoxes, pl.Plates, pl.Pallets, pl.VlOverhead, pl.OceanFreight, pl.CustFreight, "
					+ "			 pl.Depr, pl.BagCost, pl.BoxCost, pl.MaterialPerPad, pl.VLPerPad, pl.FixedCost "
					+ " FROM ProgramLine pl "
					+ "      INNER JOIN ProgramHeader ph ON pl.ProgramHeaderId =  ph.ProgramHeaderId "
					+ "      INNER JOIN ProductType pt ON pl.ProductTypeId =  pt.ProductTypeId "
					+ "      INNER JOIN ProductSize ps ON pl.ProductSizeId = ps.ProductSizeId "
					+ "      INNER JOIN PackStyle pks ON pl.PackStyleId = pks.PackStyleId "
					+ "      INNER JOIN ContainerType ct ON pl.ContainerTypeId = ct.ContainerTypeId "
					+ "      INNER JOIN Brand b ON pl.BrandId = b.BrandId "
					+ "      INNER JOIN Variant v ON pl.VariantId = v.VariantId "
					+ " WHERE ph.ProgramHeaderId = " + programHeaderId.ToString();

			IEnumerable<ProgramLineModel> programLines = _context.Database.SqlQuery<ProgramLineModel>(sql).Select(p => new ProgramLineModel
			{
				ProgramLineId = p.ProgramLineId,
				ProductTypeAbbreviation = p.ProductTypeAbbreviation,
				ProductSizeDescription = p.ProductSizeDescription,
				PackStyleDescription = p.PackStyleDescription,
				ContainerTypeDescription = p.ContainerTypeDescription,
				CustomSKU = p.CustomSKU,
				BagTypeToPackagingTypeDescription = p.BagTypeToPackagingTypeDescription,
				CaseTypeToPackagingTypeDescription = p.CaseTypeToPackagingTypeDescription,
				BrandDescription = p.BrandDescription,
				VariantDescription = p.VariantDescription,
				BagCount = p.BagCount,
				BagsPerCase = p.BagsPerCase,
				AnnualCases = p.AnnualCases,
				CaseSellingPrice = p.CaseSellingPrice,
				Pieces = p.Pieces,
				PiecePrice = p.PiecePrice,
				ContributionMargin = p.ContributionMargin,
				ContributionMarginPercent = p.ContributionMarginPercent,
				VariableMargin = p.VariableMargin,
				VariableMarginPercent = p.VariableMarginPercent,
				NetSales = p.NetSales,
				CaseCount = p.CaseCount,
				GrossSales = p.GrossSales,
				Discounts = p.Discounts,
				Fees = p.Fees,
				Other = p.Other,
				BrokerFees = p.BrokerFees,
				Scrap = p.Scrap,
				TotalBags = p.TotalBags,
				TotalBoxes = p.TotalBoxes,
				Plates = p.Plates,
				Pallets = p.Pallets,
				VlOverhead = p.VlOverhead,
				OceanFreight = p.OceanFreight,
				CustFreight = p.CustFreight,
				Depr = p.Depr,
				BagCost = p.BagCost,
				BoxCost = p.BoxCost,
				MaterialPerPad = p.MaterialPerPad,
				VLPerPad = p.VLPerPad,
				FixedCost = p.FixedCost
			});

			return programLines;
		 }

		public IEnumerable<FeatureOptionModel> GetFeatureOptionsByProgramLineId(int programLineId)
		{
			string sql = "SELECT f.Abbreviation AS FeatureAbbreviation , f.Description AS FeatureDescription , fto.Description AS FeatureToOptionDescription, "
			    + " fto.FeatureToOptionId, ROUND(pstftoc.Cost * pl.Pieces,0) AS FeatureOptionCost "
				+ " FROM ProgramLine AS pl "
				+ " INNER JOIN ProgramFeatureCost AS pfc ON pfc.ProgramLineId = pl.ProgramLineId "
				+ " INNER JOIN ProductSizeToFeatureToOptionCost AS pstftoc ON pstftoc.ProductSizeToFeatureToOptionCostId = pfc.ProductSizeToFeatureToOptionCostId "
				+ " INNER JOIN FeatureToOption AS fto ON fto.FeatureToOptionId = pstftoc.FeatureToOptionId "
				+ " INNER JOIN ProductTypeToFeature AS pttf ON pttf.ProductTypeToFeatureId = fto.ProductTypeToFeatureId "
				+ " INNER JOIN Feature AS f ON f.FeatureId = pttf.FeatureId "
				+ " WHERE pl.ProgramLineId = " + programLineId.ToString();

			IEnumerable<FeatureOptionModel> lineFeatureOptions = _context.Database.SqlQuery<FeatureOptionModel>(sql).Select(p => new FeatureOptionModel
			{
				FeatureAbbreviation = p.FeatureAbbreviation,
				FeatureDescription = p.FeatureDescription,
				FeatureToOptionDescription = p.FeatureToOptionDescription,
				FeatureToOptionId = p.FeatureToOptionId,
				FeatureOptionCost = p.FeatureOptionCost
			});

			return lineFeatureOptions;
		}

		public IEnumerable<Feature> GetFeaturesByProductCategory(int productCategoryId)
		{
			return _context.Feature.Where(f => f.ProductCategoryId == productCategoryId);
		}

		public IEnumerable<SelectModel> GetFeatureOptionList(int featureId, int productTypeId, int productSizeId)
		{
			var result = (from f in _context.Feature
						  join pttf in _context.ProductTypeToFeature on f.FeatureId equals pttf.FeatureId
						  join pt in _context.ProductType on pttf.ProductTypeId equals pt.ProductTypeId
						  join ps in _context.ProductSize on pt.ProductTypeId equals ps.ProductTypeId
						  join fto in _context.FeatureToOption on pttf.ProductTypeToFeatureId equals fto.ProductTypeToFeatureId
						  join pstftoc in _context.ProductSizeToFeatureToOptionCost on new { fto.FeatureToOptionId, ps.ProductSizeId } equals new { pstftoc.FeatureToOptionId, pstftoc.ProductSizeId }
						  where f.FeatureId == featureId && pt.ProductTypeId == productTypeId && ps.ProductSizeId == productSizeId
						  select new { f, pttf, pt, ps, fto, pstftoc })
						  .Select(p => new SelectModel
						  {
							  Value = p.fto.FeatureToOptionId.ToString(),
							  Description = p.fto.Description
						  });

			return result;
		}

		public ProductSizeToFeatureToOptionCostModel GetProductSizeToFeatureToOptionCost(int productSizeId, int featureToOptionId)
		{
			var result = (from pstftoc in _context.ProductSizeToFeatureToOptionCost
						  where pstftoc.ProductSizeId == productSizeId && pstftoc.FeatureToOptionId == featureToOptionId
						  select new { pstftoc })
						  .Select(p => new ProductSizeToFeatureToOptionCostModel
						  {
							  ProductSizeToFeatureToOptionCostId = p.pstftoc.ProductSizeToFeatureToOptionCostId,
							  Cost = p.pstftoc.Cost
						  }).FirstOrDefault();

			return result;
		}

		public void UpdateProgramLineCalcs(int programLineId)
		{
			var paramProgramLineId = new SqlParameter
			{
				ParameterName = "programLineId",
				Value = programLineId
			};

			_context.Database.ExecuteSqlCommand("exec usp_ProgramLineCalcs @programLineId ", paramProgramLineId);
		}

	}
}
