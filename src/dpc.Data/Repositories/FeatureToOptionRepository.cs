﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
    public class FeatureToOptionRepository : GenericRepository<FeatureToOption>, IFeatureToOptionRepository
    {
        public FeatureToOptionRepository(dpcEntities context)
            : base(context)
        {
        }

        public IEnumerable<FeatureToOptionModel> GetFeatureToOptionInfo()
        {
            return (from fto in _context.FeatureToOption
                    join pttf in _context.ProductTypeToFeature on fto.ProductTypeToFeatureId equals pttf.ProductTypeToFeatureId
                    join f in _context.Feature on pttf.FeatureId equals f.FeatureId
                    join pt in _context.ProductType on pttf.ProductTypeId equals pt.ProductTypeId
                    join pc in _context.ProductCategory on f.ProductCategoryId equals pc.ProductCategoryId
                    select new { fto, pttf, f, pt, pc }).Select(x => new FeatureToOptionModel
                    {
                        FeatureToOptionId = x.fto.FeatureToOptionId,
                        ProductTypeToFeatureId = x.fto.ProductTypeToFeatureId,
                        Description = x.fto.Description,
                        FeatureId = x.f.FeatureId,
                        FeatureDescription = x.f.Description,
                        FeatureAbbreviation = x.f.Abbreviation,
                        ProductTypeId = x.pt.ProductTypeId,
                        ProductTypeDescription = x.pt.Description,
                        ProductTypeAbbreviation = x.pt.Abbreviation,
                        ProductCategoryId = x.pc.ProductCategoryId,
                        ProductCategoryDescription = x.pc.Description,
                        ProductCategoryAbbreviation = x.pc.Abbreviation
                    });
        }

        public FeatureToOptionModel GetFeatureToOptionInfoById(int featureToOptionId)
        {
            IEnumerable<FeatureToOptionModel> featureToOptionInfo = GetFeatureToOptionInfo();

            return featureToOptionInfo.Where(p => p.FeatureToOptionId == featureToOptionId).SingleOrDefault();
        }

        public IEnumerable<SelectModel> GetFeatureToOptionListByFeature(int featureId, int productTypeId)
        {
            return (from f in _context.Feature
                    join pttf in _context.ProductTypeToFeature on f.FeatureId equals pttf.FeatureId
                    join fto in _context.FeatureToOption on pttf.ProductTypeToFeatureId equals fto.ProductTypeToFeatureId
                    select new { f, pttf, fto }).Where(p => p.f.FeatureId == featureId && p.pttf.ProductTypeId == productTypeId)
                    .Select(x => new SelectModel
                    {
                        Description = x.fto.Description,
                        Value = x.fto.FeatureToOptionId.ToString()
                    });
        }

        public bool isFeatureToOptionDescriptionExist(int productTypeToFeatureId, string description)
        {
			return _context.FeatureToOption.Where(x => x.ProductTypeToFeatureId == productTypeToFeatureId &&
												  x.Description.ToLower() == description.ToLower()).Any();
		}

		public bool isProductTypeToFeatureExist(int productTypeToFeatureId)
		{
			return _context.FeatureToOption.Where(x => x.ProductTypeToFeatureId == productTypeToFeatureId).Any();
		}
	}
}
