﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
    public class PackagingCostRepository : GenericRepository<PackagingCost>, IPackagingCostRepository
    {
        public PackagingCostRepository(dpcEntities context)
            : base(context)
        {
        }

        public IEnumerable<PackagingCostModel> GetPackagingCostInfo()
        {
            return (from pkc in _context.PackagingCost
                    join qr in _context.QuantityRange on pkc.QuantityRangeId equals qr.QuantityRangeId
                    join pkt in _context.PackagingType on pkc.PackagingTypeId equals pkt.PackagingTypeId
                    join ct in _context.ContainerType on pkt.ContainerTypeId equals ct.ContainerTypeId
                    join ps in _context.ProductSize on pkc.ProductSizeId equals ps.ProductSizeId
                    join pt in _context.ProductType on ps.ProductTypeId equals pt.ProductTypeId
                    join pc in _context.ProductCategory on pt.ProductCategoryId equals pc.ProductCategoryId
                    select new { pkc, qr, pkt, ct, ps, pt, pc }).ToList().Select(x => new PackagingCostModel
                    {
                        PackagingCostId = x.pkc.PackagingCostId,
                        Cost = x.pkc.Cost,
                        QuantityRangeId = x.qr.QuantityRangeId,
                        QuantityRangeItem = x.qr.Minimum.ToString("#,##0") + " - " + x.qr.Maximum.ToString("#,##0"),
                        PackagingTypeId = x.pkt.PackagingTypeId,
                        PackagingTypeDescription = x.pkt.Description,
                        PackagingTypeAbbreviation = x.pkt.Abbreviation,
                        ContainerTypeId = x.ct.ContainerTypeId,
                        ContainerTypeDescription = x.ct.Description,
                        ContainerTypeAbbreviation = x.ct.Abbreviation,
                        ProductSizeId = x.ps.ProductSizeId,
                        ProductSizeDescription = x.ps.Description,
                        ProductSizeAbbreviation = x.ps.Abbreviation,
                        ProductTypeId = x.pt.ProductTypeId,
                        ProductTypeDescription = x.pt.Description,
                        ProductTypeAbbreviation = x.pt.Abbreviation,
                        ProductCategoryId = x.pc.ProductCategoryId,
                        ProductCategoryDescription = x.pc.Description,
                        ProductCategoryAbbreviation = x.pc.Abbreviation
                    })
                    .OrderBy(y => y.ProductCategoryDescription)
                    .ThenBy(y => y.ProductTypeDescription)
                    .ThenBy(y => y.ProductSizeDescription)
                    .ThenBy(y => y.ContainerTypeDescription)
                    .ThenBy(y => y.PackagingTypeDescription)
                    .ThenBy(y => y.QuantityRangeId)
                    .ThenBy(y => y.Cost);
        }

        public PackagingCostModel GetPackagingCostInfoById(int packagingCostId)
        {
            IEnumerable<PackagingCostModel> packagingCostInfo = GetPackagingCostInfo();

            return packagingCostInfo.Where(p => p.PackagingCostId == packagingCostId).SingleOrDefault();
        }

        public bool isPackagingCostExist(int productSizeId, int packagingTypeId, int quantityRangeId)
        {

			return _context.PackagingCost.Where(x => x.ProductSizeId == productSizeId &&
											x.PackagingTypeId == packagingTypeId &&
											x.QuantityRangeId == quantityRangeId).Any();
        }
    }
}
