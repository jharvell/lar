﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class ProgramApprovalRepository : GenericRepository<ProgramApproval>, IProgramApprovalRepository
	{
		public ProgramApprovalRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<ProgramApproval> GetProgramApprovalsByProgramHeaderId(int programHeaderId)
		{
			var result = _context.ProgramApproval.Where(a => a.ProgramHeaderId == programHeaderId).ToList();

			return result;
		}

		//public bool isProgamApprovalSubmitted(int programHeaderId)
		//{
		//	int count;
		//	count = (from a in _context.ProgramApproval
		//			 select new { a })
		//		.Where(x => x.a.ProgramHeaderId == programHeaderId)
		//		.Count();

		//	if (count > 0)
		//	{
		//		return true;
		//	}
		//	return false;
		//}

		public IEnumerable<ProgramApproverModel> GetProgramApproverList(string rolename)
		{
			var result = (from c in _context.AspNetUserClaims
						  join u in _context.AspNetUsers on c.UserId equals u.Id
						  select new { c, u})
						  .Where(x => x.c.ClaimType == "Role" && x.c.ClaimValue == rolename)
						  .Select(a => new ProgramApproverModel
						  {
							  UserId = a.c.UserId,
							  Email = a.u.Email,
							  UserName = a.u.UserName,
							  FirstName = a.u.FirstName,
							  LastName = a.u.LastName
						  });

			return result;
		}

		public IEnumerable<ProgramApproverModel> GetProgramApproverListByProgramHeaderId(string rolename, int programHeaderId)
		{
			var result = (from c in _context.AspNetUserClaims
						  join u in _context.AspNetUsers on c.UserId equals u.Id
						  join p in _context.ProgramApproval on u.Id equals p.ApproverUserId
						  select new { c, u, p })
						  .Where(x => x.c.ClaimType == "Role" && x.c.ClaimValue == rolename && x.p.ProgramHeaderId == programHeaderId)
						  .Select(a => new ProgramApproverModel
						  {
							  UserId = a.c.UserId,
							  Email = a.u.Email,
							  UserName = a.u.UserName,
							  FirstName = a.u.FirstName,
							  LastName = a.u.LastName,
							  ProgramApprovalId = a.p.ProgramApprovalId,
							  Approved = a.p.Approved,
							  Comment = a.p.Comment,
							  CommentOn = a.p.CommentOn
						  });

			return result;
		}

		public ProgramApproval GetProgramApprovalByUserId(int programHeaderId, string userId)
		{
			ProgramApproval programApproval = _context.ProgramApproval.Where(a => a.ProgramHeaderId == programHeaderId && a.ApproverUserId == userId).FirstOrDefault();

			return programApproval;
		}
	}
}
