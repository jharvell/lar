﻿using dpc.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class PasswordHistoryRepository : GenericRepository<PasswordHistory>, IPasswordHistoryRepository
	{
		public PasswordHistoryRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<string> GetPasswordHistory(string userId)
		{
			return _context.PasswordHistory
				.Where(p => p.UserId == userId)
				.OrderByDescending(p => p.CreateDate)
				.Select(p => p.PasswordHash)
				.ToList();
		}
		public DateTime GetLastSavedPasswordDate(string userId)
		{
			return _context.PasswordHistory
				.Where(p => p.UserId == userId)
				.DefaultIfEmpty()
				.Max(p => p.CreateDate == null ? DateTime.MinValue : p.CreateDate);
		}
	}
}
