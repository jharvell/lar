﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
	public class PackagingTypeRepository : GenericRepository<PackagingType>, IPackagingTypeRepository
	{
		public PackagingTypeRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<PackagingTypeModel> GetPackagingTypeInfo()
		{
			return (from pkt in _context.PackagingType
					join ct in _context.ContainerType on pkt.ContainerTypeId equals ct.ContainerTypeId
					select new { pkt, ct }).Select(p => new PackagingTypeModel
					{
						PackagingTypeId = p.pkt.PackagingTypeId,
						Abbreviation = p.pkt.Abbreviation,
						Description = p.pkt.Description,
						ContainerTypeId = p.ct.ContainerTypeId,
						ContainerTypeAbbreviation = p.ct.Abbreviation,
						ContainerTypeDescription = p.ct.Description
					});
		}

		public IEnumerable<SelectModel> GetPackagingTypeListByContainerTypeProductSize(int containerTypeId, int productSizeId)
		{
			string sql = "SELECT pkt.Description, CONVERT(varchar(10), PackagingTypeId) AS Value  "
						 + " FROM PackagingType AS pkt "
						 + " WHERE pkt.PackagingTypeId IN ( "
						 + "      SELECT DISTINCT PackagingTypeId "
						 + "      FROM vBagBoxCost "
						 + "      WHERE ContainerTypeId = " + containerTypeId.ToString() + " AND ProductSizeId = " + productSizeId.ToString()
						 + " )";

			IEnumerable<SelectModel> list = _context.Database.SqlQuery<SelectModel>(sql).Select(p => new SelectModel
			{
				Description = p.Description,
				Value = p.Value
			});

			return list;
		}

		public bool isPackagingTypeExist(int containerTypeId, string description, string abbreviation)
		{
			return _context.PackagingType.Where(x => x.ContainerTypeId == containerTypeId &&
											x.Description.ToLower() == description.ToLower() &&
											x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
		}
	}
}
