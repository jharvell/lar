﻿using System.Linq;
using dpc.Data.Interfaces;

namespace dpc.Data.Repositories
{
	public class ProgramFeatureCostRepository : GenericRepository<ProgramFeatureCost>, IProgramFeatureCostRepository
	{
		public ProgramFeatureCostRepository(dpcEntities context)
			: base(context)
		{
		}

		public bool IsProductSizeToFeatureToOptionCostInProgramFeatureCost(int productSizeToFeatureToOptionCostId)
		{
			return _context.ProgramFeatureCost.Any(x => x.ProductSizeToFeatureToOptionCostId == productSizeToFeatureToOptionCostId);
		}

		public void DeleteProgramFeatureCostByProgramLineId(int programLineId)
		{
			var rows = _context.ProgramFeatureCost.Where(p => p.ProgramLineId == programLineId);
			_context.ProgramFeatureCost.RemoveRange(rows);
			_context.SaveChanges();
		}
	}
}