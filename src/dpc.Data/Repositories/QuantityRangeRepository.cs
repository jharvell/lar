﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Data.Repositories
{
    public class QuantityRangeRepository : GenericRepository<QuantityRange>, IQuantityRangeRepository
    {
		public QuantityRangeRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<QuantityRangeModel> GetQuantityRangeInfo()
		{
			return (from qr in _context.QuantityRange
					join pc in _context.ProductCategory on qr.ProductCategoryId equals pc.ProductCategoryId
					join ct in _context.ContainerType on qr.ContainerTypeId equals ct.ContainerTypeId
					select new { qr, pc, ct }).Select(p => new QuantityRangeModel
					{
						QuantityRangeId = p.qr.QuantityRangeId,
						Minimum = p.qr.Minimum,
						Maximum = p.qr.Maximum,
						ContainerTypeId = p.ct.ContainerTypeId,
						ContainerTypeDescription = p.ct.Description,
						ContainerTypeAbbreviation = p.ct.Abbreviation,
						ProductCategoryId = p.pc.ProductCategoryId,
						ProductCategoryDescription = p.pc.Description,
						ProductCategoryAbbreviation = p.pc.Abbreviation
					});
		}

		public bool isQuantityRangeExist(int productCategoryId, int containerTypeId, int minimum, int maximum)
		{
			return _context.QuantityRange.Where(x => x.ProductCategoryId == productCategoryId &&
										   x.ContainerTypeId == containerTypeId &&
										   x.Minimum == minimum &&
										   x.Maximum == maximum).Any();
		}
	}
}
