﻿using System.Collections.Generic;
using dpc.Data.Interfaces;
using System.Linq;
using dpc.Data.CustomModels;

namespace dpc.Data.Repositories
{
	public class PackStyleRepository : GenericRepository<PackStyle>, IPackStyleRepository
	{
		public PackStyleRepository(dpcEntities context)
			: base(context)
		{
		}

		public IEnumerable<PackStyleModel> GetPackStyleInfo()
		{
			return (from pks in _context.PackStyle
					join pt in _context.ProductType on pks.ProductTypeId equals pt.ProductTypeId
					join pc in _context.ProductCategory on pt.ProductCategoryId equals pc.ProductCategoryId
					select new { pks, pt, pc }).Select(x => new PackStyleModel
					{
						PackStyleId = x.pks.PackStyleId,
						Abbreviation = x.pks.Abbreviation,
						Description = x.pks.Description,
						ProductTypeId = x.pt.ProductTypeId,
						ProductTypeAbbreviation = x.pt.Abbreviation,
						ProductTypeDescription = x.pt.Description,
						ProductCategoryId = x.pc.ProductCategoryId,
						ProductCategoryAbbreviation = x.pc.Abbreviation,
						ProductCategoryDescription = x.pc.Description
					});
		}

		public PackStyleModel GetPackStyleInfoById(int packStyleId)
		{
			IEnumerable<PackStyleModel> packStyleInfo = GetPackStyleInfo();

			return packStyleInfo.Where(p => p.PackStyleId == packStyleId).SingleOrDefault();
		}

		public bool isPackStyleInProgramLine(int packStyleId)
		{
			return _context.ProgramLine.Where(x => x.PackStyleId == packStyleId).Any();
		}

		public bool isPackStyleExist(int productTypeId, string description, string abbreviation)
		{
			return _context.PackStyle.Where(x => x.ProductTypeId == productTypeId && 
											x.Description.ToLower() == description.ToLower() && 
											x.Abbreviation.ToLower() == abbreviation.ToLower()).Any();
		}
	}
}
