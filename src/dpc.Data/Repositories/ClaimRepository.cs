﻿using dpc.Data.CustomModels;
using dpc.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace dpc.Data.Repositories
{
	public class ClaimRepository : GenericRepository<AspNetUserClaims>, IClaimRepository
	{
		public ClaimRepository(dpcEntities context)
			: base(context)
		{
		}

		public bool isRoleInClaim(string roleName)
		{
			var count =
					(from c in _context.AspNetUserClaims
					 select new { c })
					.Where(x => x.c.ClaimType == "Role" && x.c.ClaimValue == roleName)
					.Count();

			if (count > 0)
			{
				return true;
			}
			return false;
		}

	}
}
