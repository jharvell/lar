﻿using dpc.Data.Interfaces;

namespace dpc.Data
{
	public interface IUnitOfWork
	{
		//User
		IClaimRepository ClaimRepository { get; }
		IPasswordHistoryRepository PasswordHistoryRepository { get; }

		//Maintenance
		IBrandOwnerRepository BrandOwnerRepository { get; }
		IBrandRepository BrandRepository { get; }
		IContainerTypeRepository ContainerTypeRepository { get; }
		ICustomerRepository CustomerRepository { get; }
		IFeatureRepository FeatureRepository { get; }
		IFeatureToOptionRepository FeatureToOptionRepository { get; }
		IMfgCostRepository MfgCostRepository { get; }
		IPackagingCostRepository PackagingCostRepository { get; }
		IPackagingTypeRepository PackagingTypeRepository { get; }
		IPackStyleRepository PackStyleRepository { get; }
		IProductCategoryRepository ProductCategoryRepository { get; }
		IProductTypeRepository ProductTypeRepository { get; }
		IProductSizeRepository ProductSizeRepository { get; }
		IProductTypeToFeatureRepository ProductTypeToFeatureRepository { get; }
		IProductSizeToFeatureToOptionCostRepository ProductSizeToFeatureToOptionCostRepository { get; }
		IQuantityRangeRepository QuantityRangeRepository { get; }
		IVariantRepository VariantRepository { get; }

		//Program
		IProgramApprovalRepository ProgramApprovalRepository { get; }
		IProgramFeatureCostRepository ProgramFeatureCostRepository { get; }
		IProgramHeaderRepository ProgramHeaderRepository { get; }
		IProgramLineRepository ProgramLineRepository { get; }

		int Save();
	}
}
