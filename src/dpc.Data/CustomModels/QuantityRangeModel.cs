﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
	public class QuantityRangeModel
    {
        public int QuantityRangeId { get; set; }

        public int ProductCategoryId { get; set; }

        public int ContainerTypeId { get; set; }

        public int Minimum { get; set; }

        public int Maximum { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

		public string ContainerTypeDescription { get; set; }

        public string ContainerTypeAbbreviation { get; set; }

		public IEnumerable<SelectModel> ContainerTypeList { get; set; }
		
		public string ProductCategoryDescription { get; set; }

		public string ProductCategoryAbbreviation { get; set; }

        public IEnumerable<SelectModel> ProductCategoryList { get; set; }
	}
}