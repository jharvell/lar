﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
    public class ProductSizeModel
	{
		public int ProductSizeId { get; set; }

		public string Abbreviation { get; set; }
		
		public string Description { get; set; }

        public System.DateTime UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public int ProductTypeId { get; set; }

		public string ProductTypeAbbreviation { get; set; }

		public string ProductTypeDescription { get; set; }

		public IEnumerable<SelectModel> ProductTypeList { get; set; }

		public int ProductCategoryId { get; set; }

		public string ProductCategoryAbbreviation { get; set; }

        public string ProductCategoryDescription { get; set; }

        public IEnumerable<SelectModel> ProductCategoryList { get; set; }
	}
}