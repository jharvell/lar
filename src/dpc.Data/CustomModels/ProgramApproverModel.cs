﻿namespace dpc.Data.CustomModels
{
	public class ProgramApproverModel
	{
		public string UserId { get; set; }

		public string Email { get; set; }

		public string UserName { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public int ProgramApprovalId { get; set; }

		public bool? Approved { get; set; }

		public string Comment { get; set; }

		public System.DateTime? CommentOn { get; set; }

		private bool _isReadOnly = true;
		public bool isReadOnly
		{
			get { return _isReadOnly; }
			set { _isReadOnly = value; }
		}
	}
}