﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
    public class MfgCostModel
	{
		public int MfgCostId { get; set; }

		public double VLandOverhead { get; set; }

		public double FixedConversionCost { get; set; }

		public double Depreciation { get; set; }

		public double Scrap { get; set; }

		public System.DateTime UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }

		public int ProductTypeId { get; set; }

		public string ProductTypeAbbreviation { get; set; }

		public string ProductTypeDescription { get; set; }

		public IEnumerable<SelectModel> ProductTypeList { get; set; }

		public int ProductCategoryId { get; set; }

		public string ProductCategoryAbbreviation { get; set; }

		public string ProductCategoryDescription { get; set; }

		public IEnumerable<SelectModel> ProductCategoryList { get; set; }
	}
}