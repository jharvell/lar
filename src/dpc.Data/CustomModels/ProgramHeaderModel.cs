﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dpc.Data.CustomModels
{
	public class ProgramHeaderModel
	{
		public int ProgramHeaderId { get; set; }
		
		public int ProductCategoryId { get; set; }

		public int ApprovalType { get; set; }

		public string BusinessUnit { get; set; }
		
		public string Title { get; set; }

		public int BrandOwnerId { get; set; }

		public string BrandOwnerDescription { get; set; }

		public IEnumerable<SelectModel> BrandOwnerList { get; set; }

		public string Status { get; set; }

		public DateTime? RequestedShipDate { get; set; }

		public DateTime? ContractEndDate { get; set; }

		public string Comments { get; set; }

		public string TransitionType { get; set; }

		public IEnumerable<SelectModel> TransitionTypeList { get; set; }

		public double? Discounts { get; set; }

		public double? FeesPromotions { get; set; }

		public double? BrokerFees { get; set; }

		public double? ReturnsDamage { get; set; }

		public bool Pallets { get; set; }

		public IEnumerable<SelectModel> PalletsList { get; set; }

		public string FreightTerms { get; set; }

		public IEnumerable<SelectModel> FreightTermsList { get; set; }

		public double? FreightPerPad { get; set; }

		public double? OceanFreight { get; set; }

		public bool NewProgram { get; set; }

		public IEnumerable<SelectModel> NewProgramList { get; set; }

		public double? ProgramFee { get; set; }

		public string ProgramFeeDescription { get; set; }

		public double? Pieces { get; set; }

		public double? Bags { get; set; }

		public double? Cases { get; set; }

		public double? ContributionMargin { get; set; }

		public double? ContributionMarginPercent { get; set; }

		public double? ContributionMarginPerPiece { get; set; }

		public double? VariableMargin { get; set; }

		public double? VariableMarginPercent { get; set; }

		public double? VariableMarginPerPiece { get; set; }

		public double? NetSales { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

		public System.DateTime CreatedOn { get; set; }

		public string CreatedBy { get; set; }

		public Nullable<System.DateTime> ApprovalSubmittedOn { get; set; }

		public string ApprovalSubmittedBy { get; set; }

		public Nullable<System.DateTime> ApprovedOn { get; set; }

		public bool isProgramLinesExist { get; set; }

		public bool isProgramSubmitted { get; set; }

		public bool isProgramApproved { get; set; }

		public bool isProgramRejected { get; set; }
	}
}