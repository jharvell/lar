﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
    public class ProductSizeToFeatureToOptionCostModel
    {
		public int ProductSizeToFeatureToOptionCostId { get; set; }
		
		public double Cost { get; set; }

        public System.DateTime UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public int ProductTypeToFeatureId { get; set; }

        public int FeatureToOptionId { get; set; }

        public string FeatureToOptionDescription { get; set; }

        public IEnumerable<SelectModel> FeatureToOptionList { get; set; }

        public int FeatureId { get; set; }

        public string FeatureDescription { get; set; }

        public string FeatureAbbreviation { get; set; }

        public IEnumerable<SelectModel> FeatureList { get; set; }

        public int ProductSizeId { get; set; }

        public string ProductSizeDescription { get; set; }

        public string ProductSizeAbbreviation { get; set; }

        public IEnumerable<SelectModel> ProductSizeList { get; set; }

        public int ProductTypeId { get; set; }

		public string ProductTypeDescription { get; set; }

		public string ProductTypeAbbreviation { get; set; }

		public IEnumerable<SelectModel> ProductTypeList { get; set; }

		public int ProductCategoryId { get; set; }

        public string ProductCategoryDescription { get; set; }

		public string ProductCategoryAbbreviation { get; set; }

        public IEnumerable<SelectModel> ProductCategoryList { get; set; }
	}
}