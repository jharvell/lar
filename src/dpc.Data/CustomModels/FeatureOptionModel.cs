﻿namespace dpc.Data.CustomModels
{
	public class FeatureOptionModel
	{
		public string FeatureAbbreviation { get; set; }

		public string FeatureDescription { get; set; }

		public string FeatureToOptionDescription { get; set; }

		public int FeatureToOptionId { get; set; }

		public double FeatureOptionCost { get; set; }
	}
}