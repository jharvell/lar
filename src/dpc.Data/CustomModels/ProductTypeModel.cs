﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
    public class ProductTypeModel
	{
		public int ProductTypeId { get; set; }

        public int ProductCategoryId { get; set; }
		
		public string Description { get; set; }

		public string Abbreviation { get; set; }

        public System.DateTime UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public string ProductCategoryDescription { get; set; }

        public string ProductCategoryAbbreviation { get; set; }

        public IEnumerable<SelectModel> ProductCategoryList { get; set; }
    }
}