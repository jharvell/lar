﻿namespace dpc.Data.CustomModels
{
	public class SelectModel
	{
		public string Description { get; set; }

		public string Value { get; set; }
	}
}