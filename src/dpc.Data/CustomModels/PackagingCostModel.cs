﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
    public class PackagingCostModel
    {
		public int PackagingCostId { get; set; }

		public double Cost { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

        public int QuantityRangeId { get; set; }

        public string QuantityRangeItem { get; set; }

        public IEnumerable<SelectModel> QuantityRangeList { get; set; }

        public int PackagingTypeId { get; set; }
        
        public string PackagingTypeDescription { get; set; }
        
        public string PackagingTypeAbbreviation { get; set; }

        public IEnumerable<SelectModel> PackagingTypeList { get; set; }

        public int ContainerTypeId { get; set; }

        public string ContainerTypeDescription { get; set; }

        public string ContainerTypeAbbreviation { get; set; }

        public IEnumerable<SelectModel> ContainerTypeList { get; set; }

        public int ProductSizeId { get; set; }
        
        public string ProductSizeDescription { get; set; }
        
        public string ProductSizeAbbreviation { get; set; }

        public IEnumerable<SelectModel> ProductSizeList { get; set; }

        public int ProductTypeId { get; set; }
        
		public string ProductTypeDescription { get; set; }
        
		public string ProductTypeAbbreviation { get; set; }

		public IEnumerable<SelectModel> ProductTypeList { get; set; }

		public int ProductCategoryId { get; set; }
        
		public string ProductCategoryDescription { get; set; }
        
		public string ProductCategoryAbbreviation { get; set; }

		public IEnumerable<SelectModel> ProductCategoryList { get; set; }
	}
}