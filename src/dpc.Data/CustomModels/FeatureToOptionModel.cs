﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
    public class FeatureToOptionModel
    {
		public int FeatureToOptionId { get; set; }

        public int ProductTypeToFeatureId { get; set; }
		
		public string Description { get; set; }

        public System.DateTime UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public int FeatureId { get; set; }

        public string FeatureDescription { get; set; }

        public string FeatureAbbreviation { get; set; }

        public IEnumerable<SelectModel> FeatureList { get; set; }

        public int ProductTypeId { get; set; }

		public string ProductTypeDescription { get; set; }

		public string ProductTypeAbbreviation { get; set; }

		public IEnumerable<SelectModel> ProductTypeList { get; set; }

		public int ProductCategoryId { get; set; }

        public string ProductCategoryDescription { get; set; }

		public string ProductCategoryAbbreviation { get; set; }

        public IEnumerable<SelectModel> ProductCategoryList { get; set; }
	}
}