﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
	public class ProgramLineModel
	{
		public int ProgramLineId { get; set; }

		public int ProgramHeaderId { get; set; }

		public int ProductTypeId { get; set; }

		public string ProductTypeAbbreviation { get; set; }

		public string ProductTypeDescription { get; set; }

		public int ProductSizeId { get; set; }

		public string ProductSizeDescription { get; set; }

		public int ContainerTypeId { get; set; }

		public string ContainerTypeDescription { get; set; }

		public int PackagingTypeId { get; set; }

		public string PackagingTypeDescription { get; set; }

		public int BagTypeToPackagingTypeId { get; set; }

		public string BagTypeToPackagingTypeDescription { get; set; }

		public int CaseTypeToPackagingTypeId { get; set; }

		public string CaseTypeToPackagingTypeDescription { get; set; }

		public int BrandId { get; set; }

		public string BrandDescription { get; set; }

		public int PackStyleId { get; set; }

		public string PackStyleDescription { get; set; }

		public int VariantId { get; set; }

		public string VariantDescription { get; set; }

		public string CustomSKU { get; set; }

		public int BagCount { get; set; }

		public int BagsPerCase { get; set; }

		public int AnnualCases { get; set; }

		public double CaseSellingPrice { get; set; }

		public double? Pieces { get; set; }

		public double PiecePrice { get; set; }

		public double? ContributionMargin { get; set; }

		public double? ContributionMarginPercent { get; set; }

		public double? VariableMargin { get; set; }

		public double? VariableMarginPercent { get; set; }

		public double? NetSales { get; set; }

		public System.DateTime UpdatedOn { get; set; }

		public string UpdatedBy { get; set; }

		public int CaseCount { get; set; }

		public double? GrossSales { get; set; }

		public double? Discounts { get; set; }

		public double? Fees { get; set; }

		public double? Other { get; set; }

		public double? BrokerFees { get; set; }

		public double? Scrap { get; set; }

		public double? TotalBags { get; set; }

		public double? TotalBoxes { get; set; }

		public double? Plates { get; set; }

		public double? Pallets { get; set; }

		public double? VlOverhead { get; set; }

		public double? OceanFreight { get; set; }

		public double? CustFreight { get; set; }

		public double? Depr { get; set; }

		public double? BagCost { get; set; }

		public double? BoxCost { get; set; }

		public double? MaterialPerPad { get; set; }

		public double? VLPerPad { get; set; }

		public double? FixedCost { get; set; }

		public IEnumerable<FeatureOptionModel> FeatureOptions { get; set; }
	}
}
