﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
    public class PackagingTypeModel
	{
        public int PackagingTypeId { get; set; }

        public int ContainerTypeId { get; set; }

        public string Description { get; set; }

        public string Abbreviation { get; set; }

        public System.DateTime UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }
        
        public string ContainerTypeDescription { get; set; }

        public string ContainerTypeAbbreviation { get; set; }

        public IEnumerable<SelectModel> ContainerTypeList { get; set; }
    }
}