﻿using System.Collections.Generic;

namespace dpc.Data.CustomModels
{
    public class BrandModel
	{
		public int BrandId { get; set; }

        public int BrandOwnerId { get; set; }
		
		public string Description { get; set; }

		public string Abbreviation { get; set; }

        public System.DateTime UpdatedOn { get; set; }

        public string UpdatedBy { get; set; }

        public string BrandOwnerDescription { get; set; }

		public string BrandOwnerAbbreviation { get; set; }

		public IEnumerable<SelectModel> BrandOwnerList { get; set; }
    }
}