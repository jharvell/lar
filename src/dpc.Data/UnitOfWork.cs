﻿using dpc.Data.Interfaces;
using dpc.Data.Repositories;
using System;

namespace dpc.Data
{
	public class UnitOfWork : IDisposable, IUnitOfWork
	{
		private dpcEntities _context = new dpcEntities();

		//User
		private IClaimRepository _claimRepository;
		private IPasswordHistoryRepository _passwordHistoryRepository;

		//Maintenance
		private IBrandRepository _brandRepository;
		private IBrandOwnerRepository _brandOwnerRepository;
		private IContainerTypeRepository _containerTypeRepository;
		private ICustomerRepository _customerRepository;
		private IFeatureRepository _featureRepository;
		private IFeatureToOptionRepository _featureToOptionRepository;
		private IMfgCostRepository _mfgCostRepository;
		private IPackagingCostRepository _packagingCostRepository;
		private IPackagingTypeRepository _packagingTypeRepository;
		private IPackStyleRepository _packStyleRepository;
		private IProductCategoryRepository _productCategoryRepository;
		private IProductSizeRepository _productSizeRepository;
		private IProductSizeToFeatureToOptionCostRepository _productSizeToFeatureToOptionCostRepository;
		private IProductTypeRepository _productTypeRepository;
		private IProductTypeToFeatureRepository _productTypeToFeatureRepository;
		private IQuantityRangeRepository _quantityRangeRepository;
		private IVariantRepository _variantRepository;

		//Program
		private IProgramApprovalRepository _programApprovalRepository;
		private IProgramFeatureCostRepository _programFeatureCostRepository;
		private IProgramHeaderRepository _programHeaderRepository;
		private IProgramLineRepository _programLineRepository;

		
		#region User
		public virtual IClaimRepository ClaimRepository => _claimRepository ?? (_claimRepository = new ClaimRepository(_context));

		public virtual IPasswordHistoryRepository PasswordHistoryRepository => _passwordHistoryRepository ?? (_passwordHistoryRepository = new PasswordHistoryRepository(_context));
		#endregion User

		#region Maintenance
		public virtual IBrandOwnerRepository BrandOwnerRepository => _brandOwnerRepository ?? (_brandOwnerRepository = new BrandOwnerRepository(_context));

		public virtual IBrandRepository BrandRepository => _brandRepository ?? (_brandRepository = new BrandRepository(_context));

		public virtual IContainerTypeRepository ContainerTypeRepository => _containerTypeRepository ?? (_containerTypeRepository = new ContainerTypeRepository(_context));

		public virtual ICustomerRepository CustomerRepository => _customerRepository ?? (_customerRepository = new CustomerRepository(_context));

		public virtual IFeatureRepository FeatureRepository => _featureRepository ?? (_featureRepository = new FeatureRepository(_context));

		public virtual IFeatureToOptionRepository FeatureToOptionRepository => _featureToOptionRepository ?? (_featureToOptionRepository = new FeatureToOptionRepository(_context));

		public virtual IMfgCostRepository MfgCostRepository => _mfgCostRepository ?? (_mfgCostRepository = new MfgCostRepository(_context));

		public virtual IPackagingCostRepository PackagingCostRepository => _packagingCostRepository ?? (_packagingCostRepository = new PackagingCostRepository(_context));

		public virtual IPackagingTypeRepository PackagingTypeRepository => _packagingTypeRepository ?? (_packagingTypeRepository = new PackagingTypeRepository(_context));

		public virtual IPackStyleRepository PackStyleRepository => _packStyleRepository ?? (_packStyleRepository = new PackStyleRepository(_context));

		public virtual IProductCategoryRepository ProductCategoryRepository => _productCategoryRepository ?? (_productCategoryRepository = new ProductCategoryRepository(_context));

		public virtual IProductSizeRepository ProductSizeRepository => _productSizeRepository ?? (_productSizeRepository = new ProductSizeRepository(_context));

		public virtual IProductSizeToFeatureToOptionCostRepository ProductSizeToFeatureToOptionCostRepository => _productSizeToFeatureToOptionCostRepository ?? (_productSizeToFeatureToOptionCostRepository = new ProductSizeToFeatureToOptionCostRepository(_context));

		public virtual IProductTypeRepository ProductTypeRepository => _productTypeRepository ?? (_productTypeRepository = new ProductTypeRepository(_context));

		public virtual IProductTypeToFeatureRepository ProductTypeToFeatureRepository => _productTypeToFeatureRepository ?? (_productTypeToFeatureRepository = new ProductTypeToFeatureRepository(_context));

		public virtual IQuantityRangeRepository QuantityRangeRepository => _quantityRangeRepository ?? (_quantityRangeRepository = new QuantityRangeRepository(_context));
		public virtual IVariantRepository VariantRepository => _variantRepository ?? (_variantRepository = new VariantRepository(_context));
		#endregion Maintenance

		#region Program
		public virtual IProgramApprovalRepository ProgramApprovalRepository => _programApprovalRepository ?? (_programApprovalRepository = new ProgramApprovalRepository(_context));

		public virtual IProgramFeatureCostRepository ProgramFeatureCostRepository => _programFeatureCostRepository ?? (_programFeatureCostRepository = new ProgramFeatureCostRepository(_context));
		public virtual IProgramHeaderRepository ProgramHeaderRepository => _programHeaderRepository ?? (_programHeaderRepository = new ProgramHeaderRepository(_context));

		public virtual IProgramLineRepository ProgramLineRepository => _programLineRepository ?? (_programLineRepository = new ProgramLineRepository(_context));
		#endregion Program

		#region Misc.
		public virtual int Save()
		{
			return _context.SaveChanges();
		}

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				_context.Dispose();
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion Misc.
	}
}
