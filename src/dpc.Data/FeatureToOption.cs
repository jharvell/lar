//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace dpc.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class FeatureToOption
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FeatureToOption()
        {
            this.ProductSizeToFeatureToOptionCost = new HashSet<ProductSizeToFeatureToOptionCost>();
        }
    
        public int FeatureToOptionId { get; set; }
        public int ProductTypeToFeatureId { get; set; }
        public string Description { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }
    
        public virtual ProductTypeToFeature ProductTypeToFeature { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductSizeToFeatureToOptionCost> ProductSizeToFeatureToOptionCost { get; set; }
    }
}
