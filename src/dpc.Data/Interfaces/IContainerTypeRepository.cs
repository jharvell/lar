﻿namespace dpc.Data.Interfaces
{
	public interface IContainerTypeRepository : IGenericRepository<ContainerType>
	{
		bool isContainerTypeExist(string description, string abbreviation);
    }
}
