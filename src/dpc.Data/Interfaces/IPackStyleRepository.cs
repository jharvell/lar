﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
    public interface IPackStyleRepository : IGenericRepository<PackStyle>
    {
		IEnumerable<PackStyleModel> GetPackStyleInfo();

	    PackStyleModel GetPackStyleInfoById(int packStyleId);

	    bool isPackStyleInProgramLine(int PackStyleId);

		bool isPackStyleExist(int productTypeId, string abbreviation, string description);
    }
}
