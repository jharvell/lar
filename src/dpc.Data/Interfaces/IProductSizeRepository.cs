﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
	public interface IProductSizeRepository : IGenericRepository<ProductSize>
	{
		IEnumerable<ProductSizeModel> GetProductSizeInfo();

		ProductSizeModel GetProductSizeInfoById(int productSizeId);

        bool isProductSizeExist(int productTypeId, string description, string abbreviation);

    }
}
