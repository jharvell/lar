﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
    public interface IPackagingTypeRepository : IGenericRepository<PackagingType>
    {
        IEnumerable<PackagingTypeModel> GetPackagingTypeInfo();

	    IEnumerable<SelectModel> GetPackagingTypeListByContainerTypeProductSize(int containerTypeId, int productSizeId);

		bool isPackagingTypeExist(int ContainerTypeId, string description, string abbreviation);
    }
}
