﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
    public interface IProgramHeaderRepository : IGenericRepository<ProgramHeader>
    {
	    IEnumerable<ProgramHeaderModel> GetProgramHeaderInfo();

	    ProgramHeaderModel GetProgramHeaderInfoById(int programHeaderId);

		IEnumerable<SelectModel> GetTransitionTypeList();

        IEnumerable<SelectModel> GetYesNoList();

        IEnumerable<SelectModel> GetFreightTermsList();

        void UpdateProgramHeaderCalcs(int programHeaderId);

        void UpdateAllCalcs(int programHeaderId);
    }
}