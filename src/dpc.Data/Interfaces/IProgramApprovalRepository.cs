﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
	public interface IProgramApprovalRepository : IGenericRepository<ProgramApproval>
	{
		IEnumerable<ProgramApproval> GetProgramApprovalsByProgramHeaderId(int programHeaderId);

		//bool isProgamApprovalSubmitted(int programHeaderId);

		IEnumerable<ProgramApproverModel> GetProgramApproverList(string rolename);

		IEnumerable<ProgramApproverModel> GetProgramApproverListByProgramHeaderId(string rolename, int programHeaderId);

		ProgramApproval GetProgramApprovalByUserId(int programHeaderId, string userId);
	}
}
