﻿namespace dpc.Data.Interfaces
{
    public interface IProductTypeToFeatureRepository : IGenericRepository<ProductTypeToFeature>
    {
        ProductTypeToFeature GetProductTypeToFeatureByKeys(int productTypeId, int featureId);
    }
}
