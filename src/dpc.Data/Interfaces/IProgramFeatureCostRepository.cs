﻿namespace dpc.Data.Interfaces
{
	public interface IProgramFeatureCostRepository : IGenericRepository<ProgramFeatureCost>
	{
		bool IsProductSizeToFeatureToOptionCostInProgramFeatureCost(int productSizeToFeatureToOptionCostId);

		void DeleteProgramFeatureCostByProgramLineId(int programLineId);
	}
}