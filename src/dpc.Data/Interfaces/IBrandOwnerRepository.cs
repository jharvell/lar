﻿namespace dpc.Data.Interfaces
{
	public interface IBrandOwnerRepository : IGenericRepository<BrandOwner>
    {
	    bool isBrandOwnerInProgramHeader(int BrandOwnerId);

	    bool isBrandOwnerExist(string description, string abbreviation);

    }
}
