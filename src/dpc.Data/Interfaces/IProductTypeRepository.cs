﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
	public interface IProductTypeRepository : IGenericRepository<ProductType>
	{
		IEnumerable<ProductTypeModel> GetProductTypeInfo();

        bool isProductTypeExist(int productTypeId, string description, string abbreviation);
    }
}
