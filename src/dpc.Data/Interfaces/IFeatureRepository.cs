﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
    public interface IFeatureRepository : IGenericRepository<Feature>
    {
		IEnumerable<FeatureModel> GetFeatureInfo();

        IEnumerable<SelectModel> GetFeatureListByProductType(int productTypeId);

        IEnumerable<SelectModel> GetFeatureListByProductCategory(int productCategoryId);

        bool isFeatureExist(int productCategoryId, string abbreviation, string description);
    }
}
