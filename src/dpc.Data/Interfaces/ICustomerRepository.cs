﻿namespace dpc.Data.Interfaces
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    { 
    }
}