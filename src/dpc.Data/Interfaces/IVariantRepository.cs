﻿namespace dpc.Data.Interfaces
{
	public interface IVariantRepository : IGenericRepository<Variant>
    {
	    bool isVariantInProgramLine(int VariantId);

	    bool isVariantExist(string description, string abbreviation);

    }
}
