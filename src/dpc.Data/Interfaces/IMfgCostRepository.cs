﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
    public interface IMfgCostRepository : IGenericRepository<MfgCost>
    {
	    IEnumerable<MfgCostModel> GetMfgCostInfo();

		bool isMfgCostExist(int productTypeId);
    }
}
