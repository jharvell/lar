﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
	public interface IProgramLineRepository : IGenericRepository<ProgramLine>
	{
		IEnumerable<ProgramLineModel> GetProgramLinesByProgramHeaderId(int id);

		IEnumerable<Feature> GetFeaturesByProductCategory(int productCategoryId);

		IEnumerable<SelectModel> GetFeatureOptionList(int featureId, int productTypeId, int productSizeId);

		ProductSizeToFeatureToOptionCostModel GetProductSizeToFeatureToOptionCost(int productSizeId, int featureToOptionId);

		IEnumerable<FeatureOptionModel> GetFeatureOptionsByProgramLineId(int programLineId);

		ProgramLineModel GetGridProgramLineById(int programLineId);

		void UpdateProgramLineCalcs(int programLineId);

	}
}