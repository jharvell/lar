﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
	public interface IFeatureToOptionRepository : IGenericRepository<FeatureToOption>
	{
		IEnumerable<FeatureToOptionModel> GetFeatureToOptionInfo();

		FeatureToOptionModel GetFeatureToOptionInfoById(int featureToOptionId);

		IEnumerable<SelectModel> GetFeatureToOptionListByFeature(int featureId, int productTypeId);

		bool isFeatureToOptionDescriptionExist(int productTypeToFeatureId, string description);

		bool isProductTypeToFeatureExist(int productTypeToFeatureId);
	}
}
