﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
    public interface IPackagingCostRepository : IGenericRepository<PackagingCost>
	{
		IEnumerable<PackagingCostModel> GetPackagingCostInfo();

        PackagingCostModel GetPackagingCostInfoById(int packagingCostId);

        bool isPackagingCostExist(int productSizeId, int packagingTypeId, int quantityRangeId);
    }
}
