﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
    public interface IClaimRepository : IGenericRepository<AspNetUserClaims>
    {
        bool isRoleInClaim(string roleName);
    }
}
