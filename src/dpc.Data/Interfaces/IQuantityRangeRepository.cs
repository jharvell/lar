﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
    public interface IQuantityRangeRepository : IGenericRepository<QuantityRange>
    {
	    IEnumerable<QuantityRangeModel> GetQuantityRangeInfo();

		bool isQuantityRangeExist(int productCategoryId, int containerTypeId, int minimum, int maximum);
	}
}
