﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
    public interface IBrandRepository : IGenericRepository<Brand>
    {
	    IEnumerable<BrandModel> GetBrandInfo();

		bool isBrandInProgramLine(int brandId);

		bool isBrandExist(int brandOwnerId, string description, string abbreviation);
    }
}
