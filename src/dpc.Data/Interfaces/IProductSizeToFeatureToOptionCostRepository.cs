﻿using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
	public interface IProductSizeToFeatureToOptionCostRepository : IGenericRepository<ProductSizeToFeatureToOptionCost>
	{
        IEnumerable<ProductSizeToFeatureToOptionCostModel> GetProductSizeToFeatureToOptionCostInfo();

        ProductSizeToFeatureToOptionCostModel GetProductSizeToFeatureToOptionCostInfoById(int productSizeToFeatureToOptionCostId);

        bool isProductSizeToFeatureToOptionCostExist(int featureToProductSizeId, int featureToOptionId);
    }
}
