﻿using System;
using System.Collections.Generic;

namespace dpc.Data.Interfaces
{
	public interface IPasswordHistoryRepository : IGenericRepository<PasswordHistory>
	{
		IEnumerable<string> GetPasswordHistory(string userId);

		DateTime GetLastSavedPasswordDate(string userId);
	}
}