﻿namespace dpc.Data.Interfaces
{
    public interface IProductCategoryRepository : IGenericRepository<ProductCategory>
    {
        bool isProductCategoryExist(string description, string abbreviation);
    }
}
