﻿namespace dpc.Common
{
	#region CheckAccess
	public abstract class ActionKey
	{
		public const string Access = "Access";
		public const string Show = "Show";
	}
	public abstract class ResourceKey
	{
		public const string ViewAdmin = "ViewAdmin";
		public const string ViewMaintenance = "ViewMaintenance";
		public const string ViewReporting = "ViewReporting";
		public const string ReportLRRdetails = "ReportLRRdetails";
		public const string ReportLRRfinancials = "ReportLRRfinancials";
		public const string ReportCalculations = "ReportCalculations";
	}
	#endregion CheckAccess


	#region Claims Authorization
	public abstract class ClaimType
	{
		public const string Role = "Role";
	}
	public abstract class ClaimValue
	{
		public const string Admin = "Admin";
		public const string Maintenance = "Maintenance";
		public const string Sales = "Sales";
		public const string Reporting_LRRdetails = "Reporting-LRR Details";
		public const string Reporting_LRRfinancials = "Reporting-LRR Financials";
		public const string Reporting_Calculations = "Reporting-Calculations";
	}
	#endregion Claims Authorization
}
