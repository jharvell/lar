﻿namespace dpc.Common
{
	public enum EnumProgramStatus
	{
		Open = 1,
		Rejected = 2,
		ReEvaluating = 3,
		Evaluating = 4,
		Approved = 5
	}
}