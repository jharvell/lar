namespace dpc.Report
{
    partial class Calculations
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Calculations));
			Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox56 = new Telerik.Reporting.TextBox();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox68 = new Telerik.Reporting.TextBox();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.reportNameTextBox = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.tableDetail = new Telerik.Reporting.Table();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox63 = new Telerik.Reporting.TextBox();
			this.textBox67 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.HeaderDataSource = new Telerik.Reporting.SqlDataSource();
			this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
			this.tableHeader = new Telerik.Reporting.Table();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox70 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// textBox46
			// 
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox56
			// 
			this.textBox56.Name = "textBox56";
			this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox65
			// 
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox43
			// 
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612571239471436D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox43.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox43.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox43.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox43.StyleName = "";
			this.textBox43.Value = "Status";
			// 
			// textBox14
			// 
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox14.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox14.Value = "Business Unit";
			// 
			// textBox17
			// 
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox17.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox17.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox17.Value = "Title";
			// 
			// textBox19
			// 
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox19.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox19.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox19.Value = "Brand Owner";
			// 
			// textBox20
			// 
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox20.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox20.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox20.StyleName = "";
			this.textBox20.Value = "Requested Ship Date";
			// 
			// textBox23
			// 
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox23.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox23.StyleName = "";
			this.textBox23.Value = "Contract End Date";
			// 
			// textBox24
			// 
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.24999998509883881D));
			this.textBox24.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox24.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox24.StyleName = "";
			this.textBox24.Value = "Comments";
			// 
			// textBox26
			// 
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612571239471436D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox26.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox26.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox26.StyleName = "";
			this.textBox26.Value = "Transition Type";
			// 
			// textBox68
			// 
			this.textBox68.Name = "textBox68";
			this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.561257004737854D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox68.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox68.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox68.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox68.StyleName = "";
			this.textBox68.Value = "Created By";
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.220833420753479D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// reportNameTextBox
			// 
			this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(-9.9341077586245774E-09D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
			this.reportNameTextBox.Name = "reportNameTextBox";
			this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.34488582611084D), Telerik.Reporting.Drawing.Unit.Inch(0.20000007748603821D));
			this.reportNameTextBox.Style.Font.Bold = true;
			this.reportNameTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.reportNameTextBox.Style.Font.Underline = true;
			this.reportNameTextBox.StyleName = "PageInfo";
			this.reportNameTextBox.Value = "Calculations for Program: {Fields.ProgramHeaderId}";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.70000016689300537D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tableDetail});
			this.detail.Name = "detail";
			// 
			// tableDetail
			// 
			this.tableDetail.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D)));
			this.tableDetail.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D)));
			this.tableDetail.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D)));
			this.tableDetail.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D)));
			this.tableDetail.Body.SetCellContent(0, 0, this.textBox49);
			this.tableDetail.Body.SetCellContent(0, 1, this.textBox63);
			this.tableDetail.Body.SetCellContent(0, 2, this.textBox67);
			tableGroup1.Name = "tableGroup9";
			tableGroup1.ReportItem = this.textBox46;
			tableGroup2.Name = "tableGroup10";
			tableGroup2.ReportItem = this.textBox56;
			tableGroup3.Name = "tableGroup11";
			tableGroup3.ReportItem = this.textBox65;
			this.tableDetail.ColumnGroups.Add(tableGroup1);
			this.tableDetail.ColumnGroups.Add(tableGroup2);
			this.tableDetail.ColumnGroups.Add(tableGroup3);
			this.tableDetail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox49,
            this.textBox63,
            this.textBox67,
            this.textBox46,
            this.textBox56,
            this.textBox65});
			this.tableDetail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.4505805969238281E-09D), Telerik.Reporting.Drawing.Unit.Inch(0.10000047832727432D));
			this.tableDetail.Name = "tableDetail";
			tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup4.Name = "detailTableGroup3";
			this.tableDetail.RowGroups.Add(tableGroup4);
			this.tableDetail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.53125D), Telerik.Reporting.Drawing.Unit.Inch(0.4791666567325592D));
			// 
			// textBox49
			// 
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox63
			// 
			this.textBox63.Name = "textBox63";
			this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox67
			// 
			this.textBox67.Name = "textBox67";
			this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D);
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// HeaderDataSource
			// 
			this.HeaderDataSource.ConnectionString = "DefaultConnection";
			this.HeaderDataSource.Name = "HeaderDataSource";
			this.HeaderDataSource.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@ProgramHeaderId", System.Data.DbType.Int32, "= Parameters.ProgramHeaderId.Value")});
			this.HeaderDataSource.SelectCommand = resources.GetString("HeaderDataSource.SelectCommand");
			// 
			// reportHeaderSection1
			// 
			this.reportHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(2.2500002384185791D);
			this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tableHeader});
			this.reportHeaderSection1.Name = "reportHeaderSection1";
			// 
			// tableHeader
			// 
			this.tableHeader.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24999997019767761D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24999997019767761D)));
			this.tableHeader.Body.SetCellContent(1, 0, this.textBox21);
			this.tableHeader.Body.SetCellContent(2, 0, this.textBox16);
			this.tableHeader.Body.SetCellContent(3, 0, this.textBox18);
			this.tableHeader.Body.SetCellContent(4, 0, this.textBox25);
			this.tableHeader.Body.SetCellContent(5, 0, this.textBox22);
			this.tableHeader.Body.SetCellContent(6, 0, this.textBox27);
			this.tableHeader.Body.SetCellContent(7, 0, this.textBox29);
			this.tableHeader.Body.SetCellContent(0, 0, this.textBox45);
			this.tableHeader.Body.SetCellContent(8, 0, this.textBox70);
			tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup5.Name = "detailTableGroup1";
			this.tableHeader.ColumnGroups.Add(tableGroup5);
			this.tableHeader.DataSource = this.HeaderDataSource;
			this.tableHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox45,
            this.textBox21,
            this.textBox16,
            this.textBox18,
            this.textBox25,
            this.textBox22,
            this.textBox27,
            this.textBox29,
            this.textBox70,
            this.textBox43,
            this.textBox14,
            this.textBox17,
            this.textBox19,
            this.textBox20,
            this.textBox23,
            this.textBox24,
            this.textBox26,
            this.textBox68});
			this.tableHeader.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(-1.9868215517249155E-08D));
			this.tableHeader.Name = "tableHeader";
			tableGroup6.Name = "group8";
			tableGroup6.ReportItem = this.textBox43;
			tableGroup7.Name = "tableGroup3";
			tableGroup7.ReportItem = this.textBox14;
			tableGroup8.Name = "tableGroup4";
			tableGroup8.ReportItem = this.textBox17;
			tableGroup9.Name = "tableGroup5";
			tableGroup9.ReportItem = this.textBox19;
			tableGroup10.Name = "group4";
			tableGroup10.ReportItem = this.textBox20;
			tableGroup11.Name = "group5";
			tableGroup11.ReportItem = this.textBox23;
			tableGroup12.Name = "group6";
			tableGroup12.ReportItem = this.textBox24;
			tableGroup13.Name = "group7";
			tableGroup13.ReportItem = this.textBox26;
			tableGroup14.Name = "group11";
			tableGroup14.ReportItem = this.textBox68;
			this.tableHeader.RowGroups.Add(tableGroup6);
			this.tableHeader.RowGroups.Add(tableGroup7);
			this.tableHeader.RowGroups.Add(tableGroup8);
			this.tableHeader.RowGroups.Add(tableGroup9);
			this.tableHeader.RowGroups.Add(tableGroup10);
			this.tableHeader.RowGroups.Add(tableGroup11);
			this.tableHeader.RowGroups.Add(tableGroup12);
			this.tableHeader.RowGroups.Add(tableGroup13);
			this.tableHeader.RowGroups.Add(tableGroup14);
			this.tableHeader.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6500613689422607D), Telerik.Reporting.Drawing.Unit.Inch(2.25D));
			this.tableHeader.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.tableHeader.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.tableHeader.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.tableHeader.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.tableHeader.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			// 
			// textBox21
			// 
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox21.Style.Font.Bold = true;
			this.textBox21.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox21.StyleName = "";
			this.textBox21.Value = "= Fields.BusinessUnit";
			// 
			// textBox16
			// 
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox16.Style.Font.Bold = true;
			this.textBox16.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox16.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox16.StyleName = "";
			this.textBox16.Value = "= Fields.Title";
			// 
			// textBox18
			// 
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox18.Style.Font.Bold = true;
			this.textBox18.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox18.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox18.StyleName = "";
			this.textBox18.Value = "= Fields.BrandOwnerDescription";
			// 
			// textBox25
			// 
			this.textBox25.Format = "{0:d}";
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox25.Style.Font.Bold = true;
			this.textBox25.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox25.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox25.StyleName = "";
			this.textBox25.Value = "= Fields.RequestedShipDate";
			// 
			// textBox22
			// 
			this.textBox22.Format = "{0:d}";
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox22.Style.Font.Bold = true;
			this.textBox22.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.StyleName = "";
			this.textBox22.Value = "= Fields.ContractEndDate";
			// 
			// textBox27
			// 
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.24999994039535523D));
			this.textBox27.Style.Font.Bold = true;
			this.textBox27.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox27.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox27.StyleName = "";
			this.textBox27.Value = "= Fields.Comments";
			// 
			// textBox29
			// 
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.24999994039535523D));
			this.textBox29.Style.Font.Bold = true;
			this.textBox29.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox29.StyleName = "";
			this.textBox29.Value = "= Fields.TransitionType";
			// 
			// textBox45
			// 
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.24999994039535523D));
			this.textBox45.Style.Font.Bold = true;
			this.textBox45.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox45.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox45.StyleName = "";
			this.textBox45.Value = "= Fields.Status";
			// 
			// textBox70
			// 
			this.textBox70.Name = "textBox70";
			this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox70.Style.Font.Bold = true;
			this.textBox70.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox70.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox70.StyleName = "";
			this.textBox70.Value = "= Fields.CreatedBy + \" on \" + Fields.CreatedOn.ToShortDateString()";
			// 
			// Calculations
			// 
			this.DataSource = this.HeaderDataSource;
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1,
            this.reportHeaderSection1});
			this.Name = "Calculations";
			this.PageSettings.Landscape = true;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
			this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(50D), Telerik.Reporting.Drawing.Unit.Inch(50D));
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(13D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource HeaderDataSource;
        private Telerik.Reporting.ReportHeaderSection reportHeaderSection1;
        private Telerik.Reporting.Table tableHeader;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox reportNameTextBox;
        private Telerik.Reporting.Table tableDetail;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox65;
		private Telerik.Reporting.TextBox textBox70;
		private Telerik.Reporting.TextBox textBox68;
	}
}