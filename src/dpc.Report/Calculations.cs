namespace dpc.Report
{
	using Business;
	using System.Data;
	using System.Drawing;
	using System.Text.RegularExpressions;
	using Telerik.Reporting;
	using Telerik.Reporting.Drawing;

	/// <summary>
	/// Summary description for Calculations.
	/// </summary>
	public partial class Calculations : Report
	{
		public Calculations(int programHeaderId)
		{
			InitializeComponent();

			HeaderDataSource.Parameters[0].Value = programHeaderId;

			var dt = GenerateReportDataTable(programHeaderId);

			tableDetail.DataSource = dt;
		}

		public Calculations()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();
		}

		private DataTable GenerateReportDataTable(int programHeaderId)
		{
			// Getting dynamic data
			ProgramManager pm = new ProgramManager();
			DataTable dt = pm.GetProgramLinesDataForCalculationsReport(programHeaderId);

			dt.Columns.Remove("ProgramLineId");
			dt.Columns.Remove("ProgramHeaderId");

			//Clear table before binding
			tableDetail.ColumnGroups.Clear();
			tableDetail.Body.Columns.Clear();
			tableDetail.Body.Rows.Clear();

			for (int i = 0; i < dt.Columns.Count; i++)
			{
				var tableGroupColumn = new TableGroup();
				tableDetail.ColumnGroups.Add(item: tableGroupColumn);

				var txtGroup = new HtmlTextBox()
				{
					Size = new SizeU(Unit.Inch(1.0), Unit.Inch(.4)),
					Value = Regex.Replace(dt.Columns[i].ColumnName, "(\\B[A-Z])", " $1"),  //Convert CamelCase to spaces
					Style =
					{
						BackgroundColor = Color.CornflowerBlue,
						Color = Color.White
					}
				};
				tableGroupColumn.ReportItem = txtGroup;

				var txtTable = new TextBox()
				{
					Size = new SizeU(Unit.Inch(1.0), Unit.Inch(.3)),
					Value = "=Fields." + dt.Columns[i].ColumnName,
					Style =
					{
						BorderStyle = {Default = BorderType.Solid},
						BorderColor = {Default = Color.Black},
						TextAlign = HorizontalAlign.Right
					}
				};

				switch (dt.Columns[i].ColumnName)
				{
					case "PiecePrice":
					case "BagCost":
					case "BoxCost":
					case "MaterialPerPad":
					case "VLPerPad":
						txtTable.Format = "{0:n3}";  //number 3 decimal places
						break;

					case "ContributionMarginPercent":
					case "VariableMarginPercent":
						txtTable.Format = "{0:p0}";  //percentage
						break;

					default:  //text fields will hit here but will not be number formatted
						txtTable.Format = "{0:n0}";  //number 0 decimal places
						break;
				}

				tableDetail.Body.SetCellContent(0, columnIndex: i, item: txtTable);

				tableDetail.Items.AddRange(items: new ReportItemBase[] { txtTable, txtGroup });
			}
			return dt;
		}
	}
}