namespace dpc.Report
{
    partial class CostModel
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CostModel));
			Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox56 = new Telerik.Reporting.TextBox();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox68 = new Telerik.Reporting.TextBox();
			this.textBox40 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.textBox42 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox57 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox58 = new Telerik.Reporting.TextBox();
			this.textBox69 = new Telerik.Reporting.TextBox();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.textBox59 = new Telerik.Reporting.TextBox();
			this.textBox38 = new Telerik.Reporting.TextBox();
			this.textBox60 = new Telerik.Reporting.TextBox();
			this.textBox64 = new Telerik.Reporting.TextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox52 = new Telerik.Reporting.TextBox();
			this.textBox61 = new Telerik.Reporting.TextBox();
			this.textBox51 = new Telerik.Reporting.TextBox();
			this.textBox55 = new Telerik.Reporting.TextBox();
			this.textBox62 = new Telerik.Reporting.TextBox();
			this.textBox54 = new Telerik.Reporting.TextBox();
			this.textBox28 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.reportNameTextBox = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.tableDetail = new Telerik.Reporting.Table();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox63 = new Telerik.Reporting.TextBox();
			this.textBox67 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.HeaderDataSource = new Telerik.Reporting.SqlDataSource();
			this.reportHeaderSection1 = new Telerik.Reporting.ReportHeaderSection();
			this.table1 = new Telerik.Reporting.Table();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.tableHeader = new Telerik.Reporting.Table();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox70 = new Telerik.Reporting.TextBox();
			this.table3 = new Telerik.Reporting.Table();
			this.textBox33 = new Telerik.Reporting.TextBox();
			this.textBox34 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.table5 = new Telerik.Reporting.Table();
			this.textBox66 = new Telerik.Reporting.TextBox();
			this.textBox32 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.textBox39 = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			this.textBox53 = new Telerik.Reporting.TextBox();
			this.table4 = new Telerik.Reporting.Table();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// textBox46
			// 
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox56
			// 
			this.textBox56.Name = "textBox56";
			this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox65
			// 
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox7
			// 
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.857147216796875D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox7.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox7.Style.Color = System.Drawing.Color.White;
			this.textBox7.Style.LineColor = System.Drawing.Color.Black;
			this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox7.StyleName = "";
			this.textBox7.Value = "Discounts";
			// 
			// textBox1
			// 
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1726173162460327D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox1.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox1.Style.Color = System.Drawing.Color.White;
			this.textBox1.Style.LineColor = System.Drawing.Color.Black;
			this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox1.Value = "Fees / Promotions";
			// 
			// textBox3
			// 
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.84970223903656006D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox3.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox3.Style.Color = System.Drawing.Color.White;
			this.textBox3.Style.LineColor = System.Drawing.Color.Black;
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "Broker Fees";
			// 
			// textBox5
			// 
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4712311029434204D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox5.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox5.Style.Color = System.Drawing.Color.White;
			this.textBox5.Style.LineColor = System.Drawing.Color.Black;
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox5.Value = "Returns, Damage, Etc";
			// 
			// textBox43
			// 
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612571239471436D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox43.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox43.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox43.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox43.StyleName = "";
			this.textBox43.Value = "Status";
			// 
			// textBox14
			// 
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox14.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox14.Value = "Business Unit";
			// 
			// textBox17
			// 
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox17.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox17.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox17.Value = "Title";
			// 
			// textBox19
			// 
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox19.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox19.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox19.Value = "Brand Owner";
			// 
			// textBox20
			// 
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox20.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox20.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox20.StyleName = "";
			this.textBox20.Value = "Requested Ship Date";
			// 
			// textBox23
			// 
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox23.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox23.StyleName = "";
			this.textBox23.Value = "Contract End Date";
			// 
			// textBox24
			// 
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612572431564331D), Telerik.Reporting.Drawing.Unit.Inch(0.24999998509883881D));
			this.textBox24.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox24.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox24.StyleName = "";
			this.textBox24.Value = "Comments";
			// 
			// textBox26
			// 
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5612571239471436D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox26.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox26.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox26.StyleName = "";
			this.textBox26.Value = "Transition Type";
			// 
			// textBox68
			// 
			this.textBox68.Name = "textBox68";
			this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.561257004737854D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox68.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox68.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox68.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox68.StyleName = "";
			this.textBox68.Value = "Created By";
			// 
			// textBox40
			// 
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.541666567325592D), Telerik.Reporting.Drawing.Unit.Inch(0.25000014901161194D));
			this.textBox40.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox40.Style.Color = System.Drawing.Color.White;
			this.textBox40.Style.LineColor = System.Drawing.Color.Black;
			this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox40.StyleName = "";
			this.textBox40.Value = "Pallets";
			// 
			// textBox6
			// 
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox6.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox6.Style.Color = System.Drawing.Color.White;
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox6.StyleName = "";
			this.textBox6.Value = "Freight Terms";
			// 
			// textBox41
			// 
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D));
			this.textBox41.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox41.Style.Color = System.Drawing.Color.White;
			this.textBox41.Style.LineColor = System.Drawing.Color.Black;
			this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox41.StyleName = "";
			this.textBox41.Value = "Freight Per Pad";
			// 
			// textBox42
			// 
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4097224473953247D), Telerik.Reporting.Drawing.Unit.Inch(0.25000017881393433D));
			this.textBox42.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox42.Style.Color = System.Drawing.Color.White;
			this.textBox42.Style.LineColor = System.Drawing.Color.Black;
			this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox42.StyleName = "";
			this.textBox42.Value = "Ocean Freight per Pad";
			// 
			// textBox44
			// 
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89662867784500122D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox44.Style.Font.Bold = true;
			this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox44.StyleName = "";
			this.textBox44.Value = "Bags";
			// 
			// textBox57
			// 
			this.textBox57.Name = "textBox57";
			this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89662867784500122D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox57.Style.Font.Bold = true;
			this.textBox57.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox57.StyleName = "";
			this.textBox57.Value = "Pieces";
			// 
			// textBox12
			// 
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89662867784500122D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox12.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox12.Style.Color = System.Drawing.Color.White;
			this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox12.Value = "(In thousands)";
			// 
			// textBox58
			// 
			this.textBox58.Format = "{0:N0}";
			this.textBox58.Name = "textBox58";
			this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.60069435834884644D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox58.StyleName = "";
			this.textBox58.Value = "= Fields.Bags / 1000";
			// 
			// textBox69
			// 
			this.textBox69.Format = "{0:N0}";
			this.textBox69.Name = "textBox69";
			this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.60069435834884644D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox69.StyleName = "";
			this.textBox69.Value = "= Fields.Pieces / 1000";
			// 
			// textBox36
			// 
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.60069435834884644D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox36.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox36.Style.Color = System.Drawing.Color.White;
			this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox36.Value = "Volume";
			// 
			// textBox47
			// 
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3113476037979126D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox47.Style.Font.Bold = true;
			this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox47.StyleName = "";
			this.textBox47.Value = "Contribution Margin";
			// 
			// textBox59
			// 
			this.textBox59.Name = "textBox59";
			this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3113477230072022D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox59.Style.Font.Bold = true;
			this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox59.StyleName = "";
			this.textBox59.Value = "Net Sales";
			// 
			// textBox38
			// 
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3113476037979126D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox38.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox38.Style.Color = System.Drawing.Color.White;
			this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox38.Value = "P&L";
			// 
			// textBox60
			// 
			this.textBox60.Format = "{0:C0}";
			this.textBox60.Name = "textBox60";
			this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65000003576278687D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox60.StyleName = "";
			this.textBox60.Value = "= Fields.ContributionMargin / 1000";
			// 
			// textBox64
			// 
			this.textBox64.Format = "{0:C0}";
			this.textBox64.Name = "textBox64";
			this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65000009536743164D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox64.StyleName = "";
			this.textBox64.Value = "= Fields.NetSales / 1000";
			// 
			// textBox48
			// 
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65000003576278687D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox48.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox48.Style.Color = System.Drawing.Color.White;
			this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox48.StyleName = "";
			this.textBox48.Value = "$";
			// 
			// textBox52
			// 
			this.textBox52.Format = "{0:P0}";
			this.textBox52.Name = "textBox52";
			this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64999997615814209D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox52.StyleName = "";
			this.textBox52.Value = "= Fields.ContributionMarginPercent";
			// 
			// textBox61
			// 
			this.textBox61.Name = "textBox61";
			this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64999991655349731D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox61.StyleName = "";
			// 
			// textBox51
			// 
			this.textBox51.Name = "textBox51";
			this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64999997615814209D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox51.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox51.Style.Color = System.Drawing.Color.White;
			this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox51.StyleName = "";
			this.textBox51.Value = "%";
			// 
			// textBox55
			// 
			this.textBox55.Format = "{0:C2}";
			this.textBox55.Name = "textBox55";
			this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583367109298706D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox55.StyleName = "";
			this.textBox55.Value = "= Fields.ContributionMarginPerPiece";
			// 
			// textBox62
			// 
			this.textBox62.Name = "textBox62";
			this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583367109298706D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox62.StyleName = "";
			// 
			// textBox54
			// 
			this.textBox54.Name = "textBox54";
			this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583367109298706D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox54.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox54.Style.Color = System.Drawing.Color.White;
			this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox54.StyleName = "";
			this.textBox54.Value = "Per Piece";
			// 
			// textBox28
			// 
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.86099898815155029D), Telerik.Reporting.Drawing.Unit.Inch(0.25000017881393433D));
			this.textBox28.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox28.Style.Color = System.Drawing.Color.White;
			this.textBox28.Style.LineColor = System.Drawing.Color.Black;
			this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox28.StyleName = "";
			this.textBox28.Value = "New Program";
			// 
			// textBox30
			// 
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6456261873245239D), Telerik.Reporting.Drawing.Unit.Inch(0.25000017881393433D));
			this.textBox30.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox30.Style.Color = System.Drawing.Color.White;
			this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox30.StyleName = "";
			this.textBox30.Value = "Unique Program Fee/Cost";
			// 
			// textBox31
			// 
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9690724611282349D), Telerik.Reporting.Drawing.Unit.Inch(0.25000017881393433D));
			this.textBox31.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
			this.textBox31.Style.Color = System.Drawing.Color.White;
			this.textBox31.Style.LineColor = System.Drawing.Color.Black;
			this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox31.StyleName = "";
			this.textBox31.Value = "Program Fee Description";
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.220833420753479D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.reportNameTextBox});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// reportNameTextBox
			// 
			this.reportNameTextBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(-9.9341077586245774E-09D), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D));
			this.reportNameTextBox.Name = "reportNameTextBox";
			this.reportNameTextBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.34488582611084D), Telerik.Reporting.Drawing.Unit.Inch(0.20000007748603821D));
			this.reportNameTextBox.Style.Font.Bold = true;
			this.reportNameTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.reportNameTextBox.Style.Font.Underline = true;
			this.reportNameTextBox.StyleName = "PageInfo";
			this.reportNameTextBox.Value = "Infant Feature-Creation Cost Model for Program: {Fields.ProgramHeaderId}";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.70000016689300537D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tableDetail});
			this.detail.Name = "detail";
			// 
			// tableDetail
			// 
			this.tableDetail.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D)));
			this.tableDetail.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D)));
			this.tableDetail.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D)));
			this.tableDetail.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D)));
			this.tableDetail.Body.SetCellContent(0, 0, this.textBox49);
			this.tableDetail.Body.SetCellContent(0, 1, this.textBox63);
			this.tableDetail.Body.SetCellContent(0, 2, this.textBox67);
			tableGroup1.Name = "tableGroup9";
			tableGroup1.ReportItem = this.textBox46;
			tableGroup2.Name = "tableGroup10";
			tableGroup2.ReportItem = this.textBox56;
			tableGroup3.Name = "tableGroup11";
			tableGroup3.ReportItem = this.textBox65;
			this.tableDetail.ColumnGroups.Add(tableGroup1);
			this.tableDetail.ColumnGroups.Add(tableGroup2);
			this.tableDetail.ColumnGroups.Add(tableGroup3);
			this.tableDetail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox49,
            this.textBox63,
            this.textBox67,
            this.textBox46,
            this.textBox56,
            this.textBox65});
			this.tableDetail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.4505805969238281E-09D), Telerik.Reporting.Drawing.Unit.Inch(0.10000047832727432D));
			this.tableDetail.Name = "tableDetail";
			tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup4.Name = "detailTableGroup3";
			this.tableDetail.RowGroups.Add(tableGroup4);
			this.tableDetail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.53125D), Telerik.Reporting.Drawing.Unit.Inch(0.4791666567325592D));
			// 
			// textBox49
			// 
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox63
			// 
			this.textBox63.Name = "textBox63";
			this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// textBox67
			// 
			this.textBox67.Name = "textBox67";
			this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1770833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D);
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// HeaderDataSource
			// 
			this.HeaderDataSource.ConnectionString = "DefaultConnection";
			this.HeaderDataSource.Name = "HeaderDataSource";
			this.HeaderDataSource.Parameters.AddRange(new Telerik.Reporting.SqlDataSourceParameter[] {
            new Telerik.Reporting.SqlDataSourceParameter("@ProgramHeaderId", System.Data.DbType.Int32, "= Parameters.ProgramHeaderId.Value")});
			this.HeaderDataSource.SelectCommand = resources.GetString("HeaderDataSource.SelectCommand");
			// 
			// reportHeaderSection1
			// 
			this.reportHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(2.2500002384185791D);
			this.reportHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.tableHeader,
            this.table3,
            this.table5,
            this.table4});
			this.reportHeaderSection1.Name = "reportHeaderSection1";
			// 
			// table1
			// 
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.85714757442474365D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1726174354553223D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.84970271587371826D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.47123122215271D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25D)));
			this.table1.Body.SetCellContent(0, 0, this.textBox8);
			this.table1.Body.SetCellContent(0, 1, this.textBox9);
			this.table1.Body.SetCellContent(0, 2, this.textBox2);
			this.table1.Body.SetCellContent(0, 3, this.textBox4);
			tableGroup5.Name = "group";
			tableGroup5.ReportItem = this.textBox7;
			tableGroup6.Name = "tableGroup";
			tableGroup6.ReportItem = this.textBox1;
			tableGroup7.Name = "tableGroup1";
			tableGroup7.ReportItem = this.textBox3;
			tableGroup8.Name = "tableGroup2";
			tableGroup8.ReportItem = this.textBox5;
			this.table1.ColumnGroups.Add(tableGroup5);
			this.table1.ColumnGroups.Add(tableGroup6);
			this.table1.ColumnGroups.Add(tableGroup7);
			this.table1.ColumnGroups.Add(tableGroup8);
			this.table1.DataSource = this.HeaderDataSource;
			this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox8,
            this.textBox9,
            this.textBox2,
            this.textBox4,
            this.textBox7,
            this.textBox1,
            this.textBox3,
            this.textBox5});
			this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.703829288482666D), Telerik.Reporting.Drawing.Unit.Inch(-1.9868215517249155E-08D));
			this.table1.Name = "table1";
			tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup9.Name = "detailTableGroup";
			this.table1.RowGroups.Add(tableGroup9);
			this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.3506989479064941D), Telerik.Reporting.Drawing.Unit.Inch(0.50000005960464478D));
			this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			// 
			// textBox8
			// 
			this.textBox8.Format = "{0:P2}";
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85714763402938843D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox8.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox8.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox8.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox8.StyleName = "";
			this.textBox8.Value = "= Fields.Discounts";
			// 
			// textBox9
			// 
			this.textBox9.Format = "{0:P2}";
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1726174354553223D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox9.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox9.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox9.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox9.StyleName = "";
			this.textBox9.Value = "= Fields.FeesPromotions";
			// 
			// textBox2
			// 
			this.textBox2.Format = "{0:P2}";
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.84970265626907349D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.StyleName = "";
			this.textBox2.Value = "= Fields.BrokerFees";
			// 
			// textBox4
			// 
			this.textBox4.Format = "{0:P1}";
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4712313413619995D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox4.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox4.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox4.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox4.StyleName = "";
			this.textBox4.Value = "= Fields.ReturnsDamage";
			// 
			// tableHeader
			// 
			this.tableHeader.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24999998509883881D)));
			this.tableHeader.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24999998509883881D)));
			this.tableHeader.Body.SetCellContent(1, 0, this.textBox21);
			this.tableHeader.Body.SetCellContent(2, 0, this.textBox16);
			this.tableHeader.Body.SetCellContent(3, 0, this.textBox18);
			this.tableHeader.Body.SetCellContent(4, 0, this.textBox25);
			this.tableHeader.Body.SetCellContent(5, 0, this.textBox22);
			this.tableHeader.Body.SetCellContent(6, 0, this.textBox27);
			this.tableHeader.Body.SetCellContent(7, 0, this.textBox29);
			this.tableHeader.Body.SetCellContent(0, 0, this.textBox45);
			this.tableHeader.Body.SetCellContent(8, 0, this.textBox70);
			tableGroup10.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup10.Name = "detailTableGroup1";
			this.tableHeader.ColumnGroups.Add(tableGroup10);
			this.tableHeader.DataSource = this.HeaderDataSource;
			this.tableHeader.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox45,
            this.textBox21,
            this.textBox16,
            this.textBox18,
            this.textBox25,
            this.textBox22,
            this.textBox27,
            this.textBox29,
            this.textBox70,
            this.textBox43,
            this.textBox14,
            this.textBox17,
            this.textBox19,
            this.textBox20,
            this.textBox23,
            this.textBox24,
            this.textBox26,
            this.textBox68});
			this.tableHeader.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(-1.9868215517249155E-08D));
			this.tableHeader.Name = "tableHeader";
			tableGroup11.Name = "group8";
			tableGroup11.ReportItem = this.textBox43;
			tableGroup12.Name = "tableGroup3";
			tableGroup12.ReportItem = this.textBox14;
			tableGroup13.Name = "tableGroup4";
			tableGroup13.ReportItem = this.textBox17;
			tableGroup14.Name = "tableGroup5";
			tableGroup14.ReportItem = this.textBox19;
			tableGroup15.Name = "group4";
			tableGroup15.ReportItem = this.textBox20;
			tableGroup16.Name = "group5";
			tableGroup16.ReportItem = this.textBox23;
			tableGroup17.Name = "group6";
			tableGroup17.ReportItem = this.textBox24;
			tableGroup18.Name = "group7";
			tableGroup18.ReportItem = this.textBox26;
			tableGroup19.Name = "group11";
			tableGroup19.ReportItem = this.textBox68;
			this.tableHeader.RowGroups.Add(tableGroup11);
			this.tableHeader.RowGroups.Add(tableGroup12);
			this.tableHeader.RowGroups.Add(tableGroup13);
			this.tableHeader.RowGroups.Add(tableGroup14);
			this.tableHeader.RowGroups.Add(tableGroup15);
			this.tableHeader.RowGroups.Add(tableGroup16);
			this.tableHeader.RowGroups.Add(tableGroup17);
			this.tableHeader.RowGroups.Add(tableGroup18);
			this.tableHeader.RowGroups.Add(tableGroup19);
			this.tableHeader.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6500613689422607D), Telerik.Reporting.Drawing.Unit.Inch(2.25D));
			this.tableHeader.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.tableHeader.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.tableHeader.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.tableHeader.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.tableHeader.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			// 
			// textBox21
			// 
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox21.Style.Font.Bold = true;
			this.textBox21.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox21.StyleName = "";
			this.textBox21.Value = "= Fields.BusinessUnit";
			// 
			// textBox16
			// 
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox16.Style.Font.Bold = true;
			this.textBox16.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox16.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox16.StyleName = "";
			this.textBox16.Value = "= Fields.Title";
			// 
			// textBox18
			// 
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox18.Style.Font.Bold = true;
			this.textBox18.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox18.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox18.StyleName = "";
			this.textBox18.Value = "= Fields.BrandOwnerDescription";
			// 
			// textBox25
			// 
			this.textBox25.Format = "{0:d}";
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox25.Style.Font.Bold = true;
			this.textBox25.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox25.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox25.StyleName = "";
			this.textBox25.Value = "= Fields.RequestedShipDate";
			// 
			// textBox22
			// 
			this.textBox22.Format = "{0:d}";
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25000005960464478D));
			this.textBox22.Style.Font.Bold = true;
			this.textBox22.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.StyleName = "";
			this.textBox22.Value = "= Fields.ContractEndDate";
			// 
			// textBox27
			// 
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.24999994039535523D));
			this.textBox27.Style.Font.Bold = true;
			this.textBox27.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox27.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox27.StyleName = "";
			this.textBox27.Value = "= Fields.Comments";
			// 
			// textBox29
			// 
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.24999994039535523D));
			this.textBox29.Style.Font.Bold = true;
			this.textBox29.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox29.StyleName = "";
			this.textBox29.Value = "= Fields.TransitionType";
			// 
			// textBox45
			// 
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.24999994039535523D));
			this.textBox45.Style.Font.Bold = true;
			this.textBox45.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox45.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox45.StyleName = "";
			this.textBox45.Value = "= Fields.Status";
			// 
			// textBox70
			// 
			this.textBox70.Name = "textBox70";
			this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0888042449951172D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox70.Style.Font.Bold = true;
			this.textBox70.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox70.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox70.StyleName = "";
			this.textBox70.Value = "= Fields.CreatedBy + \" on \" + Fields.CreatedOn.ToShortDateString()";
			// 
			// table3
			// 
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.54166674613952637D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.9999997615814209D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.9999997615814209D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4097225666046143D)));
			this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25000002980232239D)));
			this.table3.Body.SetCellContent(0, 0, this.textBox33);
			this.table3.Body.SetCellContent(0, 2, this.textBox34);
			this.table3.Body.SetCellContent(0, 3, this.textBox35);
			this.table3.Body.SetCellContent(0, 1, this.textBox11);
			tableGroup20.Name = "group1";
			tableGroup20.ReportItem = this.textBox40;
			tableGroup21.Name = "group9";
			tableGroup21.ReportItem = this.textBox6;
			tableGroup22.Name = "group2";
			tableGroup22.ReportItem = this.textBox41;
			tableGroup23.Name = "group3";
			tableGroup23.ReportItem = this.textBox42;
			this.table3.ColumnGroups.Add(tableGroup20);
			this.table3.ColumnGroups.Add(tableGroup21);
			this.table3.ColumnGroups.Add(tableGroup22);
			this.table3.ColumnGroups.Add(tableGroup23);
			this.table3.DataSource = this.HeaderDataSource;
			this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox33,
            this.textBox11,
            this.textBox34,
            this.textBox35,
            this.textBox40,
            this.textBox6,
            this.textBox41,
            this.textBox42});
			this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(8.30000114440918D), Telerik.Reporting.Drawing.Unit.Inch(1.9868215517249155E-08D));
			this.table3.Name = "table3";
			tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup24.Name = "detailTableGroup";
			this.table3.RowGroups.Add(tableGroup24);
			this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.9513888359069824D), Telerik.Reporting.Drawing.Unit.Inch(0.50000017881393433D));
			this.table3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			// 
			// textBox33
			// 
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.54166680574417114D), Telerik.Reporting.Drawing.Unit.Inch(0.24999998509883881D));
			this.textBox33.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox33.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox33.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox33.StyleName = "";
			this.textBox33.Value = "= IIf(Fields.Pallets = \"True\", \"Yes\", \"No\")";
			// 
			// textBox34
			// 
			this.textBox34.Format = "{0:C2}";
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99999982118606567D), Telerik.Reporting.Drawing.Unit.Inch(0.24999998509883881D));
			this.textBox34.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox34.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox34.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox34.StyleName = "";
			this.textBox34.Value = "= Fields.FreightPerPad";
			// 
			// textBox35
			// 
			this.textBox35.Format = "{0:C2}";
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4097224473953247D), Telerik.Reporting.Drawing.Unit.Inch(0.24999998509883881D));
			this.textBox35.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox35.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox35.StyleName = "";
			this.textBox35.Value = "= Fields.OceanFreight";
			// 
			// textBox11
			// 
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99999982118606567D), Telerik.Reporting.Drawing.Unit.Inch(0.24999998509883881D));
			this.textBox11.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox11.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox11.StyleName = "";
			this.textBox11.Value = "= Fields.FreightTerms";
			// 
			// table5
			// 
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.89662867784500122D)));
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.60069429874420166D)));
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3113476037979126D)));
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.65000003576278687D)));
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.64999991655349731D)));
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.64583361148834229D)));
			this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25D)));
			this.table5.Body.SetCellContent(0, 0, this.textBox66);
			this.table5.Body.SetCellContent(0, 1, this.textBox32);
			this.table5.Body.SetCellContent(0, 2, this.textBox37);
			this.table5.Body.SetCellContent(0, 3, this.textBox39);
			this.table5.Body.SetCellContent(0, 4, this.textBox50);
			this.table5.Body.SetCellContent(0, 5, this.textBox53);
			tableGroup28.Name = "group25";
			tableGroup27.ChildGroups.Add(tableGroup28);
			tableGroup27.Name = "group19";
			tableGroup27.ReportItem = this.textBox44;
			tableGroup26.ChildGroups.Add(tableGroup27);
			tableGroup26.Name = "group10";
			tableGroup26.ReportItem = this.textBox57;
			tableGroup25.ChildGroups.Add(tableGroup26);
			tableGroup25.Name = "tableGroup6";
			tableGroup25.ReportItem = this.textBox12;
			tableGroup32.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.Cases"));
			tableGroup32.Name = "cases";
			tableGroup32.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.Cases", Telerik.Reporting.SortDirection.Asc));
			tableGroup31.ChildGroups.Add(tableGroup32);
			tableGroup31.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.Bags"));
			tableGroup31.Name = "bags";
			tableGroup31.ReportItem = this.textBox58;
			tableGroup31.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.Bags", Telerik.Reporting.SortDirection.Asc));
			tableGroup30.ChildGroups.Add(tableGroup31);
			tableGroup30.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.Pieces"));
			tableGroup30.Name = "pieces";
			tableGroup30.ReportItem = this.textBox69;
			tableGroup30.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.Pieces", Telerik.Reporting.SortDirection.Asc));
			tableGroup29.ChildGroups.Add(tableGroup30);
			tableGroup29.Name = "tableGroup7";
			tableGroup29.ReportItem = this.textBox36;
			tableGroup36.Name = "group27";
			tableGroup35.ChildGroups.Add(tableGroup36);
			tableGroup35.Name = "group21";
			tableGroup35.ReportItem = this.textBox47;
			tableGroup34.ChildGroups.Add(tableGroup35);
			tableGroup34.Name = "group12";
			tableGroup34.ReportItem = this.textBox59;
			tableGroup33.ChildGroups.Add(tableGroup34);
			tableGroup33.Name = "tableGroup8";
			tableGroup33.ReportItem = this.textBox38;
			tableGroup40.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.VariableMargin"));
			tableGroup40.Name = "variableMargin";
			tableGroup40.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.VariableMargin", Telerik.Reporting.SortDirection.Asc));
			tableGroup39.ChildGroups.Add(tableGroup40);
			tableGroup39.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.ContributionMargin"));
			tableGroup39.Name = "contributionMargin";
			tableGroup39.ReportItem = this.textBox60;
			tableGroup39.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.ContributionMargin", Telerik.Reporting.SortDirection.Asc));
			tableGroup38.ChildGroups.Add(tableGroup39);
			tableGroup38.Groupings.Add(new Telerik.Reporting.Grouping("= Fields.NetSales"));
			tableGroup38.Name = "netSales";
			tableGroup38.ReportItem = this.textBox64;
			tableGroup38.Sortings.Add(new Telerik.Reporting.Sorting("= Fields.NetSales", Telerik.Reporting.SortDirection.Asc));
			tableGroup37.ChildGroups.Add(tableGroup38);
			tableGroup37.Name = "group13";
			tableGroup37.ReportItem = this.textBox48;
			tableGroup44.Name = "group29";
			tableGroup43.ChildGroups.Add(tableGroup44);
			tableGroup43.Name = "group23";
			tableGroup43.ReportItem = this.textBox52;
			tableGroup42.ChildGroups.Add(tableGroup43);
			tableGroup42.Name = "group16";
			tableGroup42.ReportItem = this.textBox61;
			tableGroup41.ChildGroups.Add(tableGroup42);
			tableGroup41.Name = "group15";
			tableGroup41.ReportItem = this.textBox51;
			tableGroup48.Name = "group30";
			tableGroup47.ChildGroups.Add(tableGroup48);
			tableGroup47.Name = "group24";
			tableGroup47.ReportItem = this.textBox55;
			tableGroup46.ChildGroups.Add(tableGroup47);
			tableGroup46.Name = "group18";
			tableGroup46.ReportItem = this.textBox62;
			tableGroup45.ChildGroups.Add(tableGroup46);
			tableGroup45.Name = "group17";
			tableGroup45.ReportItem = this.textBox54;
			this.table5.ColumnGroups.Add(tableGroup25);
			this.table5.ColumnGroups.Add(tableGroup29);
			this.table5.ColumnGroups.Add(tableGroup33);
			this.table5.ColumnGroups.Add(tableGroup37);
			this.table5.ColumnGroups.Add(tableGroup41);
			this.table5.ColumnGroups.Add(tableGroup45);
			this.table5.DataSource = this.HeaderDataSource;
			this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox66,
            this.textBox32,
            this.textBox37,
            this.textBox39,
            this.textBox50,
            this.textBox53,
            this.textBox12,
            this.textBox57,
            this.textBox44,
            this.textBox36,
            this.textBox69,
            this.textBox58,
            this.textBox38,
            this.textBox59,
            this.textBox47,
            this.textBox48,
            this.textBox64,
            this.textBox60,
            this.textBox51,
            this.textBox61,
            this.textBox52,
            this.textBox54,
            this.textBox62,
            this.textBox55});
			this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(1.1791666746139526D));
			this.table5.Name = "table5";
			tableGroup49.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup49.Name = "detailTableGroup2";
			this.table5.RowGroups.Add(tableGroup49);
			this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.7545042037963867D), Telerik.Reporting.Drawing.Unit.Inch(1D));
			this.table5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			// 
			// textBox66
			// 
			this.textBox66.Name = "textBox66";
			this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89662867784500122D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox66.Style.Font.Bold = true;
			this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox66.StyleName = "";
			this.textBox66.Value = "Cases";
			// 
			// textBox32
			// 
			this.textBox32.Format = "{0:N0}";
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.60069435834884644D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox32.StyleName = "";
			this.textBox32.Value = "= Fields.Cases / 1000";
			// 
			// textBox37
			// 
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3113477230072022D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox37.Style.Font.Bold = true;
			this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox37.StyleName = "";
			this.textBox37.Value = "Variable Margin";
			// 
			// textBox39
			// 
			this.textBox39.Format = "{0:C0}";
			this.textBox39.Name = "textBox39";
			this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65000009536743164D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox39.StyleName = "";
			this.textBox39.Value = "= Fields.VariableMargin / 1000";
			// 
			// textBox50
			// 
			this.textBox50.Format = "{0:P0}";
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64999991655349731D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox50.StyleName = "";
			this.textBox50.Value = "= Fields.VariableMarginPercent";
			// 
			// textBox53
			// 
			this.textBox53.Format = "{0:C2}";
			this.textBox53.Name = "textBox53";
			this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583367109298706D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox53.StyleName = "";
			this.textBox53.Value = "= Fields.VariableMarginPerPiece";
			// 
			// table4
			// 
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.86099874973297119D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6456261873245239D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9690730571746826D)));
			this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.25D)));
			this.table4.Body.SetCellContent(0, 0, this.textBox13);
			this.table4.Body.SetCellContent(0, 1, this.textBox10);
			this.table4.Body.SetCellContent(0, 2, this.textBox15);
			tableGroup50.Name = "group1";
			tableGroup50.ReportItem = this.textBox28;
			tableGroup51.Name = "group9";
			tableGroup51.ReportItem = this.textBox30;
			tableGroup52.Name = "group2";
			tableGroup52.ReportItem = this.textBox31;
			this.table4.ColumnGroups.Add(tableGroup50);
			this.table4.ColumnGroups.Add(tableGroup51);
			this.table4.ColumnGroups.Add(tableGroup52);
			this.table4.DataSource = this.HeaderDataSource;
			this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13,
            this.textBox10,
            this.textBox15,
            this.textBox28,
            this.textBox30,
            this.textBox31});
			this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.703829288482666D), Telerik.Reporting.Drawing.Unit.Inch(0.57916659116745D));
			this.table4.Name = "table4";
			tableGroup53.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup53.Name = "detailTableGroup";
			this.table4.RowGroups.Add(tableGroup53);
			this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.4756984710693359D), Telerik.Reporting.Drawing.Unit.Inch(0.50000017881393433D));
			this.table4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			// 
			// textBox13
			// 
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.86099904775619507D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox13.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox13.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox13.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox13.StyleName = "";
			this.textBox13.Value = "= IIf(Fields.NewProgram= \"True\", \"Yes\", \"No\")";
			// 
			// textBox10
			// 
			this.textBox10.Format = "{0:C2}";
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6456261873245239D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox10.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox10.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox10.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox10.StyleName = "";
			this.textBox10.Value = "= Fields.ProgramFee";
			// 
			// textBox15
			// 
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9690721035003662D), Telerik.Reporting.Drawing.Unit.Inch(0.25D));
			this.textBox15.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox15.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox15.StyleName = "";
			this.textBox15.Value = "= Fields.ProgramFeeDescription";
			// 
			// CostModel
			// 
			this.DataSource = this.HeaderDataSource;
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1,
            this.reportHeaderSection1});
			this.Name = "CostModel";
			this.PageSettings.Landscape = true;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
			this.PageSettings.PaperSize = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(50D), Telerik.Reporting.Drawing.Unit.Inch(50D));
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(13D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.SqlDataSource HeaderDataSource;
        private Telerik.Reporting.ReportHeaderSection reportHeaderSection1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.Table tableHeader;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox reportNameTextBox;
        private Telerik.Reporting.Table tableDetail;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox65;
		private Telerik.Reporting.TextBox textBox70;
		private Telerik.Reporting.TextBox textBox68;
	}
}