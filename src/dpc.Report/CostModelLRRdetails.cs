namespace dpc.Report
{
	using Business;
	using System.Data;
	using System.Drawing;
	using System.Text.RegularExpressions;
	using Telerik.Reporting;
	using Telerik.Reporting.Drawing;

	/// <summary>
	/// Summary description for CostModelLRRdetails.
	/// </summary>
	public partial class CostModelLRRdetails : Telerik.Reporting.Report
	{
		public CostModelLRRdetails(int programHeaderId)
		{
			InitializeComponent();

			this.HeaderDataSource.Parameters[0].Value = programHeaderId;

			var dt = GenerateReportDataTable(programHeaderId);

			tableDetail.DataSource = dt;
		}

		public CostModelLRRdetails()
		{
			//
			// Required for telerik Reporting designer support
			//
			InitializeComponent();
		}

		private DataTable GenerateReportDataTable(int programHeaderId)
		{
			// Getting dynamic data
			ProgramManager pm = new ProgramManager();
			DataTable dt = pm.GetProgramLinesDataForLRRdetailsReport(programHeaderId);

			dt.Columns.Remove("ProgramLineId");
			dt.Columns.Remove("ProgramHeaderId");

			//Clear table before binding
			tableDetail.ColumnGroups.Clear();
			tableDetail.Body.Columns.Clear();
			tableDetail.Body.Rows.Clear();

			HtmlTextBox txtGroup;
			TextBox txtTable;

			for (int i = 0; i < dt.Columns.Count; i++)
			{
				var tableGroupColumn = new TableGroup();
				tableDetail.ColumnGroups.Add(item: tableGroupColumn);

				txtGroup = new HtmlTextBox()
				{
					Size = new SizeU(Unit.Inch(1.0), Unit.Inch(.3)),
					Value = Regex.Replace(dt.Columns[i].ColumnName, "(\\B[A-Z])", " $1"),  //Convert CamelCase to spaces
					Style =
					{
						BackgroundColor = System.Drawing.Color.CornflowerBlue,
						Color = System.Drawing.Color.White
					}
				};
				tableGroupColumn.ReportItem = txtGroup;

				txtTable = new TextBox()
				{
					Size = new SizeU(Unit.Inch(1.0), Unit.Inch(.3)),
					Value = "=Fields." + dt.Columns[i].ColumnName,
					Style =
					{
						BorderStyle = {Default = BorderType.Solid},
						BorderColor = {Default = Color.Black}
					}
				};

				switch (dt.Columns[i].ColumnName)
				{
					case "BagCount":
					case "BagsPerCase":
					case "AnnualCases":
					case "Pieces":
						txtTable.Format = ("{0:n0}");
						break;

					case "CaseSellingPrice":
						txtTable.Format = ("{0:c2}");
						break;

					case "PiecePrice":
						txtTable.Format = ("{0:c3}");
						break;

					case "ContributionMargin":
					case "VariableMargin":
					case "NetSales":
						txtTable.Format = ("{0:c0}");
						break;

					case "ContributionMarginPercent":
					case "VariableMarginPercent":
						txtTable.Format = ("{0:p0}");
						break;
				}

				tableDetail.Body.SetCellContent(0, columnIndex: i, item: txtTable);

				tableDetail.Items.AddRange(items: new ReportItemBase[] { txtTable, txtGroup });
			}
			return dt;
		}
	}
}