﻿using dpc.Business.Interfaces;
using dpc.Data;
using dpc.Data.CustomModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dpc.Business
{
	public class MaintenanceManager : BaseManager, IMaintenanceManager
	{
        private readonly IUnitOfWork _uow;

        #region constructors

        public MaintenanceManager()
        {
            _uow = new UnitOfWork();
        }

        public MaintenanceManager(IUnitOfWork unitOfWork)
        {
            _uow = unitOfWork;
        }

        #endregion constructors

		#region Brand

		public Brand GetBrandById(int id)
		{
			if (!(id > 0))
			{
				throw new ArgumentException($"Contact IT: Invalid argument for GetBrandById where id = {id}");
			}

			return _uow.BrandRepository.GetByID(id);
		}

		public int InsertBrand(Brand brand)
		{
			if (brand == null)
			{
				throw new ArgumentException($"Contact IT: Invalid argument for InsertBrand");
			}

			_uow.BrandRepository.Insert(brand);
			_uow.Save();

			return brand.BrandId;
		}

		public void UpdateBrand(Brand brand)
		{
			if (brand == null)
			{
				throw new ArgumentException($"Contact IT: Invalid argument for UpdateBrand");
			}
			_uow.BrandRepository.Update(brand);
			_uow.Save();
		}

		public void DeleteBrand(int brandId)
		{
			if (!(brandId > 0))
			{
				throw new ArgumentException($"Contact IT: Invalid argument for DeleteBrand where id = {brandId}");
			}

			_uow.BrandRepository.Delete(brandId);
			_uow.Save();
		}

		public IEnumerable<SelectModel> GetBrandList()
		{
			return _uow.BrandRepository.Get().Select(x => new SelectModel
			{
				Description = x.Description,
				Value = x.BrandId.ToString()
			})
			.OrderBy(y => y.Description);
		}

		public IEnumerable<BrandModel> GetBrandInfo()
		{
			var result = _uow.BrandRepository.GetBrandInfo()
				.OrderBy(x => x.BrandOwnerDescription)
				.ThenBy(x => x.Description)
				.ThenBy(x => x.Abbreviation);

			return result;
		}

		public IEnumerable<SelectModel> GetBrandListByBrandOwnerId(int brandOwnerId)
		{
			if (!(brandOwnerId > 0))
			{
				throw new ArgumentException($"Contact IT: Invalid argument for GetBrandListByBrandOwnerId where id = {brandOwnerId}");
			}

			return _uow.BrandRepository.Get(x => x.BrandOwnerId == brandOwnerId).Select(r => new SelectModel
			{
				Description = r.Description,
				Value = r.BrandId.ToString()
			})
			.OrderBy(ps => ps.Description);
		}

		public bool isBrandInProgramLine(int brandId)
		{
			if (!(brandId > 0))
			{
				throw new ArgumentException($"Contact IT: Invalid argument for isBrandInProgramLine where id = {brandId}");
			}

			return _uow.BrandRepository.isBrandInProgramLine(brandId);
		}

		public bool isBrandExist(int brandOwnerId, string description, string abbreviation)
		{
			if (!(brandOwnerId > 0))
			{
				throw new ArgumentException($"Contact IT: Invalid argument for isBrandExist where id = {brandOwnerId}");
			}

			return _uow.BrandRepository.isBrandExist(brandOwnerId, description, abbreviation);
		}

		#endregion Brand

		#region BrandOwner

		public IEnumerable<BrandOwner> GetBrandOwner()
		{
			IEnumerable<BrandOwner> brandOwner = _uow.BrandOwnerRepository.Get()
				.OrderBy(x => x.Description)
				.ThenBy(x => x.Abbreviation);
			return brandOwner;
		}

		public BrandOwner GetBrandOwnerById(int id)
		{
			return _uow.BrandOwnerRepository.GetByID(id);
		}

		public int InsertBrandOwner(BrandOwner brandOwner)
		{
			_uow.BrandOwnerRepository.Insert(brandOwner);
			_uow.Save();

			return brandOwner.BrandOwnerId;
		}

		public void UpdateBrandOwner(BrandOwner brandOwner)
		{
			_uow.BrandOwnerRepository.Update(brandOwner);
			_uow.Save();
		}

		public void DeleteBrandOwner(int brandOwnerId)
		{
			_uow.BrandOwnerRepository.Delete(brandOwnerId);
			_uow.Save();
		}

		public IEnumerable<SelectModel> GetBrandOwnerList()
		{
			return _uow.BrandOwnerRepository.Get().Select(x => new SelectModel
			{
				Description = x.Description,
				Value = x.BrandOwnerId.ToString()
			})
			.OrderBy(y => y.Description);
		}

		public bool isBrandOwnerExist(string description, string abbreviation)
		{
			return _uow.BrandOwnerRepository.isBrandOwnerExist(description, abbreviation);
		}

		public bool isBrandOwnerInProgramHeader(int brandOwnerId)
		{
			return _uow.BrandOwnerRepository.isBrandOwnerInProgramHeader(brandOwnerId);
		}

		public bool isBrandOwnerInBrand(int brandOwnerId)
		{
			return _uow.BrandRepository.Get(x => x.BrandOwnerId == brandOwnerId).Any();
		}

		#endregion BrandOwner

		#region ContainerType

		public IEnumerable<ContainerType> GetContainerType()
		{
			IEnumerable<ContainerType> containerType = _uow.ContainerTypeRepository.Get()
				.OrderBy(x => x.Description)
				.ThenBy(x => x.Abbreviation);
			return containerType;
		}

		public ContainerType GetContainerTypeById(int id)
		{
			return _uow.ContainerTypeRepository.GetByID(id);
		}

		public int InsertContainerType(ContainerType containerType)
		{
			_uow.ContainerTypeRepository.Insert(containerType);
			_uow.Save();

			return containerType.ContainerTypeId;
		}

		public void UpdateContainerType(ContainerType containerType)
		{
			_uow.ContainerTypeRepository.Update(containerType);
			_uow.Save();
		}

		public void DeleteContainerType(int containerTypeId)
		{
			_uow.ContainerTypeRepository.Delete(containerTypeId);
			_uow.Save();
		}

		public IEnumerable<SelectModel> GetContainerTypeList()
		{
			return _uow.ContainerTypeRepository.Get().Select(x => new SelectModel
			{
				Description = x.Description,
				Value = x.ContainerTypeId.ToString()
			})
			.OrderBy(ct => ct.Description);
		}

		public bool isContainerTypeExist(string description, string abbreviation)
		{
			return _uow.ContainerTypeRepository.isContainerTypeExist(description, abbreviation);
		}

		public bool isContainerTypeInPackagingType(int containerTypeId)
		{
			return _uow.PackagingTypeRepository.Get(x => x.ContainerTypeId == containerTypeId).Any();
		}

		public bool isContainerTypeInQuantityRange(int containerTypeId)
		{
			return _uow.QuantityRangeRepository.Get(x => x.ContainerTypeId == containerTypeId).Any();
		}

		#endregion ContainerType

		#region Feature

		public IEnumerable<Feature> GetFeature()
		{
			IEnumerable<Feature> feature = _uow.FeatureRepository.Get();
			return feature;
		}

		public Feature GetFeatureById(int id)
		{
			return _uow.FeatureRepository.GetByID(id);
		}

		public int InsertFeature(Feature feature)
		{
			_uow.FeatureRepository.Insert(feature);
			_uow.Save();

			return feature.FeatureId;
		}

		public void UpdateFeature(Feature feature)
		{
			_uow.FeatureRepository.Update(feature);
			_uow.Save();
		}

		public void DeleteFeature(int id)
		{
			_uow.FeatureRepository.Delete(id);
			_uow.Save();
		}

		public IEnumerable<FeatureModel> GetFeatureInfo()
		{
			return _uow.FeatureRepository.GetFeatureInfo()
				.OrderBy(pc => pc.ProductCategoryDescription)
				.ThenBy(f => f.Description)
				.ThenBy(f => f.Abbreviation);
		}

		public IEnumerable<SelectModel> GetFeatureList()
		{
			return _uow.FeatureRepository.Get().Select(r => new SelectModel
			{
				Description = r.Description,
				Value = r.FeatureId.ToString()
			})
			.OrderBy(f => f.Description);
		}

		public bool isFeatureExist(int productCategoryId, string description, string abbreviation)
		{
			return _uow.FeatureRepository.isFeatureExist(productCategoryId, description, abbreviation);
		}

		public IEnumerable<SelectModel> GetFeatureListByProductType(int productTypeId)
		{
			return _uow.FeatureRepository.GetFeatureListByProductType(productTypeId)
			.OrderBy(f => f.Description);
		}

		public IEnumerable<SelectModel> GetFeatureListByProductCategory(int categoryId)
		{
			return _uow.FeatureRepository.GetFeatureListByProductCategory(categoryId)
			.OrderBy(f => f.Description);
		}

		public bool isFeatureInProductTypeToFeature(int featureId)
		{
			return _uow.ProductTypeToFeatureRepository.Get(x => x.FeatureId == featureId).Any();
		}

		#endregion Feature

		#region FeatureToOption

		public IEnumerable<FeatureToOption> GetFeatureToOption()
		{
			IEnumerable<FeatureToOption> featureToOption = _uow.FeatureToOptionRepository.Get();
			return featureToOption;
		}

		public FeatureToOption GetFeatureToOptionById(int id)
		{
			return _uow.FeatureToOptionRepository.GetByID(id);
		}

		public int InsertFeatureToOption(FeatureToOptionModel featureToOptionModel)
		{
			var pttf = _uow.ProductTypeToFeatureRepository.GetProductTypeToFeatureByKeys(featureToOptionModel.ProductTypeId, featureToOptionModel.FeatureId);

			//If no relationship exists, Insert ProductTypeToFeature table
			var productTypeToFeature = new ProductTypeToFeature();
			var featureToOption = new FeatureToOption();
			if (pttf == null)
			{
				productTypeToFeature.ProductTypeId = featureToOptionModel.ProductTypeId;
				productTypeToFeature.FeatureId = featureToOptionModel.FeatureId;
				_uow.ProductTypeToFeatureRepository.Insert(productTypeToFeature);
				_uow.Save();

				featureToOption.ProductTypeToFeatureId = productTypeToFeature.ProductTypeToFeatureId;  //use new Id
			}
			else
			{
				featureToOption.ProductTypeToFeatureId = pttf.ProductTypeToFeatureId;  //use existing Id
			}

			//Insert FeatureToOption table (NOTE: controller checks if option already exists)
			featureToOption.Description = featureToOptionModel.Description;
			featureToOption.UpdatedOn = featureToOptionModel.UpdatedOn;
			featureToOption.UpdatedBy = featureToOptionModel.UpdatedBy;
			_uow.FeatureToOptionRepository.Insert(featureToOption);
			_uow.Save();

			return featureToOptionModel.FeatureToOptionId;
		}

		public void UpdateFeatureToOption(FeatureToOption featureToOption)
		{
			_uow.FeatureToOptionRepository.Update(featureToOption);
			_uow.Save();
		}

		public void DeleteFeatureToOption(int id)
		{
			_uow.FeatureToOptionRepository.Delete(id);
			_uow.Save();
		}

		public void DeleteProductTypeToFeature(int productTypeToFeatureId)
		{
			_uow.ProductTypeToFeatureRepository.Delete(productTypeToFeatureId);
			_uow.Save();
		}

		public IEnumerable<FeatureToOptionModel> GetFeatureToOptionInfo()
		{
			return _uow.FeatureToOptionRepository.GetFeatureToOptionInfo()
				.OrderBy(pc => pc.ProductCategoryDescription)
				.ThenBy(pt => pt.ProductTypeDescription)
				.ThenBy(f => f.FeatureDescription)
				.ThenBy(f => f.Description);
		}

		public FeatureToOptionModel GetFeatureToOptionInfoById(int featureToOptionId)
		{
			return _uow.FeatureToOptionRepository.GetFeatureToOptionInfoById(featureToOptionId);
		}

		public IEnumerable<SelectModel> GetFeatureToOptionList()
		{
			return _uow.FeatureToOptionRepository.Get().Select(r => new SelectModel
			{
				Description = r.Description,
				Value = r.FeatureToOptionId.ToString()
			})
			.OrderBy(f => f.Description);
		}

		public IEnumerable<SelectModel> GetFeatureToOptionListByFeature(int featureId, int productTypeId)
		{
			return _uow.FeatureToOptionRepository.GetFeatureToOptionListByFeature(featureId, productTypeId)
			.OrderBy(fto => fto.Description);
		}

		public bool isFeatureToOptionExist(int productTypeId, int featureId, string description)
		{
			var pttf = _uow.ProductTypeToFeatureRepository.GetProductTypeToFeatureByKeys(productTypeId, featureId);

			if (pttf != null)
			{
				return _uow.FeatureToOptionRepository.isFeatureToOptionDescriptionExist(pttf.ProductTypeToFeatureId, description);
			}
			else
			{
				return false;
			}

		}

		public bool isProductTypeToFeatureExist(int productTypeToFeatureId)
		{
			var pttf = _uow.FeatureToOptionRepository.isProductTypeToFeatureExist(productTypeToFeatureId);

			return pttf;

		}

		public bool isFeatureToOptionInProductSizeToFeatureToOptionCost(int featureToOptionId)
		{
			return _uow.ProductSizeToFeatureToOptionCostRepository.Get(x => x.FeatureToOptionId == featureToOptionId).Any();
		}

		#endregion FeatureToOption

		#region MfgCost

		public IEnumerable<MfgCost> GetMfgCost()
		{
			IEnumerable<MfgCost> mfgCost = _uow.MfgCostRepository.Get()
				.OrderBy(x => x.ProductType)
				.ThenBy(x => x.VLandOverhead);
			return mfgCost;
		}

		public MfgCost GetMfgCostById(int id)
		{
			return _uow.MfgCostRepository.GetByID(id);
		}

		public int InsertMfgCost(MfgCost mfgCost)
		{
			_uow.MfgCostRepository.Insert(mfgCost);
			_uow.Save();

			return mfgCost.MfgCostId;
		}

		public void UpdateMfgCost(MfgCost mfgCost)
		{
			_uow.MfgCostRepository.Update(mfgCost);
			_uow.Save();
		}

		public void DeleteMfgCost(int mfgCostId)
		{
			_uow.MfgCostRepository.Delete(mfgCostId);
			_uow.Save();
		}

		public IEnumerable<MfgCostModel> GetMfgCostInfo()
		{
			IEnumerable<MfgCostModel> mfgCostInfo = _uow.MfgCostRepository.GetMfgCostInfo()
				.OrderBy(x => x.ProductCategoryDescription)
				.ThenBy(x => x.ProductTypeDescription)
				.ThenBy(x => x.VLandOverhead);
			return mfgCostInfo;
		}

		public bool isMfgCostExist(int productTypeId)
		{
			return _uow.MfgCostRepository.isMfgCostExist(productTypeId);
		}

		#endregion MfgCost

		#region PackagingCost

		public IEnumerable<PackagingCost> GetPackagingCost()
		{
			IEnumerable<PackagingCost> packagingCost = _uow.PackagingCostRepository.Get();
			return packagingCost;
		}

		public PackagingCost GetPackagingCostById(int id)
		{
			return _uow.PackagingCostRepository.GetByID(id);
		}

		public int InsertPackagingCost(PackagingCost packagingCost)
		{
			_uow.PackagingCostRepository.Insert(packagingCost);
			_uow.Save();

			return packagingCost.PackagingCostId;
		}

		public void UpdatePackagingCost(PackagingCost packagingCost)
		{
			_uow.PackagingCostRepository.Update(packagingCost);
			_uow.Save();
		}

		public void DeletePackagingCost(int id)
		{
			_uow.PackagingCostRepository.Delete(id);
			_uow.Save();
		}

		public IEnumerable<PackagingCostModel> GetPackagingCostInfo()
		{
			IEnumerable<PackagingCostModel> packagingCostInfo = _uow.PackagingCostRepository.GetPackagingCostInfo();
			return packagingCostInfo;
		}

		public PackagingCostModel GetPackagingCostInfoById(int packagingCostId)
		{
			PackagingCostModel packagingCostInfoById = _uow.PackagingCostRepository.GetPackagingCostInfoById(packagingCostId);
			return packagingCostInfoById;
		}

		public bool isPackagingCostExist(int productSizeId, int packagingTypeId, int quantityRangeId)
		{
			return _uow.PackagingCostRepository.isPackagingCostExist(productSizeId, packagingTypeId, quantityRangeId);
		}

		#endregion PackagingCost

		#region PackagingType

		public IEnumerable<PackagingType> GetPackagingType()
		{
			IEnumerable<PackagingType> packagingType = _uow.PackagingTypeRepository.Get();
			return packagingType;
		}

		public PackagingType GetPackagingTypeById(int id)
		{
			return _uow.PackagingTypeRepository.GetByID(id);
		}

		public int InsertPackagingType(PackagingType packagingType)
		{
			_uow.PackagingTypeRepository.Insert(packagingType);
			_uow.Save();

			return packagingType.PackagingTypeId;
		}

		public void UpdatePackagingType(PackagingType packagingType)
		{
			_uow.PackagingTypeRepository.Update(packagingType);
			_uow.Save();
		}

		public void DeletePackagingType(int id)
		{
			_uow.PackagingTypeRepository.Delete(id);
			_uow.Save();
		}

		public IEnumerable<PackagingTypeModel> GetPackagingTypeInfo()
		{
			IEnumerable<PackagingTypeModel> packagingTypeInfo = _uow.PackagingTypeRepository.GetPackagingTypeInfo()
				.OrderBy(x => x.ContainerTypeDescription)
				.ThenBy(x => x.Description)
				.ThenBy(x => x.Abbreviation);
			return packagingTypeInfo;
		}

		public IEnumerable<SelectModel> GetPackagingTypeList()
		{
			return _uow.PackagingTypeRepository.Get().Select(x => new SelectModel
			{
				Description = x.Description,
				Value = x.PackagingTypeId.ToString()
			})
			.OrderBy(y => y.Description);
		}
		public IEnumerable<SelectModel> GetPackagingTypeListByContainerType(int containerTypeId)
		{
			return _uow.PackagingTypeRepository.Get(x => x.ContainerTypeId == containerTypeId).Select(y => new SelectModel
			{
				Description = y.Description,
				Value = y.PackagingTypeId.ToString()
			})
			.OrderBy(z => z.Description);
		}

		public IEnumerable<SelectModel> GetPackagingTypeListByContainerTypeProductSize(int containerTypeId, int productSizeId)
		{
			return _uow.PackagingTypeRepository.GetPackagingTypeListByContainerTypeProductSize(containerTypeId, productSizeId).OrderBy(x => x.Description);
		}

		public bool isPackagingTypeExist(int containerType, string description, string abbreviation)
		{
			return _uow.PackagingTypeRepository.isPackagingTypeExist(containerType, description, abbreviation);
		}

		public bool isPackagingTypeInPackagingCost(int packagingTypeId)
		{
			return _uow.PackagingCostRepository.Get(x => x.PackagingTypeId == packagingTypeId).Any();
		}

		#endregion PackagingType

		#region PackStyle

		public IEnumerable<PackStyle> GetPackStyle()
		{
			IEnumerable<PackStyle> packStyle = _uow.PackStyleRepository.Get()
				.OrderBy(pc => pc.Abbreviation)
				.ThenBy(pc => pc.Description);
			return packStyle;
		}

		public PackStyle GetPackStyleById(int id)
		{
			return _uow.PackStyleRepository.GetByID(id);
		}

		public int InsertPackStyle(PackStyle packStyle)
		{
			_uow.PackStyleRepository.Insert(packStyle);
			_uow.Save();

			return packStyle.PackStyleId;
		}

		public void UpdatePackStyle(PackStyle packStyle)
		{
			_uow.PackStyleRepository.Update(packStyle);
			_uow.Save();
		}

		public void DeletePackStyle(int packStyleId)
		{
			_uow.PackStyleRepository.Delete(packStyleId);
			_uow.Save();
		}

		public IEnumerable<SelectModel> GetPackStyleList()
		{
			return _uow.PackStyleRepository.Get().Select(r => new SelectModel
			{
				Description = r.Description,
				Value = r.PackStyleId.ToString()
			})
			.OrderBy(pc => pc.Description);
		}

		public bool isPackStyleInProgramLine(int packStyleId)
		{
			return _uow.PackStyleRepository.isPackStyleInProgramLine(packStyleId);
		}

		public bool isPackStyleExist(int productTypeId, string description, string abbreviation)
		{
			return _uow.PackStyleRepository.isPackStyleExist(productTypeId, description, abbreviation);
		}

		public IEnumerable<PackStyleModel> GetPackStyleInfo()
		{
			var result = _uow.PackStyleRepository.GetPackStyleInfo()
				.OrderBy(x => x.ProductCategoryDescription)
				.ThenBy(x => x.ProductTypeDescription)
				.ThenBy(x => x.Description);

			return result;
		}

		public PackStyleModel GetPackStyleInfoById(int packStyleId)
		{
			PackStyleModel packStyleInfoById = _uow.PackStyleRepository.GetPackStyleInfoById(packStyleId);
			return packStyleInfoById;
		}

		public IEnumerable<SelectModel> GetPackStyleListByProductType(int productTypeId)
		{
			return _uow.PackStyleRepository.Get(x => x.ProductTypeId == productTypeId).Select(r => new SelectModel
			{
				Description = r.Description,
				Value = r.PackStyleId.ToString()
			})
			.OrderBy(ps => ps.Description);
		}

		#endregion PackStyle

		#region ProductCategory
		public IEnumerable<ProductCategory> GetProductCategory()
		{
			IEnumerable<ProductCategory> productCategory = _uow.ProductCategoryRepository.Get()
				.OrderBy(pc => pc.Description)
				.ThenBy(pc => pc.Abbreviation);
			return productCategory;
		}

		public ProductCategory GetProductCategoryById(int id)
		{
			return _uow.ProductCategoryRepository.GetByID(id);
		}

		public int InsertProductCategory(ProductCategory productCategory)
		{
			_uow.ProductCategoryRepository.Insert(productCategory);
			_uow.Save();

			return productCategory.ProductCategoryId;
		}

		public void UpdateProductCategory(ProductCategory productCategory)
		{
			_uow.ProductCategoryRepository.Update(productCategory);
			_uow.Save();
		}

		public void DeleteProductCategory(int productCategoryId)
		{
			_uow.ProductCategoryRepository.Delete(productCategoryId);
			_uow.Save();
		}

		public IEnumerable<SelectModel> GetProductCategoryList()
		{
			return _uow.ProductCategoryRepository.Get().Select(r => new SelectModel
			{
				Description = r.Description,
				Value = r.ProductCategoryId.ToString()
			})
			.OrderBy(pc => pc.Description);
		}

		public bool isProductCategoryExist(string description, string abbreviation)
		{
			return _uow.ProductCategoryRepository.isProductCategoryExist(description, abbreviation);
		}

		public bool isProductCategoryInProductType(int productCategoryId)
		{
			return _uow.ProductTypeRepository.Get(t => t.ProductCategoryId == productCategoryId).Any();
		}

		public bool isProductCategoryInFeature(int productCategoryId)
		{
			return _uow.FeatureRepository.Get(t => t.ProductCategoryId == productCategoryId).Any();
		}

		public bool isProductCategoryInQuantityRange(int productCategoryId)
		{
			return _uow.QuantityRangeRepository.Get(t => t.ProductCategoryId == productCategoryId).Any();
		}

		#endregion ProductCategory

		#region ProductSize

		public IEnumerable<ProductSize> GetProductSize()
		{
			IEnumerable<ProductSize> productSize = _uow.ProductSizeRepository.Get();
			return productSize;
		}

		public ProductSize GetProductSizeById(int id)
		{
			return _uow.ProductSizeRepository.GetByID(id);
		}

		public int InsertProductSize(ProductSize productSize)
		{
			_uow.ProductSizeRepository.Insert(productSize);
			_uow.Save();

			return productSize.ProductSizeId;
		}

		public void UpdateProductSize(ProductSize productSize)
		{
			_uow.ProductSizeRepository.Update(productSize);
			_uow.Save();
		}

		public void DeleteProductSize(int id)
		{
			_uow.ProductSizeRepository.Delete(id);
			_uow.Save();
		}

		public IEnumerable<ProductSizeModel> GetProductSizeInfo()
		{
			IEnumerable<ProductSizeModel> productSizeInfo = _uow.ProductSizeRepository.GetProductSizeInfo()
				.OrderBy(x => x.ProductCategoryDescription)
				.ThenBy(x => x.ProductTypeDescription)
				.ThenBy(x => x.Abbreviation)
				.ThenBy(x => x.Description);
			return productSizeInfo;
		}

		public ProductSizeModel GetProductSizeInfoById(int productSizeId)
		{
			return _uow.ProductSizeRepository.GetProductSizeInfoById(productSizeId);
		}

		public IEnumerable<SelectModel> GetProductSizeList()
		{
			return _uow.ProductSizeRepository.Get().Select(x => new SelectModel
			{
				Description = x.Description,
				Value = x.ProductSizeId.ToString()
			})
			.OrderBy(y => y.Description);
		}

		public IEnumerable<SelectModel> GetProductSizeListByProductType(int productTypeId)
		{
			return _uow.ProductSizeRepository.Get(x => x.ProductTypeId == productTypeId).Select(y => new SelectModel
			{
				Description = y.Description,
				Value = y.ProductSizeId.ToString()
			})
			.OrderBy(z => z.Description);
		}

		public bool isProductSizeExist(int productTypeId, string description, string abbreviation)
		{
			return _uow.ProductSizeRepository.isProductSizeExist(productTypeId, description, abbreviation);
		}

		public bool isProductSizeInPackagingCost(int productSizeId)
		{
			return _uow.PackagingCostRepository.Get(x => x.ProductSizeId == productSizeId).Any();
		}

		public bool isProducSizeInProductSizeToFeatureToOptionCost(int productSizeId)
		{
			return _uow.ProductSizeToFeatureToOptionCostRepository.Get(x => x.ProductSizeId == productSizeId).Any();
		}

		#endregion ProductSize

		#region ProductSizeToFeatureToOptionCost

		public IEnumerable<ProductSizeToFeatureToOptionCost> GetProductSizeToFeatureToOptionCost()
		{
			IEnumerable<ProductSizeToFeatureToOptionCost> productSizeToFeatureToOptionCost = _uow.ProductSizeToFeatureToOptionCostRepository.Get();
			return productSizeToFeatureToOptionCost;
		}

		public ProductSizeToFeatureToOptionCost GetProductSizeToFeatureToOptionCostById(int id)
		{
			return _uow.ProductSizeToFeatureToOptionCostRepository.GetByID(id);
		}

		public int InsertProductSizeToFeatureToOptionCost(ProductSizeToFeatureToOptionCost productSizeToFeatureToOptionCost)
		{
			_uow.ProductSizeToFeatureToOptionCostRepository.Insert(productSizeToFeatureToOptionCost);
			_uow.Save();

			return productSizeToFeatureToOptionCost.ProductSizeToFeatureToOptionCostId;
		}

		public void UpdateProductSizeToFeatureToOptionCost(ProductSizeToFeatureToOptionCost productSizeToFeatureToOptionCost)
		{
			_uow.ProductSizeToFeatureToOptionCostRepository.Update(productSizeToFeatureToOptionCost);
			_uow.Save();
		}

		public void DeleteProductSizeToFeatureToOptionCost(int id)
		{
			_uow.ProductSizeToFeatureToOptionCostRepository.Delete(id);
			_uow.Save();
		}

		public IEnumerable<ProductSizeToFeatureToOptionCostModel> GetProductSizeToFeatureToOptionCostInfo()
		{
			var result = _uow.ProductSizeToFeatureToOptionCostRepository.GetProductSizeToFeatureToOptionCostInfo()
				.OrderBy(x => x.ProductCategoryDescription)
				.ThenBy(x => x.ProductTypeDescription)
				.ThenBy(x => x.ProductSizeDescription)
				.ThenBy(x => x.FeatureDescription)
				.ThenBy(x => x.FeatureToOptionDescription)
				.ThenBy(x => x.Cost);

			return result;
		}

		public ProductSizeToFeatureToOptionCostModel GetProductSizeToFeatureToOptionCostInfoById(int productSizeToFeatureToOptionCost)
		{
			return _uow.ProductSizeToFeatureToOptionCostRepository.GetProductSizeToFeatureToOptionCostInfoById(productSizeToFeatureToOptionCost);
		}

		public bool isProductSizeToFeatureToOptionCostExist(int productSizeId, int featureToOptionId)
		{
			return _uow.ProductSizeToFeatureToOptionCostRepository.isProductSizeToFeatureToOptionCostExist(productSizeId, featureToOptionId);
		}

		public bool IsProductSizeToFeatureToOptionCostInProgramFeatureCost(int productSizeToFeatureToOptionCostId)
		{
			return _uow.ProgramFeatureCostRepository.IsProductSizeToFeatureToOptionCostInProgramFeatureCost(productSizeToFeatureToOptionCostId);
		}

		#endregion ProductSizeToFeatureToOptionCost

		#region ProductType

		public IEnumerable<ProductType> GetProductType()
		{
			IEnumerable<ProductType> productType = _uow.ProductTypeRepository.Get()
				.OrderBy(x => x.ProductCategory.Description)
				.ThenBy(x => x.Description)
				.ThenBy(x => x.Abbreviation);
			return productType;
		}

		public ProductType GetProductTypeById(int id)
		{
			return _uow.ProductTypeRepository.GetByID(id);
		}

		public int InsertProductType(ProductType productType)
		{
			_uow.ProductTypeRepository.Insert(productType);
			_uow.Save();

			return productType.ProductTypeId;
		}

		public void UpdateProductType(ProductType productType)
		{
			_uow.ProductTypeRepository.Update(productType);
			_uow.Save();
		}

		public void DeleteProductType(int productTypeId)
		{
			_uow.ProductTypeRepository.Delete(productTypeId);
			_uow.Save();
		}

		public IEnumerable<ProductTypeModel> GetProductTypeInfo()
		{
			var result = _uow.ProductTypeRepository.GetProductTypeInfo()
				.OrderBy(x => x.ProductCategoryDescription)
				.ThenBy(x => x.Description)
				.ThenBy(x => x.Abbreviation);

			return result;
		}

		public IEnumerable<SelectModel> GetProductTypeList()
		{
			return _uow.ProductTypeRepository.Get().Select(x => new SelectModel
			{
				Description = x.Description,
				Value = x.ProductTypeId.ToString()
			})
			.OrderBy(y => y.Description);
		}

		public IEnumerable<SelectModel> GetProductTypeListByProductCategory(int categoryId)
		{
			return _uow.ProductTypeRepository.Get(x => x.ProductCategoryId == categoryId).Select(r => new SelectModel
			{
				Description = r.Description,
				Value = r.ProductTypeId.ToString()
			})
			.OrderBy(pt => pt.Description);
		}

		public bool isProductTypeExist(int productCategoryId, string description, string abbreviation)
		{
			return _uow.ProductTypeRepository.isProductTypeExist(productCategoryId, description, abbreviation);
		}

		public bool isProductTypeInProductSize(int productTypeId)
		{
			return _uow.ProductSizeRepository.Get(x => x.ProductTypeId == productTypeId).Any();
		}

		public bool isProductTypeInProductTypeToFeature(int productTypeId)
		{
			return _uow.ProductTypeToFeatureRepository.Get(x => x.ProductTypeId == productTypeId).Any();
		}

		public bool isProductTypeInPackStyle(int productTypeId)
		{
			return _uow.PackStyleRepository.Get(x => x.ProductTypeId == productTypeId).Any();
		}

		public bool isProductTypeInMfgCost(int productTypeId)
		{
			return _uow.MfgCostRepository.Get(x => x.ProductTypeId == productTypeId).Any();
		}

		#endregion ProductType

		#region QuantityRange

		public IEnumerable<QuantityRange> GetQuantityRange()
		{
			IEnumerable<QuantityRange> quantityRange = _uow.QuantityRangeRepository.Get()
				.OrderBy(x => x.ProductCategory.Description)
				.ThenBy(x => x.ContainerType.Description)
				.ThenBy(x => x.Minimum)
				.ThenBy(x => x.Maximum);
			return quantityRange;
		}

		public QuantityRange GetQuantityRangeById(int id)
		{
			return _uow.QuantityRangeRepository.GetByID(id);
		}

		public int InsertQuantityRange(QuantityRange quantityRange)
		{
			_uow.QuantityRangeRepository.Insert(quantityRange);
			_uow.Save();

			return quantityRange.QuantityRangeId;
		}

		public void UpdateQuantityRange(QuantityRange quantityRange)
		{
			_uow.QuantityRangeRepository.Update(quantityRange);
			_uow.Save();
		}

		public void DeleteQuantityRange(int quantityRangeId)
		{
			_uow.QuantityRangeRepository.Delete(quantityRangeId);
			_uow.Save();
		}

		public IEnumerable<QuantityRangeModel> GetQuantityRangeInfo()
		{
			var result = _uow.QuantityRangeRepository.GetQuantityRangeInfo()
				.OrderBy(x => x.ProductCategoryDescription)
				.ThenBy(x => x.ContainerTypeDescription)
				.ThenBy(x => x.Minimum)
				.ThenBy(x => x.Maximum);

			return result;
		}

		public IEnumerable<SelectModel> GetQuantityRangeList()
		{
			return _uow.QuantityRangeRepository.Get().Select(x => new SelectModel
			{
				Description = x.Minimum.ToString() + " - " + x.Maximum.ToString(),
				Value = x.QuantityRangeId.ToString()
			})
			.OrderBy(x => int.Parse(x.Value));
		}

		public IEnumerable<SelectModel> GetQuantityRangeListByContainerType(int containerTypeId)
		{
			return _uow.QuantityRangeRepository.Get(x => x.ContainerTypeId == containerTypeId).Select(y => new SelectModel
			{
				Description = y.Minimum.ToString("#,##0") + " - " + y.Maximum.ToString("#,##0"),
				Value = y.QuantityRangeId.ToString()
			})
			.OrderBy(x => int.Parse(x.Value));
		}

		public bool isQuantityRangeExist(int productCategoryId, int containerTypeId, int minimum, int maximum)
		{
			return _uow.QuantityRangeRepository.isQuantityRangeExist(productCategoryId, containerTypeId, minimum, maximum);
		}

		public bool isQuantityRangeInPackagingCost(int quantityRangeId)
		{
			return _uow.PackagingCostRepository.Get(x => x.QuantityRangeId == quantityRangeId).Any();
		}

		#endregion QuantityRange#region BrandOwner

		#region Variant

		public IEnumerable<Variant> GetVariant()
		{
			IEnumerable<Variant> variant = _uow.VariantRepository.Get()
				.OrderBy(x => x.Description)
				.ThenBy(x => x.Abbreviation);
			return variant;
		}

		public Variant GetVariantById(int id)
		{
			return _uow.VariantRepository.GetByID(id);
		}

		public int InsertVariant(Variant variant)
		{
			_uow.VariantRepository.Insert(variant);
			_uow.Save();

			return variant.VariantId;
		}

		public void UpdateVariant(Variant variant)
		{
			_uow.VariantRepository.Update(variant);
			_uow.Save();
		}

		public void DeleteVariant(int variantId)
		{
			_uow.VariantRepository.Delete(variantId);
			_uow.Save();
		}

		public IEnumerable<SelectModel> GetVariantList()
		{
			return _uow.VariantRepository.Get().Select(x => new SelectModel
			{
				Description = x.Description,
				Value = x.VariantId.ToString()
			})
			.OrderBy(y => y.Description);
		}

		public bool isVariantExist(string description, string abbreviation)
		{
			return _uow.VariantRepository.isVariantExist(description, abbreviation);
		}

		public bool isVariantInProgramLine(int variantId)
		{
			return _uow.VariantRepository.isVariantInProgramLine(variantId);
		}

		#endregion Variant
	}
}
