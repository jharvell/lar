﻿using dpc.Business.Interfaces;
using dpc.Common;
using dpc.Data;
using dpc.Data.CustomModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace dpc.Business
{
	public class ProgramManager : BaseManager, IProgramManager
	{
		private readonly IUnitOfWork _uow;

		#region constructors

		public ProgramManager()
		{
			_uow = new UnitOfWork();
		}

		public ProgramManager(IUnitOfWork unitOfWork)
		{
			_uow = unitOfWork;
		}

		#endregion constructors

		#region Customer
		public IEnumerable<Customer> GetCustomer()
		{
			IEnumerable<Customer> customer = _uow.CustomerRepository.Get()
				.OrderBy(p => p.CustomerId);
			return customer;
		}

		public Customer GetCustomerById(int id)
		{
			return _uow.CustomerRepository.GetByID(id);
		}

		public void InsertCustomer(Customer customer)
		{
			customer.UpdatedOn = DateTime.Now;
			customer.UpdatedBy = HttpContext.Current.User.Identity.Name;
			_uow.CustomerRepository.Insert(customer);
			_uow.Save();
		}

		public void UpdateCustomer(Customer customer)
		{
			customer.UpdatedOn = DateTime.Now;
			customer.UpdatedBy = HttpContext.Current.User.Identity.Name;
			_uow.CustomerRepository.Update(customer);
			_uow.Save();
		}

		public void DeleteCustomer(int customerId)
		{
			_uow.CustomerRepository.Delete(customerId);
			_uow.Save();
		}

		#endregion Program

		#region ProgramHeader

		public IEnumerable<ProgramHeader> GetProgramHeader()
		{
			IEnumerable<ProgramHeader> programHeader = _uow.ProgramHeaderRepository.Get()
				.OrderBy(p => p.ProgramHeaderId);
			return programHeader;
		}

		public ProgramHeader GetProgramHeaderById(int id)
		{
			return _uow.ProgramHeaderRepository.GetByID(id);
		}

		public async Task<ProgramHeader> GetProgramHeaderByIdAsync(int id)
		{
			var result = await Task.Run(() =>
			{
				return _uow.ProgramHeaderRepository.GetByID(id);
			});
			return result;
		}

		public void InsertProgramHeader(ProgramHeader programHeader)
		{
			programHeader.UpdatedOn = DateTime.Now;
			programHeader.UpdatedBy = HttpContext.Current.User.Identity.Name;
			programHeader.CreatedOn = DateTime.Now;
			programHeader.CreatedBy = HttpContext.Current.User.Identity.Name;
			_uow.ProgramHeaderRepository.Insert(programHeader);
			_uow.Save();
		}

		public void UpdateProgramHeader(ProgramHeader programHeader)
		{
			programHeader.UpdatedOn = DateTime.Now;
			programHeader.UpdatedBy = HttpContext.Current.User.Identity.Name;
			_uow.ProgramHeaderRepository.Update(programHeader);
			_uow.Save();
		}

		public void DeleteProgramHeader(int programHeaderId)
		{
			_uow.ProgramHeaderRepository.Delete(programHeaderId);
			_uow.Save();
		}

		public IEnumerable<ProgramHeaderModel> GetProgramHeaderInfo()
		{
			IEnumerable<ProgramHeaderModel> programHeaderInfo = _uow.ProgramHeaderRepository.GetProgramHeaderInfo()
				.OrderBy(x => x.ProgramHeaderId);
			return programHeaderInfo;
		}

		public ProgramHeaderModel GetProgramHeaderInfoById(int programHeaderId)
		{
			ProgramHeaderModel programHeaderInfo = _uow.ProgramHeaderRepository.GetProgramHeaderInfoById(programHeaderId);
			return programHeaderInfo;
		}

		public int GetProgramHeaderToProgramLineCount(int id)
		{
			return _uow.ProgramLineRepository.Get(x => x.ProgramHeaderId == id).Count();
		}

		public IEnumerable<SelectModel> GetBrandOwnerList()
		{
			return _uow.BrandOwnerRepository.Get().Select(x => new SelectModel
			{
				Description = x.Description,
				Value = x.BrandOwnerId.ToString()
			})
			.OrderBy(y => y.Description);
		}

		public IEnumerable<SelectModel> GetTransitionTypeList()
		{
			return _uow.ProgramHeaderRepository.GetTransitionTypeList();
		}

		public IEnumerable<SelectModel> GetYesNoList()
		{
			return _uow.ProgramHeaderRepository.GetYesNoList();
		}

		public IEnumerable<SelectModel> GetFreightTermsList()
		{
			return _uow.ProgramHeaderRepository.GetFreightTermsList();
		}

		public void UpdateProgramHeaderCalcs(int programHeaderId)
		{
			_uow.ProgramHeaderRepository.UpdateProgramHeaderCalcs(programHeaderId);
		}

		public void UpdateAllCalcs(int programHeaderId)
		{
			_uow.ProgramHeaderRepository.UpdateAllCalcs(programHeaderId);
		}

		public EnumProgramStatus GetProgramStatus(int programHeaderId)
		{
			var approvedFlag = true;
			var maxCommentOn = DateTime.MinValue;
			var programApprovals = GetProgramApprovalsByProgramHeaderId(programHeaderId);

			if (programApprovals.Count() == 0)
			{
				return EnumProgramStatus.Open;
			}

			foreach (var approval in programApprovals)
			{

				//Get max CommentOn date used to determine "Evaluating" state
				if (approval.CommentOn > maxCommentOn)
				{
					maxCommentOn = approval.CommentOn ?? DateTime.MinValue;
				}
			}

			//IF Status = "Rejected" AND (ProgramHeader.ApprovalSubmittedOn > MAX(ProgramApproval.CommentOn) = Re-Evaluating
			var programHeader = GetProgramHeaderById(programHeaderId);
			if (programHeader.Status == EnumProgramStatus.Rejected.ToString() && programHeader.ApprovalSubmittedOn > maxCommentOn)
			{
				//TODO: Develop better solution here...
				programHeader.Status = EnumProgramStatus.ReEvaluating.ToString();
				UpdateProgramHeader(programHeader);
				return EnumProgramStatus.ReEvaluating;
			}

			foreach (var approval in programApprovals)
			{
				if (approval.Approved == false && programHeader.ApprovalSubmittedOn < maxCommentOn)
				{
					return EnumProgramStatus.Rejected;
				}
				else if (approval.Approved == false || approval.Approved == null)
				{
					approvedFlag = false;
				}
			}

			if (approvedFlag)
			{
				return EnumProgramStatus.Approved;
			}
			else
			{
				return EnumProgramStatus.Evaluating;
			}
		}

		public void UpdateProgramHeaderStatus(int programHeaderId)
		{
			var programHeader = GetProgramHeaderById(programHeaderId);

			programHeader.Status = GetProgramStatus(programHeaderId).ToString();

			UpdateProgramHeader(programHeader);
		}

		#endregion Program

		#region ProgramLine

		public IEnumerable<ProgramLine> GetProgramLine()
		{
			IEnumerable<ProgramLine> programLine = _uow.ProgramLineRepository.Get()
				.OrderBy(p => p.ProgramLineId);
			return programLine;
		}

		public ProgramLine GetProgramLineById(int id)
		{
			return _uow.ProgramLineRepository.GetByID(id);
		}

		public ProgramLineModel GetGridProgramLineById(int id)
		{
			ProgramLineModel programLine = _uow.ProgramLineRepository.GetGridProgramLineById(id);
			programLine.FeatureOptions = _uow.ProgramLineRepository.GetFeatureOptionsByProgramLineId(programLine.ProgramLineId).ToList();
			return programLine;
		}

		public void InsertProgramLine(ProgramLine programLine)
		{
			programLine.UpdatedOn = DateTime.Now;
			programLine.UpdatedBy = HttpContext.Current.User.Identity.Name;
			_uow.ProgramLineRepository.Insert(programLine);
			_uow.Save();
		}

		public void UpdateProgramLine(ProgramLine programLine)
		{
			programLine.UpdatedOn = DateTime.Now;
			programLine.UpdatedBy = HttpContext.Current.User.Identity.Name;
			_uow.ProgramLineRepository.Update(programLine);
			_uow.Save();
		}

		public void DeleteProgramLine(int programLineId)
		{
			_uow.ProgramLineRepository.Delete(programLineId);
			_uow.Save();
		}

		public IEnumerable<ProgramLineModel> GetProgramLinesByProgramHeaderId(int id)
		{
			IEnumerable<ProgramLineModel> programLineList = _uow.ProgramLineRepository.GetProgramLinesByProgramHeaderId(id).ToList();

			foreach (var programLine in programLineList)
			{
				programLine.FeatureOptions = _uow.ProgramLineRepository.GetFeatureOptionsByProgramLineId(programLine.ProgramLineId).ToList();
			}
			return programLineList;
		}

		public IEnumerable<Feature> GetFeaturesByProductCategory(int productCategoryId)
		{
			IEnumerable<Feature> feature = _uow.ProgramLineRepository.GetFeaturesByProductCategory(productCategoryId);
			return feature;
		}

		public IEnumerable<SelectModel> GetFeatureOptionList(int featureId, int productTypeId, int productSizeId)
		{
			IEnumerable<SelectModel> featureToOption = _uow.ProgramLineRepository.GetFeatureOptionList(featureId, productTypeId, productSizeId);
			return featureToOption;
		}

		public ProductSizeToFeatureToOptionCostModel GetProductSizeToFeatureToOptionCost(int productSizeId, int featureToOptionId)
		{
			ProductSizeToFeatureToOptionCostModel productSizeToFeatureToOptionCost = _uow.ProgramLineRepository.GetProductSizeToFeatureToOptionCost(productSizeId, featureToOptionId);
			return productSizeToFeatureToOptionCost;
		}

		public void InsertProgramFeatureCost(int programLineId, int productSizeToFeatureToOptionCostId, double cost)
		{
			ProgramFeatureCost programFeatureCost = new ProgramFeatureCost();

			programFeatureCost.ProgramLineId = programLineId;
			programFeatureCost.ProductSizeToFeatureToOptionCostId = productSizeToFeatureToOptionCostId;
			programFeatureCost.OptionCost = cost;

			_uow.ProgramFeatureCostRepository.Insert(programFeatureCost);
			_uow.Save();
		}

		public void DeleteProgramFeatureCostByProgramLineId(int programLineId)
		{
			_uow.ProgramFeatureCostRepository.DeleteProgramFeatureCostByProgramLineId(programLineId);
		}

		public DataTable GetProgramLinesDataByProgramHeaderId(int programHeaderId)
		{
			IEnumerable<ProgramLineModel> programLines = GetProgramLinesByProgramHeaderId(programHeaderId);

			// build initial table and add columns
			var dataTable = new DataTable();
			dataTable.Columns.Add("ProgramHeaderId", typeof(string));
			dataTable.Columns.Add("ProgramLineId", typeof(string));
			dataTable.Columns.Add("Product", typeof(string));
			dataTable.Columns.Add("Size", typeof(string));
			dataTable.Columns.Add("PackStyle", typeof(string));
			dataTable.Columns.Add("ConsumableUnit", typeof(string));
			dataTable.Columns.Add("CustomSku", typeof(string));
			dataTable.Columns.Add("SecondaryPackagingType", typeof(string));
			dataTable.Columns.Add("ShipperType", typeof(string));
			dataTable.Columns.Add("Brand", typeof(string));
			dataTable.Columns.Add("Variant", typeof(string));

			// get all features for product category
			ProgramHeader ph = GetProgramHeaderById(programHeaderId);
			IEnumerable<Feature> features = GetFeaturesByProductCategory(ph.ProductCategoryId);

			// add feature columns to the table definition
			foreach (Feature feature in features)
			{
				DataColumn dc = new DataColumn(feature.Description.Replace(" ", "").Replace("/", ""), typeof(string));
				dataTable.Columns.Add(dc);
			}

			// add more columns to table
			dataTable.Columns.Add("BagCount", typeof(int));
			dataTable.Columns.Add("BagsPerCase", typeof(int));
			dataTable.Columns.Add("AnnualCases", typeof(int));
			dataTable.Columns.Add("CaseSellingPrice", typeof(double));
			dataTable.Columns.Add("Pieces", typeof(double));
			dataTable.Columns.Add("PiecePrice", typeof(double));
			dataTable.Columns.Add("ContributionMargin", typeof(double));
			dataTable.Columns.Add("ContributionMarginPercent", typeof(double));
			dataTable.Columns.Add("VariableMargin", typeof(double));
			dataTable.Columns.Add("VariableMarginPercent", typeof(double));
			dataTable.Columns.Add("NetSales", typeof(double));

			DataRow dr;
			foreach (ProgramLineModel programLine in programLines)
			{
				dr = dataTable.NewRow();
				dr["ProgramHeaderId"] = programHeaderId;
				dr["ProgramLineId"] = programLine.ProgramLineId;
				dr["Product"] = programLine.ProductTypeAbbreviation;
				dr["Size"] = programLine.ProductSizeDescription;
				dr["PackStyle"] = programLine.PackStyleDescription;
				dr["ConsumableUnit"] = programLine.ContainerTypeDescription;
				dr["CustomSku"] = programLine.CustomSKU;
				dr["SecondaryPackagingType"] = programLine.BagTypeToPackagingTypeDescription;
				dr["ShipperType"] = programLine.CaseTypeToPackagingTypeDescription;
				dr["Brand"] = programLine.BrandDescription;
				dr["Variant"] = programLine.VariantDescription;
				dr["BagCount"] = programLine.BagCount;
				dr["BagsPerCase"] = programLine.BagsPerCase;
				dr["AnnualCases"] = programLine.AnnualCases;
				dr["CaseSellingPrice"] = programLine.CaseSellingPrice;
				dr["Pieces"] = programLine.Pieces / 1000;
				dr["PiecePrice"] = programLine.PiecePrice;
				dr["ContributionMargin"] = programLine.ContributionMargin / 1000;
				dr["ContributionMarginPercent"] = programLine.ContributionMarginPercent;
				dr["VariableMargin"] = programLine.VariableMargin / 1000;
				dr["VariableMarginPercent"] = programLine.VariableMarginPercent;
				dr["NetSales"] = programLine.NetSales / 1000;

				//add values to features (seleted options)
				foreach (var option in programLine.FeatureOptions)
				{
					var featureName = option.FeatureDescription.Replace(" ", "").Replace("/", "");

					dr[featureName] = option.FeatureToOptionDescription;
				}

				dataTable.Rows.Add(dr);
			}

			return dataTable;
		}

		public DataTable GetProgramLinesDataForLRRdetailsReport(int programHeaderId)
		{
			IEnumerable<ProgramLineModel> programLines = GetProgramLinesByProgramHeaderId(programHeaderId);

			// build initial table and add columns
			var dataTable = new DataTable();
			dataTable.Columns.Add("ProgramHeaderId", typeof(string));
			dataTable.Columns.Add("ProgramLineId", typeof(string));
			dataTable.Columns.Add("Product", typeof(string));
			dataTable.Columns.Add("Size", typeof(string));
			dataTable.Columns.Add("PackStyle", typeof(string));
			dataTable.Columns.Add("ConsumableUnit", typeof(string));
			dataTable.Columns.Add("CustomSku", typeof(string));
			dataTable.Columns.Add("SecondaryPackagingType", typeof(string));
			dataTable.Columns.Add("ShipperType", typeof(string));
			dataTable.Columns.Add("Brand", typeof(string));
			dataTable.Columns.Add("Variant", typeof(string));

			// get all features for product category
			ProgramHeader ph = GetProgramHeaderById(programHeaderId);
			IEnumerable<Feature> features = GetFeaturesByProductCategory(ph.ProductCategoryId);

			// add feature columns to the table definition
			foreach (Feature feature in features)
			{
				DataColumn dc = new DataColumn(feature.Description.Replace(" ", "").Replace("/", ""), typeof(string));
				dataTable.Columns.Add(dc);
			}

			// add more columns to table
			dataTable.Columns.Add("BagCount", typeof(int));
			dataTable.Columns.Add("BagsPerCase", typeof(int));
			dataTable.Columns.Add("AnnualCases", typeof(int));
			dataTable.Columns.Add("Pieces", typeof(double));

			DataRow dr;
			foreach (ProgramLineModel programLine in programLines)
			{
				dr = dataTable.NewRow();
				dr["ProgramHeaderId"] = programHeaderId;
				dr["ProgramLineId"] = programLine.ProgramLineId;
				dr["Product"] = programLine.ProductTypeAbbreviation;
				dr["Size"] = programLine.ProductSizeDescription;
				dr["PackStyle"] = programLine.PackStyleDescription;
				dr["ConsumableUnit"] = programLine.ContainerTypeDescription;
				dr["CustomSku"] = programLine.CustomSKU;
				dr["SecondaryPackagingType"] = programLine.BagTypeToPackagingTypeDescription;
				dr["ShipperType"] = programLine.CaseTypeToPackagingTypeDescription;
				dr["Brand"] = programLine.BrandDescription;
				dr["Variant"] = programLine.VariantDescription;
				dr["BagCount"] = programLine.BagCount;
				dr["BagsPerCase"] = programLine.BagsPerCase;
				dr["AnnualCases"] = programLine.AnnualCases;
				dr["Pieces"] = programLine.Pieces / 1000;

				//add values to features (seleted options)
				foreach (var option in programLine.FeatureOptions)
				{
					var featureName = option.FeatureDescription.Replace(" ", "").Replace("/", "");

					dr[featureName] = option.FeatureToOptionDescription;
				}

				dataTable.Rows.Add(dr);
			}

			return dataTable;
		}

		public DataTable GetProgramLinesDataForLRRfinancialsReport(int programHeaderId)
		{
			IEnumerable<ProgramLineModel> programLines = GetProgramLinesByProgramHeaderId(programHeaderId);

			// build initial table and add columns
			var dataTable = new DataTable();
			dataTable.Columns.Add("ProgramHeaderId", typeof(string));
			dataTable.Columns.Add("ProgramLineId", typeof(string));
			dataTable.Columns.Add("Product", typeof(string));
			dataTable.Columns.Add("Size", typeof(string));
			dataTable.Columns.Add("PackStyle", typeof(string));
			dataTable.Columns.Add("ConsumableUnit", typeof(string));
			dataTable.Columns.Add("CustomSku", typeof(string));
			dataTable.Columns.Add("SecondaryPackagingType", typeof(string));
			dataTable.Columns.Add("ShipperType", typeof(string));
			dataTable.Columns.Add("Brand", typeof(string));
			dataTable.Columns.Add("Variant", typeof(string));

			// add more columns to table
			dataTable.Columns.Add("BagCount", typeof(int));
			dataTable.Columns.Add("BagsPerCase", typeof(int));
			dataTable.Columns.Add("CaseSellingPrice", typeof(double));

			DataRow dr;
			foreach (ProgramLineModel programLine in programLines)
			{
				dr = dataTable.NewRow();
				dr["ProgramHeaderId"] = programHeaderId;
				dr["ProgramLineId"] = programLine.ProgramLineId;
				dr["Product"] = programLine.ProductTypeAbbreviation;
				dr["Size"] = programLine.ProductSizeDescription;
				dr["PackStyle"] = programLine.PackStyleDescription;
				dr["ConsumableUnit"] = programLine.ContainerTypeDescription;
				dr["CustomSku"] = programLine.CustomSKU;
				dr["SecondaryPackagingType"] = programLine.BagTypeToPackagingTypeDescription;
				dr["ShipperType"] = programLine.CaseTypeToPackagingTypeDescription;
				dr["Brand"] = programLine.BrandDescription;
				dr["Variant"] = programLine.VariantDescription;
				dr["BagCount"] = programLine.BagCount;
				dr["BagsPerCase"] = programLine.BagsPerCase;
				dr["CaseSellingPrice"] = programLine.CaseSellingPrice;

				dataTable.Rows.Add(dr);
			}

			return dataTable;
		}

		public DataTable GetProgramLinesDataForCalculationsReport(int programHeaderId)
		{
			IEnumerable<ProgramLineModel> programLines = GetProgramLinesByProgramHeaderId(programHeaderId);

			// build initial table and add columns
			var dataTable = new DataTable();
			dataTable.Columns.Add("ProgramHeaderId", typeof(string));
			dataTable.Columns.Add("ProgramLineId", typeof(string));
			dataTable.Columns.Add("Product", typeof(string));
			dataTable.Columns.Add("Size", typeof(string));
			dataTable.Columns.Add("PackStyle", typeof(string));
			dataTable.Columns.Add("PiecePrice", typeof(double));
			dataTable.Columns.Add("Pieces", typeof(double));
			dataTable.Columns.Add("BagCount", typeof(int));
			dataTable.Columns.Add("CaseCount", typeof(int));
			dataTable.Columns.Add("Cases", typeof(int));
			dataTable.Columns.Add("GrossSales", typeof(double));
			dataTable.Columns.Add("Discounts", typeof(double));
			dataTable.Columns.Add("Fees", typeof(double));
			dataTable.Columns.Add("AllOther", typeof(double));
			dataTable.Columns.Add("BrokerFees", typeof(double));
			dataTable.Columns.Add("NetSales", typeof(double));

			//// get all features for product category
			ProgramHeader ph = GetProgramHeaderById(programHeaderId);
			IEnumerable<Feature> features = GetFeaturesByProductCategory(ph.ProductCategoryId);

			// add feature columns to the table definition
			foreach (Feature feature in features)
			{
				DataColumn dc =
					new DataColumn(feature.Description.Replace(" ", "").Replace("/", ""), typeof(string))
					{
						DataType = Type.GetType("System.Decimal")
					};
				dataTable.Columns.Add(dc);
			}

			dataTable.Columns.Add("Scrap", typeof(double));
			dataTable.Columns.Add("TotalBags", typeof(double));
			dataTable.Columns.Add("TotalBoxes", typeof(double));
			dataTable.Columns.Add("Plates", typeof(double));
			dataTable.Columns.Add("Pallets", typeof(double));
			dataTable.Columns.Add("VlOverhead", typeof(double));
			dataTable.Columns.Add("FixedCost", typeof(double));
			dataTable.Columns.Add("OceanFreight", typeof(double));
			dataTable.Columns.Add("CustFreight", typeof(double));
			dataTable.Columns.Add("Depr", typeof(double));
			dataTable.Columns.Add("VariableMargin", typeof(double));
			dataTable.Columns.Add("ContributionMargin", typeof(double));
			dataTable.Columns.Add("ContributionMarginPercent", typeof(double));
			dataTable.Columns.Add("VariableMarginPercent", typeof(double));
			dataTable.Columns.Add("BagCost", typeof(double));
			dataTable.Columns.Add("BoxCost", typeof(double));
			dataTable.Columns.Add("MaterialPerPad", typeof(double));
			dataTable.Columns.Add("VLPerPad", typeof(double));

			DataRow dr;
			foreach (ProgramLineModel programLine in programLines)
			{
				dr = dataTable.NewRow();
				dr["ProgramHeaderId"] = programHeaderId;
				dr["ProgramLineId"] = programLine.ProgramLineId;
				dr["Product"] = programLine.ProductTypeAbbreviation;
				dr["Size"] = programLine.ProductSizeDescription;
				dr["PackStyle"] = programLine.PackStyleDescription;
				dr["PiecePrice"] = programLine.PiecePrice;
				dr["Pieces"] = programLine.Pieces;
				dr["BagCount"] = programLine.BagCount;
				dr["CaseCount"] = programLine.CaseCount;
				dr["Cases"] = programLine.BagsPerCase;
				dr["GrossSales"] = programLine.GrossSales;
				dr["Discounts"] = programLine.Discounts;
				dr["Fees"] = programLine.Fees;
				dr["AllOther"] = programLine.Other;
				dr["BrokerFees"] = programLine.BrokerFees;
				dr["NetSales"] = programLine.NetSales;

				//add values to features (seleted options)
				foreach (var option in programLine.FeatureOptions)
				{
					var featureName = option.FeatureDescription.Replace(" ", "").Replace("/", "");
					dr[featureName] = option.FeatureOptionCost;
				}

				dr["Scrap"] = programLine.Scrap;
				dr["TotalBags"] = programLine.TotalBags;
				dr["TotalBoxes"] = programLine.TotalBoxes;
				dr["Plates"] = programLine.Plates;
				dr["Pallets"] = programLine.Pallets;
				dr["VlOverhead"] = programLine.VlOverhead;
				dr["FixedCost"] = programLine.FixedCost;
				dr["OceanFreight"] = programLine.OceanFreight;
				dr["CustFreight"] = programLine.CustFreight;
				dr["Depr"] = programLine.Depr;
				dr["VariableMargin"] = programLine.VariableMargin;
				dr["ContributionMargin"] = programLine.ContributionMargin;
				dr["ContributionMarginPercent"] = programLine.ContributionMarginPercent;
				dr["VariableMarginPercent"] = programLine.VariableMarginPercent;
				dr["BagCost"] = programLine.BagCost;
				dr["BoxCost"] = programLine.BoxCost;
				dr["MaterialPerPad"] = programLine.MaterialPerPad;
				dr["VLPerPad"] = programLine.VLPerPad;

				dataTable.Rows.Add(dr);
			}

			return dataTable;
		}

		public void UpdateProgramLineCalcs(int programLineId)
		{
			_uow.ProgramLineRepository.UpdateProgramLineCalcs(programLineId);
		}

		public void UpdateProgramFeatureCostInProgramLines(int programHeaderId)
		{
			IEnumerable<ProgramLineModel> programLineList = _uow.ProgramLineRepository.GetProgramLinesByProgramHeaderId(programHeaderId).ToList();

			foreach (var programLine in programLineList)
			{
				IEnumerable<ProgramFeatureCost> programFeatureCostList = _uow.ProgramFeatureCostRepository.Get(x => x.ProgramLineId == programLine.ProgramLineId).ToList();

				foreach (var programFeatureCost in programFeatureCostList)
				{
					//Get latest cost from Maintenance table
					var productSizeToFeatureToOptionCost = _uow.ProductSizeToFeatureToOptionCostRepository.GetByID(programFeatureCost.ProductSizeToFeatureToOptionCostId);
				
					//Update ProgramFeatureCost for Program Line
					programFeatureCost.OptionCost = productSizeToFeatureToOptionCost.Cost;
					_uow.ProgramFeatureCostRepository.Update(programFeatureCost);
					_uow.Save();
				}
			}
		}

		#endregion ProgramLine

		#region Approval

		public IEnumerable<ProgramApproval> GetProgramApprovalsByProgramHeaderId(int id)
		{
			return _uow.ProgramApprovalRepository.GetProgramApprovalsByProgramHeaderId(id);
		}

		public ProgramApproval GetProgramApprovalById(int id)
		{
			ProgramApproval programApproval = _uow.ProgramApprovalRepository.GetByID(id);

			return programApproval;
		}

		public ProgramApproval GetProgramApprovalByUserId(int programHeaderId, string userId)
		{
			ProgramApproval programApproval = _uow.ProgramApprovalRepository.GetProgramApprovalByUserId(programHeaderId, userId);

			return programApproval;
		}

		public IEnumerable<ProgramApproverModel> GetProgramApproverList(string rolename)
		{
			IEnumerable<ProgramApproverModel> approvers = _uow.ProgramApprovalRepository.GetProgramApproverList(rolename);

			return approvers;
		}

		public IEnumerable<ProgramApproverModel> GetProgramApproverListByProgramHeaderId(string rolename, int programHeaderId)
		{
			IEnumerable<ProgramApproverModel> approvers = _uow.ProgramApprovalRepository.GetProgramApproverListByProgramHeaderId(rolename, programHeaderId).ToList();
			return approvers;
		}

		public void UpdateSubmitForApproval(ProgramHeader programHeader)
		{
			programHeader.ApprovalSubmittedOn = DateTime.Now;
			programHeader.ApprovalSubmittedBy = HttpContext.Current.User.Identity.Name;
			_uow.ProgramHeaderRepository.Update(programHeader);
			_uow.Save();
		}

		public void InsertProgramApproval(ProgramApproval programApproval)
		{
			_uow.ProgramApprovalRepository.Insert(programApproval);
			_uow.Save();
		}

		public void UpdateProgamApproval(ProgramApproval programApproval)
		{
			programApproval.CommentOn = DateTime.Now;
			_uow.ProgramApprovalRepository.Update(programApproval);
			_uow.Save();
		}


		public bool isProgramSubmitted(int programHeaderId)
		{
			var status = GetProgramStatus(programHeaderId);

			//If Open the program has NOT been submitted
			if (status == EnumProgramStatus.Open)
			{
				return false;
			}
			return true;
		}

		public bool isProgramApproved(int programHeaderId)
		{
			var status = GetProgramStatus(programHeaderId);

			if (status == EnumProgramStatus.Approved)
			{
				return true;
			}
			return false;
		}

		public bool isProgramRejected(int programHeaderId)
		{
			var status = GetProgramStatus(programHeaderId);

			if (status == EnumProgramStatus.Rejected)
			{
				return true;
			}
			return false;
		}


		//public bool isProgamApprovalSubmitted(int programHeaderId)
		//{
		//	return _uow.ProgramApprovalRepository.isProgamApprovalSubmitted(programHeaderId);
		//}

		//public bool isProgamEditable(int programHeaderId)
		//{
		//	var status = GetProgramStatus(programHeaderId);

		//	if (status == "Open" || status == "Rejected")
		//	{
		//		return true;
		//	}
		//	return false;
		//}

		#endregion Approval
	}
}
