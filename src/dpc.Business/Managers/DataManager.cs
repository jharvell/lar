﻿using dpc.Business.Interfaces;
using dpc.Data;
using dpc.Data.CustomModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dpc.Business
{
    public class DataManager : BaseManager, IDataManager
    {
        private IUnitOfWork _uow;

        #region constructors

        public DataManager()
        {
            _uow = new UnitOfWork();
        }

        public DataManager(IUnitOfWork unitOfWork)
        {
            _uow = unitOfWork;
        }

        #endregion constructors

        #region Claim

        public bool isRoleInClaim(string roleName)
        {
            return _uow.ClaimRepository.isRoleInClaim(roleName);
        }

        #endregion Claim

        #region PasswordHistory

        public void InsertPasswordHistory(PasswordHistory PasswordHistory)
        {
            PasswordHistory.CreateDate = DateTime.Now;
            _uow.PasswordHistoryRepository.Insert(PasswordHistory);
            _uow.Save();
        }

        public IEnumerable<string> GetPasswordHistory(string userId)
        {
            return _uow.PasswordHistoryRepository.GetPasswordHistory(userId);
        }

        public DateTime GetLastSavedPasswordDate(string userId)
        {
            return _uow.PasswordHistoryRepository.GetLastSavedPasswordDate(userId);
        }

        #endregion PasswordHistory
    }
}
