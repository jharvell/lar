﻿using dpc.Data;
using dpc.Data.CustomModels;
using System.Collections.Generic;

namespace dpc.Business.Interfaces
{
	public interface IMaintenanceManager
	{
		#region Brand
		Brand GetBrandById(int id);

		int InsertBrand(Brand brand);

		void UpdateBrand(Brand brand);

		void DeleteBrand(int brandId);

		IEnumerable<SelectModel> GetBrandList();

		IEnumerable<BrandModel> GetBrandInfo();

		IEnumerable<SelectModel> GetBrandListByBrandOwnerId(int brandOwnerId);

		bool isBrandInProgramLine(int brandId);

		bool isBrandExist(int brandOwnerId, string description, string abbreviation);
		#endregion Brand

		#region BrandOwner
		IEnumerable<BrandOwner> GetBrandOwner();

		BrandOwner GetBrandOwnerById(int id);

		int InsertBrandOwner(BrandOwner brandOwner);

		void UpdateBrandOwner(BrandOwner brandOwner);

		void DeleteBrandOwner(int brandOwnerId);

		IEnumerable<SelectModel> GetBrandOwnerList();

		bool isBrandOwnerExist(string description, string abbreviation);

		bool isBrandOwnerInProgramHeader(int brandOwnerId);

		bool isBrandOwnerInBrand(int brandOwnerId);
		#endregion BrandOwner

		#region ContainerType
		IEnumerable<ContainerType> GetContainerType();

		ContainerType GetContainerTypeById(int id);

		int InsertContainerType(ContainerType containerType);

		void UpdateContainerType(ContainerType containerType);

		void DeleteContainerType(int containerTypeId);

		IEnumerable<SelectModel> GetContainerTypeList();

		bool isContainerTypeExist(string description, string abbreviation);

		bool isContainerTypeInPackagingType(int containerTypeId);

		bool isContainerTypeInQuantityRange(int containerTypeId);
		#endregion ContainerType

		#region Feature
		IEnumerable<Feature> GetFeature();

		Feature GetFeatureById(int id);

		int InsertFeature(Feature feature);

		void UpdateFeature(Feature feature);

		void DeleteFeature(int featureId);

		IEnumerable<FeatureModel> GetFeatureInfo();

		IEnumerable<SelectModel> GetFeatureList();

		bool isFeatureExist(int productCategoryId, string description, string abbreviation);

		IEnumerable<SelectModel> GetFeatureListByProductType(int productTypeId);

		IEnumerable<SelectModel> GetFeatureListByProductCategory(int categoryId);

		bool isFeatureInProductTypeToFeature(int featureId);
		#endregion Feature

		#region FeatureToOption
		IEnumerable<FeatureToOption> GetFeatureToOption();

		FeatureToOption GetFeatureToOptionById(int id);

		int InsertFeatureToOption(FeatureToOptionModel featureToOption);

		void UpdateFeatureToOption(FeatureToOption featureToOption);

		void DeleteFeatureToOption(int featureToOptionId);

		void DeleteProductTypeToFeature(int productTypeToFeatureId);

		IEnumerable<FeatureToOptionModel> GetFeatureToOptionInfo();

		IEnumerable<SelectModel> GetFeatureToOptionList();

		FeatureToOptionModel GetFeatureToOptionInfoById(int featureToOptionId);

		IEnumerable<SelectModel> GetFeatureToOptionListByFeature(int featureId, int productTypeId);

		bool isFeatureToOptionExist(int productTypeId, int FeatureId, string description);

		bool isProductTypeToFeatureExist(int ProductTypeToFeatureId);

		bool isFeatureToOptionInProductSizeToFeatureToOptionCost(int featureToOptionId);

		#endregion FeatureToOption

		#region MfgCost
		IEnumerable<MfgCost> GetMfgCost();

		MfgCost GetMfgCostById(int id);

		int InsertMfgCost(MfgCost mfgCost);

		void UpdateMfgCost(MfgCost mfgCost);

		void DeleteMfgCost(int mfgCostId);

		IEnumerable<MfgCostModel> GetMfgCostInfo();

		bool isMfgCostExist(int productTypeId);
		#endregion MfgCost

		#region PackagingCost
		IEnumerable<PackagingCost> GetPackagingCost();

		PackagingCost GetPackagingCostById(int id);

		int InsertPackagingCost(PackagingCost packagingCost);

		void UpdatePackagingCost(PackagingCost packagingCost);

		void DeletePackagingCost(int packagingCostId);

		IEnumerable<PackagingCostModel> GetPackagingCostInfo();

		PackagingCostModel GetPackagingCostInfoById(int packagingCostId);

		bool isPackagingCostExist(int productSizeId, int packagingTypeId, int quantityRangeId);
		#endregion PackagingCost

		#region PackagingType
		IEnumerable<PackagingType> GetPackagingType();

		PackagingType GetPackagingTypeById(int id);

		int InsertPackagingType(PackagingType packagingType);

		void UpdatePackagingType(PackagingType packagingType);

		void DeletePackagingType(int packagingTypeId);

		IEnumerable<PackagingTypeModel> GetPackagingTypeInfo();

		IEnumerable<SelectModel> GetPackagingTypeList();

		IEnumerable<SelectModel> GetPackagingTypeListByContainerType(int containerTypeId);

		IEnumerable<SelectModel> GetPackagingTypeListByContainerTypeProductSize(int containerTypeId, int productSizeId);

		bool isPackagingTypeExist(int containerType, string description, string abbreviation);

		bool isPackagingTypeInPackagingCost(int packagingTypeId);
		#endregion ProductType

		#region PackStyle
		IEnumerable<PackStyle> GetPackStyle();

		PackStyle GetPackStyleById(int id);

		int InsertPackStyle(PackStyle packStyle);

		void UpdatePackStyle(PackStyle packStyle);

		void DeletePackStyle(int packStyleId);

		IEnumerable<SelectModel> GetPackStyleList();

		bool isPackStyleInProgramLine(int PackStyleId);

		bool isPackStyleExist(int productTypeId, string abbreviation, string description);

		IEnumerable<PackStyleModel> GetPackStyleInfo();

		PackStyleModel GetPackStyleInfoById(int packStyleId);

		IEnumerable<SelectModel> GetPackStyleListByProductType(int productTypeId);
		#endregion PackStyle

		#region ProductCategory
		IEnumerable<ProductCategory> GetProductCategory();

		ProductCategory GetProductCategoryById(int id);

		int InsertProductCategory(ProductCategory productCategory);

		void UpdateProductCategory(ProductCategory productCategory);

		void DeleteProductCategory(int productCategoryId);

		IEnumerable<SelectModel> GetProductCategoryList();

		bool isProductCategoryExist(string description, string abbreviation);

		bool isProductCategoryInProductType(int productCategoryId);

		bool isProductCategoryInFeature(int productCategoryId);

		bool isProductCategoryInQuantityRange(int productCategoryId);
		#endregion ProductCategory

		#region ProductSize
		IEnumerable<ProductSize> GetProductSize();

		ProductSize GetProductSizeById(int id);

		int InsertProductSize(ProductSize productSize);

		void UpdateProductSize(ProductSize productSize);

		void DeleteProductSize(int productSizeId);

		IEnumerable<ProductSizeModel> GetProductSizeInfo();

		ProductSizeModel GetProductSizeInfoById(int productSizeId);

		IEnumerable<SelectModel> GetProductSizeList();

		IEnumerable<SelectModel> GetProductSizeListByProductType(int productTypeId);

		bool isProductSizeExist(int productTypeId, string description, string abbreviation);

		bool isProductSizeInPackagingCost(int productSizeId);

		bool isProducSizeInProductSizeToFeatureToOptionCost(int productSizeId);
		#endregion ProductSize

		#region ProductSizeToFeatureToOptionCost
		IEnumerable<ProductSizeToFeatureToOptionCost> GetProductSizeToFeatureToOptionCost();

		ProductSizeToFeatureToOptionCost GetProductSizeToFeatureToOptionCostById(int id);

		int InsertProductSizeToFeatureToOptionCost(ProductSizeToFeatureToOptionCost productSizeToFeatureToOptionCost);

		void UpdateProductSizeToFeatureToOptionCost(ProductSizeToFeatureToOptionCost productSizeToFeatureToOptionCost);

		void DeleteProductSizeToFeatureToOptionCost(int productSizeToFeatureToOptionCostId);

		IEnumerable<ProductSizeToFeatureToOptionCostModel> GetProductSizeToFeatureToOptionCostInfo();

		ProductSizeToFeatureToOptionCostModel GetProductSizeToFeatureToOptionCostInfoById(int productSizeToFeatureToOptionCostId);

		bool isProductSizeToFeatureToOptionCostExist(int featureToProductSizeId, int featureToOptionId);

		bool IsProductSizeToFeatureToOptionCostInProgramFeatureCost(int productSizeToFeatureToOptionCostId);
		#endregion ProductSizeToFeatureToOptionCost

		#region ProductType
		IEnumerable<ProductType> GetProductType();

		ProductType GetProductTypeById(int id);

		int InsertProductType(ProductType productType);

		void UpdateProductType(ProductType productType);

		void DeleteProductType(int productTypeId);

		IEnumerable<ProductTypeModel> GetProductTypeInfo();

		IEnumerable<SelectModel> GetProductTypeList();

		IEnumerable<SelectModel> GetProductTypeListByProductCategory(int categoryId);

		bool isProductTypeExist(int productTypeId, string description, string abbreviation);

		bool isProductTypeInProductSize(int productTypeId);

		bool isProductTypeInProductTypeToFeature(int productTypeId);

		bool isProductTypeInPackStyle(int productTypeId);

		bool isProductTypeInMfgCost(int productTypeId);
		#endregion ProductType

		#region QuantityRange
		IEnumerable<QuantityRange> GetQuantityRange();

		QuantityRange GetQuantityRangeById(int id);

		int InsertQuantityRange(QuantityRange quantityRange);

		void UpdateQuantityRange(QuantityRange quantityRange);

		void DeleteQuantityRange(int quantityRangeId);

		IEnumerable<QuantityRangeModel> GetQuantityRangeInfo();

		IEnumerable<SelectModel> GetQuantityRangeList();

		IEnumerable<SelectModel> GetQuantityRangeListByContainerType(int containerTypeId);

		bool isQuantityRangeExist(int productCategoryId, int containerTypeId, int minimum, int maximum);

		bool isQuantityRangeInPackagingCost(int quantityRangeId);

		#endregion QuantityRange

		#region Variant
		IEnumerable<Variant> GetVariant();

		Variant GetVariantById(int id);

		int InsertVariant(Variant variant);

		void UpdateVariant(Variant variant);

		void DeleteVariant(int variantId);

		IEnumerable<SelectModel> GetVariantList();

		bool isVariantExist(string description, string abbreviation);

		bool isVariantInProgramLine(int brandOwnerId);
		#endregion Variant
	}
}
