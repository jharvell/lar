﻿using dpc.Data;
using dpc.Data.CustomModels;
using System;
using System.Collections.Generic;

namespace dpc.Business.Interfaces
{
	public interface IDataManager
	{
		#region Claims

		bool isRoleInClaim(string roleName);

		#endregion Claims

		#region PasswordHistory

		void InsertPasswordHistory(PasswordHistory PasswordHistory);
		IEnumerable<string> GetPasswordHistory(string userId);
		DateTime GetLastSavedPasswordDate(string userId);

		#endregion PasswordHistory
	}
}
