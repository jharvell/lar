﻿using dpc.Data;
using dpc.Data.CustomModels;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using dpc.Common;

namespace dpc.Business.Interfaces
{
	public interface IProgramManager
	{
		#region Customer

		IEnumerable<Customer> GetCustomer();

		Customer GetCustomerById(int id);

		void InsertCustomer(Customer customer);

		void UpdateCustomer(Customer customer);

		void DeleteCustomer(int customerId);

		#endregion Program

		#region ProgramHeader

		IEnumerable<ProgramHeader> GetProgramHeader();

		ProgramHeader GetProgramHeaderById(int id);

		Task<ProgramHeader> GetProgramHeaderByIdAsync(int id);

		void InsertProgramHeader(ProgramHeader programHeader);

		void UpdateProgramHeader(ProgramHeader programHeader);

		void DeleteProgramHeader(int programHeaderId);

		IEnumerable<ProgramHeaderModel> GetProgramHeaderInfo();

		ProgramHeaderModel GetProgramHeaderInfoById(int programHeaderId);

		int GetProgramHeaderToProgramLineCount(int id);

		IEnumerable<SelectModel> GetBrandOwnerList();

		IEnumerable<SelectModel> GetTransitionTypeList();

		IEnumerable<SelectModel> GetYesNoList();

		IEnumerable<SelectModel> GetFreightTermsList();

		void UpdateProgramHeaderCalcs(int programHeaderId);

		void UpdateAllCalcs(int programHeaderId);

		EnumProgramStatus GetProgramStatus(int programHeaderId);

		void UpdateProgramHeaderStatus(int programHeaderId);

		#endregion ProgramHeader

		#region ProgramLine

		IEnumerable<ProgramLine> GetProgramLine();

		ProgramLine GetProgramLineById(int id);

		void InsertProgramLine(ProgramLine programLine);

		void UpdateProgramLine(ProgramLine programLine);

		void DeleteProgramLine(int programLineId);

		IEnumerable<ProgramLineModel> GetProgramLinesByProgramHeaderId(int id);

		IEnumerable<Feature> GetFeaturesByProductCategory(int productCategoryId);

		IEnumerable<SelectModel> GetFeatureOptionList(int featureId, int productTypeId, int productSizeId);

		ProductSizeToFeatureToOptionCostModel GetProductSizeToFeatureToOptionCost(int productSizeId, int featureToOptionId);

		void InsertProgramFeatureCost(int programLineId, int productSizeToFeatureToOptionCostId, double cost);

		void DeleteProgramFeatureCostByProgramLineId(int programLineId);

		ProgramLineModel GetGridProgramLineById(int id);

		DataTable GetProgramLinesDataByProgramHeaderId(int programHeaderId);

		void UpdateProgramLineCalcs(int programLineId);

		void UpdateProgramFeatureCostInProgramLines(int programHeaderId);

		#endregion ProgramHeader

		#region Approval

		IEnumerable<ProgramApproval> GetProgramApprovalsByProgramHeaderId(int id);

		ProgramApproval GetProgramApprovalById(int id);

		ProgramApproval GetProgramApprovalByUserId(int programHeaderId, string userId);

		IEnumerable<ProgramApproverModel> GetProgramApproverList(string rolename);

		IEnumerable<ProgramApproverModel> GetProgramApproverListByProgramHeaderId(string rolename, int programHeaderId);

		void UpdateSubmitForApproval(ProgramHeader programHeader);

		void InsertProgramApproval(ProgramApproval programApproval);

		void UpdateProgamApproval(ProgramApproval programApproval);

		bool isProgramSubmitted(int programHeaderId);

		bool isProgramApproved(int programHeaderId);

		bool isProgramRejected(int programHeaderId);

		#endregion Approval
	}
}
