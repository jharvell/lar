﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace dpc.Web
{
    public static class ExtendedClaimsProvider
    {
        public static IEnumerable<Claim> GetClaims(dpc.Web.Models.ApplicationUser user)
        {

            List<Claim> claims = new List<Claim>();

            return claims;
        }

        public static Claim CreateClaim(string type, string value)
        {
            return new Claim(type, value, ClaimValueTypes.String);
        }

    }
}