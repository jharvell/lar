﻿using AutoMapper;
using dpc.Data;
using dpc.Web.ViewModel;

namespace dpc.Web.Extensions
{
	public static class DataObjectExtensions
	{
		public static ProgramHeaderFinanacialsViewModel ToViewModel(this ProgramHeader input)
		{
			return Mapper.Map<ProgramHeaderFinanacialsViewModel>(input);
		}
	}
}