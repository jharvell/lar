﻿namespace System.Web.Mvc.Html
{
	public static class HtmlExtensions
	{
		public static MvcForm BeginForm(this HtmlHelper htmlHelper, string formId)
		{
			return htmlHelper.BeginForm(null, null, FormMethod.Post, new { id = formId, name = formId });
		}

		public static MvcForm BeginForm(this HtmlHelper htmlHelper, string formId, FormMethod method)
		{
			return htmlHelper.BeginForm(null, null, method, new { id = formId });
		}

		public static string ParamAction(this UrlHelper url, string action, string controller, string param)
		{
			Uri requestUrl = url.RequestContext.HttpContext.Request.Url;

			return url.Action(action, controller) + "/" + param;
		}
	}
}