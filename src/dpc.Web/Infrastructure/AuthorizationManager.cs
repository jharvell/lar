﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using dpc.Common;

namespace dpc.Web
{
	//Used to authorize at the Controller/Action level
	public class ClaimsAccessAttribute : AuthorizeAttribute
	{
		protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
		{
			base.HandleUnauthorizedRequest(filterContext);

			if (filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated)
			{
				var routeData = new RouteData();
				routeData.Values.Add("controller", "Error");
				routeData.Values.Add("action", "ErrorNotAuthorized");
				filterContext.Result = new RedirectToRouteResult(routeData.Values);
			}
		}

		public string ClaimType { get; set; }
		public string Value { get; set; }

		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			if (httpContext == null)
				throw new ArgumentNullException("httpContext");

			//Check if user has claim and value from input params
			var user = (System.Security.Claims.ClaimsIdentity)(httpContext.User.Identity);
			if (user.HasClaim(ClaimType, Value))
			{
				return true;
			}

			return false;
		}
	}

	public class AuthorizationManager : ClaimsAuthorizationManager
	{
		//Used to authorize content within a Controller/Action
		public override bool CheckAccess(System.Security.Claims.AuthorizationContext context)
		{
			var principal = context.Principal;
			var resource = context.Resource.First();
			var action = context.Action.First();
			

			if (!principal.Identity.IsAuthenticated)
			{
				return false;
			}

			#region -- Access Navigation
			if (action.Value == ActionKey.Access && resource.Value == ResourceKey.ViewAdmin)
			{
				return principal.HasClaim(ClaimType.Role, ClaimValue.Admin);
			}

			if (action.Value == ActionKey.Access && resource.Value == ResourceKey.ViewMaintenance)
			{
				return principal.HasClaim(ClaimType.Role, ClaimValue.Maintenance);
			}

			if (action.Value == ActionKey.Access && resource.Value == ResourceKey.ViewReporting)
			{
				var Report1 = principal.HasClaim(ClaimType.Role, ClaimValue.Reporting_LRRdetails);
				var Report2 = principal.HasClaim(ClaimType.Role, ClaimValue.Reporting_LRRfinancials);
				var Report3 = principal.HasClaim(ClaimType.Role, ClaimValue.Reporting_Calculations);

				if (Report1 || Report2 || Report3)
				{
					return true;
				}
			}

			if (action.Value == ActionKey.Access && resource.Value == ResourceKey.ReportLRRdetails)
			{
				return principal.HasClaim(ClaimType.Role, ClaimValue.Reporting_LRRdetails);
			}
			if (action.Value == ActionKey.Access && resource.Value == ResourceKey.ReportLRRfinancials)
			{
				return principal.HasClaim(ClaimType.Role, ClaimValue.Reporting_LRRfinancials);
			}
			if (action.Value == ActionKey.Access && resource.Value == ResourceKey.ReportCalculations)
			{
				return principal.HasClaim(ClaimType.Role, ClaimValue.Reporting_Calculations);
			}
			#endregion -- Access Navigation


			#region -- Show Resource
			//Reporting
			if (action.Value == ActionKey.Show && resource.Value == ResourceKey.ReportLRRdetails)
			{
				return principal.HasClaim(ClaimType.Role, ClaimValue.Reporting_LRRdetails);
			}
			if (action.Value == ActionKey.Show && resource.Value == ResourceKey.ReportLRRfinancials)
			{
				return principal.HasClaim(ClaimType.Role, ClaimValue.Reporting_LRRfinancials);
			}
			if (action.Value == ActionKey.Show && resource.Value == ResourceKey.ReportCalculations)
			{
				return principal.HasClaim(ClaimType.Role, ClaimValue.Reporting_Calculations);
			}
			#endregion -- Show Resource

			//Default Return value
			return false;
		}
	}
}
