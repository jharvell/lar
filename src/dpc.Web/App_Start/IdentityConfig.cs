﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using dpc.Web.Models;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace dpc.Web
{
	public class EmailService : IIdentityMessageService
	{
		public Task SendAsync(IdentityMessage message)
		{
			// Plug in your email service here to send an email.
			//return Task.FromResult(0);

			var userName = Convert.ToString(ConfigurationManager.AppSettings["SendEmailuserName"].ToString());
			var from = Convert.ToString(ConfigurationManager.AppSettings["SendEmailfrom"].ToString());  //Can be any valid email
			var password = Convert.ToString(ConfigurationManager.AppSettings["SendEmailpassword"].ToString());
			var port = Convert.ToInt32(ConfigurationManager.AppSettings["SendEmailport"].ToString());
			var smtp = Convert.ToString(ConfigurationManager.AppSettings["SendEmailsmtp"].ToString());

			var smtpClient = new SmtpClient(smtp, port);
			smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
			smtpClient.UseDefaultCredentials = false;
			smtpClient.EnableSsl = true;
			smtpClient.Credentials = new NetworkCredential(userName, password);

			var mailMessage = new MailMessage(from, message.Destination);
			mailMessage.Subject = message.Subject;
			mailMessage.Body = message.Body;
			mailMessage.IsBodyHtml = true;

			//Temporary disable Security... refer to http://stackoverflow.com/questions/777607/the-remote-certificate-is-invalid-according-to-the-validation-procedure-using
			ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors){ return true; };

			return smtpClient.SendMailAsync(mailMessage);
		}
	}

	public class SmsService : IIdentityMessageService
	{
		public Task SendAsync(IdentityMessage message)
		{
			// Plug in your SMS service here to send a text message.
			return Task.FromResult(0);
		}
	}

	// Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
	public class ApplicationUserManager : UserManager<ApplicationUser>
	{
		public ApplicationUserManager(IUserStore<ApplicationUser> store)
			: base(store)
		{
		}

		public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
		{
			var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
			// Configure validation logic for usernames
			manager.UserValidator = new UserValidator<ApplicationUser>(manager)
			{
				AllowOnlyAlphanumericUserNames = true,
				RequireUniqueEmail = true
			};

			// Configure validation logic for passwords
			manager.PasswordValidator = new PasswordValidator
			{
				RequiredLength = Convert.ToInt32(ConfigurationManager.AppSettings["PasswordRequiredLength"].ToString()),
				RequireNonLetterOrDigit = Convert.ToBoolean(ConfigurationManager.AppSettings["PasswordRequireNonLetterOrDigit"].ToString()),
				RequireDigit = Convert.ToBoolean(ConfigurationManager.AppSettings["PasswordRequireDigit"].ToString()),
				RequireLowercase = Convert.ToBoolean(ConfigurationManager.AppSettings["PasswordRequireLowercase"].ToString()),
				RequireUppercase = Convert.ToBoolean(ConfigurationManager.AppSettings["PasswordRequireUppercase"].ToString()),
			};

			// Configure user lockout defaults
			manager.UserLockoutEnabledByDefault = Convert.ToBoolean(ConfigurationManager.AppSettings["UserLockoutEnabledByDefault"].ToString());
			manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(Double.Parse(ConfigurationManager.AppSettings["DefaultAccountLockoutTimeSpan"].ToString()));
			manager.MaxFailedAccessAttemptsBeforeLockout = Convert.ToInt32(ConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"].ToString());

			// Configure communication services
			manager.EmailService = new EmailService();
			manager.SmsService = new SmsService();

			var dataProtectionProvider = options.DataProtectionProvider;
			if (dataProtectionProvider != null)
			{
				manager.UserTokenProvider =
					new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
			}
			return manager;
		}
	}

	// Configure the application sign-in manager which is used in this application.
	public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
	{
		public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
			: base(userManager, authenticationManager)
		{
		}

		public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
		{
			return user.GenerateUserIdentityAsync((ApplicationUserManager) UserManager);
		}

		public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
		{
			return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
		}
	}

	// Configure the RoleManager used in the application.  RoleManager is defined in the ASP.NET Identity core assembly.
	public class ApplicationRoleManager : RoleManager<ApplicationRole>
	{
		public ApplicationRoleManager(IRoleStore<ApplicationRole, string> roleStore)
			: base(roleStore)
		{

		}

		public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options,
			IOwinContext context)
		{
			return new ApplicationRoleManager(new RoleStore<ApplicationRole>(context.Get<ApplicationDbContext>()));
		}
	}
}
