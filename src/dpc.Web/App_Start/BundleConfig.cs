﻿using System.Web.Optimization;

namespace dpc.Web
{
	public class BundleConfig
	{
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/lib/jquery-{version}.js"
						));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/lib/jquery.validate*"
						));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/lib/modernizr-*"
						));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
						"~/Scripts/lib/bootstrap.js",
						"~/Scripts/lib/respond.js"
						));

			bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
						"~/Scripts/kendo/jszip.min.js",  // Used for Excel export (must be first)
						"~/Scripts/kendo/kendo.all.min.js",  //Also used for Reports instead of kendo.web.min.js & kendo.mobile.min.js
						"~/Scripts/kendo/kendo.aspnetmvc.min.js",
						"~/ReportViewer/js/telerikReportViewer-10.1.16.615.js"
						//"~/ReportViewer/js/kendo.subset.2015.3.930.min.js"  //Used for Reports but conflicts with jszip.min.js
						));

			bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
						"~/Scripts/custom/app.js",
						"~/Scripts/lib/jquery-validate.bootstrap-tooltip.min.js",
						"~/Scripts/lib/alertify.js"
						));


			bundles.Add(new StyleBundle("~/Content/kendo/css").Include(
						"~/Content/kendo/kendo.common.min.css",
						"~/Content/kendo/kendo.bootstrap.min.css")
						);

			bundles.Add(new StyleBundle("~/Content/css").Include(
						"~/Content/bootstrap.css",
						"~/Content/site.css",
						"~/Content/alertifyjs/alertify.css",
						"~/Content/alertifyjs/themes/bootstrap.css"
						));

			bundles.IgnoreList.Clear();
		}
	}
}
