﻿using AutoMapper;
using dpc.Common;
using dpc.Data;
using dpc.Data.CustomModels;
using dpc.Web.Models;
using dpc.Web.ViewModel;
using dpc.Web.ViewModels;

namespace dpc.Web.App_Start
{
	public static class AutoMapperConfig
	{
		public static void Start()
		{
			//Mapper.CreateMap<Source, Destination>()
			//Destination cannot have Unmapped members

			//Automapper 4.2 has marked the static API as obsolete
			//Mapper.CreateMap<ProductCategoryViewModel, ProductCategory>();
			//Mapper.CreateMap<ProductCategory, ProductCategoryViewModel>();
			//Mapper.CreateMap<ICollection<ProductCategoryViewModel>, ICollection<ProductCategory>>();
			//Mapper.CreateMap<ICollection<ProductCategory>, ICollection<ProductCategoryViewModel>>();

			Mapper.Initialize(cfg =>
			{
				//General
				#region Common

				cfg.CreateMap<SelectModel, SelectViewModel>().ReverseMap();

				#endregion Common

				#region User

				cfg.CreateMap<ApplicationUser, ApplicationUserViewModel>()
					.ForMember(d => d.NewPassword, s => s.Ignore())
					.ForMember(d => d.ConfirmPassword, s => s.Ignore())
					.ForMember(d => d.PasswordRequiredLength, s => s.Ignore());

				#endregion User

				//Maintenance

				#region Brand

				cfg.CreateMap<BrandModel, BrandViewModel>().ReverseMap();

				cfg.CreateMap<Brand, BrandViewModel>()
					.ForMember(d => d.BrandOwnerList, s => s.Ignore());

				cfg.CreateMap<BrandViewModel, Brand>()
					.ForMember(d => d.UpdatedOn, s => s.Ignore())
					.ForMember(d => d.UpdatedBy, s => s.Ignore())
					.ForMember(d => d.BrandOwner, s => s.Ignore())
					.ForMember(d => d.ProgramLine, s => s.Ignore());

				#endregion Brand

				#region BrandOwner

				cfg.CreateMap<BrandOwner, BrandOwnerViewModel>().ReverseMap();

				#endregion BrandOwner

				#region ContainerType

				cfg.CreateMap<ContainerType, ContainerTypeViewModel>().ReverseMap();

				#endregion ContainerType

				#region Feature

				cfg.CreateMap<FeatureModel, FeatureViewModel>().ReverseMap();

				cfg.CreateMap<Feature, FeatureViewModel>()
					.ForMember(d => d.ProductCategoryAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductCategoryDescription, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				cfg.CreateMap<FeatureViewModel, Feature>()
					.ForMember(d => d.ProductTypeToFeature, s => s.Ignore())
					.ForMember(d => d.ProductCategory, s => s.Ignore())
					.ForMember(d => d.ProductCategory, s => s.Ignore())
					.ForMember(d => d.UpdatedOn, s => s.Ignore())
					.ForMember(d => d.UpdatedBy, s => s.Ignore());

				#endregion Feature

				#region FeatureToOption

				cfg.CreateMap<FeatureOptionModel, FeatureOptionViewModel>().ReverseMap();

				cfg.CreateMap<FeatureToOption, FeatureToOptionViewModel>()
					.ForMember(d => d.FeatureId, s => s.Ignore())
					.ForMember(d => d.FeatureDescription, s => s.Ignore())
					.ForMember(d => d.FeatureAbbreviation, s => s.Ignore())
					.ForMember(d => d.FeatureList, s => s.Ignore())
					.ForMember(d => d.ProductTypeId, s => s.Ignore())
					.ForMember(d => d.ProductTypeDescription, s => s.Ignore())
					.ForMember(d => d.ProductTypeAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductTypeList, s => s.Ignore())
					.ForMember(d => d.ProductCategoryId, s => s.Ignore())
					.ForMember(d => d.ProductCategoryDescription, s => s.Ignore())
					.ForMember(d => d.ProductCategoryAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				cfg.CreateMap<FeatureToOptionViewModel, FeatureToOption>()
					.ForMember(d => d.ProductTypeToFeature, s => s.Ignore())
					.ForMember(d => d.ProductSizeToFeatureToOptionCost, s => s.Ignore());

				cfg.CreateMap<FeatureToOptionModel, FeatureToOptionViewModel>().ReverseMap();

				cfg.CreateMap<FeatureToOption, FeatureToOptionModel>()
					.ForMember(d => d.FeatureId, s => s.Ignore())
					.ForMember(d => d.FeatureDescription, s => s.Ignore())
					.ForMember(d => d.FeatureAbbreviation, s => s.Ignore())
					.ForMember(d => d.FeatureList, s => s.Ignore())
					.ForMember(d => d.ProductTypeId, s => s.Ignore())
					.ForMember(d => d.ProductTypeDescription, s => s.Ignore())
					.ForMember(d => d.ProductTypeAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductTypeList, s => s.Ignore())
					.ForMember(d => d.ProductCategoryId, s => s.Ignore())
					.ForMember(d => d.ProductCategoryDescription, s => s.Ignore())
					.ForMember(d => d.ProductCategoryAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				#endregion FeatureToOption

				#region MfgCost

				cfg.CreateMap<MfgCostModel, MfgCostViewModel>().ReverseMap();

				cfg.CreateMap<MfgCost, MfgCostModel>()
					.ForMember(d => d.ProductTypeAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductTypeDescription, s => s.Ignore())
					.ForMember(d => d.ProductTypeList, s => s.Ignore())
					.ForMember(d => d.ProductCategoryId, s => s.Ignore())
					.ForMember(d => d.ProductCategoryAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductCategoryDescription, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				cfg.CreateMap<MfgCostViewModel, MfgCost>()
					.ForMember(d => d.ProductType, s => s.Ignore());

				cfg.CreateMap<MfgCost, MfgCostViewModel>()
					.ForMember(d => d.ProductTypeList, s => s.Ignore())
					.ForMember(d => d.ProductCategoryId, s => s.Ignore())
					.ForMember(d => d.ProductCategoryAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductCategoryDescription, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				#endregion MfgCost

				#region PackagingCost

				cfg.CreateMap<PackagingCostModel, PackagingCostViewModel>().ReverseMap();

				cfg.CreateMap<PackagingCostViewModel, PackagingCost>()
					.ForMember(d => d.PackagingType, s => s.Ignore())
					.ForMember(d => d.ProductSize, s => s.Ignore())
					.ForMember(d => d.QuantityRange, s => s.Ignore());

				cfg.CreateMap<PackagingCost, PackagingCostViewModel>()
					.ForMember(d => d.QuantityRangeId, s => s.Ignore())
					.ForMember(d => d.QuantityRangeItem, s => s.Ignore())
					.ForMember(d => d.QuantityRangeList, s => s.Ignore())
					.ForMember(d => d.PackagingTypeId, s => s.Ignore())
					.ForMember(d => d.PackagingTypeDescription, s => s.Ignore())
					.ForMember(d => d.PackagingTypeAbbreviation, s => s.Ignore())
					.ForMember(d => d.PackagingTypeList, s => s.Ignore())
					.ForMember(d => d.ContainerTypeId, s => s.Ignore())
					.ForMember(d => d.ContainerTypeDescription, s => s.Ignore())
					.ForMember(d => d.ContainerTypeAbbreviation, s => s.Ignore())
					.ForMember(d => d.ContainerTypeList, s => s.Ignore())
					.ForMember(d => d.ProductSizeId, s => s.Ignore())
					.ForMember(d => d.ProductSizeDescription, s => s.Ignore())
					.ForMember(d => d.ProductSizeAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductSizeList, s => s.Ignore())
					.ForMember(d => d.ProductTypeId, s => s.Ignore())
					.ForMember(d => d.ProductTypeDescription, s => s.Ignore())
					.ForMember(d => d.ProductTypeAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductTypeList, s => s.Ignore())
					.ForMember(d => d.ProductCategoryId, s => s.Ignore())
					.ForMember(d => d.ProductCategoryDescription, s => s.Ignore())
					.ForMember(d => d.ProductCategoryAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				#endregion PackagingCost

				#region PackagingType

				cfg.CreateMap<PackagingTypeModel, PackagingTypeViewModel>().ReverseMap();

				cfg.CreateMap<PackagingTypeViewModel, PackagingType>()
					.ForMember(d => d.PackagingCost, s => s.Ignore())
					.ForMember(d => d.ContainerType, s => s.Ignore())
					.ForMember(d => d.ProgramLine, s => s.Ignore())
					.ForMember(d => d.ProgramLine1, s => s.Ignore());

				cfg.CreateMap<PackagingType, PackagingTypeViewModel>()
					.ForMember(d => d.ContainerTypeList, s => s.Ignore());

				#endregion PackagingType

				#region PackStyle

				cfg.CreateMap<PackStyleModel, PackStyleViewModel>().ReverseMap();

				cfg.CreateMap<PackStyle, PackStyleViewModel>()
					.ForMember(d => d.ProductTypeAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductTypeDescription, s => s.Ignore())
					.ForMember(d => d.ProductTypeList, s => s.Ignore())
					.ForMember(d => d.ProductCategoryId, s => s.Ignore())
					.ForMember(d => d.ProductCategoryAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductCategoryDescription, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				cfg.CreateMap<PackStyleViewModel, PackStyle>()
					.ForMember(d => d.ProductType, s => s.Ignore())
					.ForMember(d => d.ProgramLine, s => s.Ignore());

				#endregion PackStyle

				#region ProductCategory

				cfg.CreateMap<ProductCategory, ProductCategoryViewModel>().ReverseMap();

				#endregion ProductCategory

				#region ProductSize

				cfg.CreateMap<ProductSizeModel, ProductSizeViewModel>().ReverseMap();

				cfg.CreateMap<ProductSize, ProductSizeViewModel>()
					.ForMember(d => d.ProductTypeList, s => s.Ignore())
					.ForMember(d => d.ProductCategoryId, s => s.Ignore())
					.ForMember(d => d.ProductCategoryAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductCategoryDescription, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				cfg.CreateMap<ProductSizeViewModel, ProductSize>()
					.ForMember(d => d.ProductType, s => s.Ignore())
					.ForMember(d => d.PackagingCost, s => s.Ignore())
					.ForMember(d => d.ProductSizeToFeatureToOptionCost, s => s.Ignore())
					.ForMember(d => d.ProgramLine, s => s.Ignore());

				#endregion ProductSize

				#region ProductSizeToFeatureToOptionCost

				cfg.CreateMap<ProductSizeToFeatureToOptionCostModel, ProductSizeToFeatureToOptionCostViewModel>().ReverseMap();

				cfg.CreateMap<ProductSizeToFeatureToOptionCostViewModel, ProductSizeToFeatureToOptionCost>()
					.ForMember(d => d.ProductSizeToFeatureToOptionCostId, s => s.Ignore())
					.ForMember(d => d.FeatureToOption, s => s.Ignore())
					.ForMember(d => d.ProductSize, s => s.Ignore())
					.ForMember(d => d.ProgramFeatureCost, s => s.Ignore());

				cfg.CreateMap<ProductSizeToFeatureToOptionCost, ProductSizeToFeatureToOptionCostViewModel>()
					.ForMember(d => d.FeatureToOptionId, s => s.Ignore())
					.ForMember(d => d.ProductTypeToFeatureId, s => s.Ignore())
					.ForMember(d => d.FeatureToOptionList, s => s.Ignore())
					.ForMember(d => d.FeatureId, s => s.Ignore())
					.ForMember(d => d.FeatureAbbreviation, s => s.Ignore())
					.ForMember(d => d.FeatureDescription, s => s.Ignore())
					.ForMember(d => d.FeatureList, s => s.Ignore())
					.ForMember(d => d.ProductSizeId, s => s.Ignore())
					.ForMember(d => d.ProductSizeAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductSizeDescription, s => s.Ignore())
					.ForMember(d => d.ProductSizeList, s => s.Ignore())
					.ForMember(d => d.ProductTypeId, s => s.Ignore())
					.ForMember(d => d.ProductTypeAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductTypeDescription, s => s.Ignore())
					.ForMember(d => d.ProductTypeList, s => s.Ignore())
					.ForMember(d => d.ProductCategoryId, s => s.Ignore())
					.ForMember(d => d.ProductCategoryAbbreviation, s => s.Ignore())
					.ForMember(d => d.ProductCategoryDescription, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				#endregion ProductSizeToFeatureToOptionCost

				#region ProductType

				cfg.CreateMap<ProductTypeModel, ProductTypeViewModel>().ReverseMap();

				cfg.CreateMap<ProductType, ProductTypeViewModel>()
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				cfg.CreateMap<ProductTypeViewModel, ProductType>()
					.ForMember(d => d.ProductCategory, s => s.Ignore())
					.ForMember(d => d.ProductSize, s => s.Ignore())
					.ForMember(d => d.ProductTypeToFeature, s => s.Ignore())
					.ForMember(d => d.PackStyle, s => s.Ignore())
					.ForMember(d => d.ProgramLine, s => s.Ignore())
					.ForMember(d => d.MfgCost, s => s.Ignore());

				#endregion ProductType

				#region QuantityRange
				
				cfg.CreateMap<QuantityRangeModel, QuantityRangeViewModel>().ReverseMap();

				cfg.CreateMap<QuantityRangeViewModel, QuantityRange>()
					.ForMember(d => d.ContainerType, s => s.Ignore())
					.ForMember(d => d.PackagingCost, s => s.Ignore())
					.ForMember(d => d.ProductCategory, s => s.Ignore());

				cfg.CreateMap<QuantityRange, QuantityRangeViewModel>()
					.ForMember(d => d.ContainerTypeList, s => s.Ignore())
					.ForMember(d => d.ProductCategoryList, s => s.Ignore());

				#endregion QuantityRange

				#region Variant

				cfg.CreateMap<Variant, VariantViewModel>().ReverseMap();

				#endregion Variant

				//Program

				#region Approval

				cfg.CreateMap<ProgramApproverModel, ProgramApproverViewModel>().ReverseMap();

				cfg.CreateMap<ProgramApproval, ProgramApproverViewModel>()
					.ForMember(d => d.UserId, opt => opt.MapFrom(s => s.ApproverUserId))
					.ForMember(d => d.Email, s => s.Ignore())
					.ForMember(d => d.UserName, s => s.Ignore())
					.ForMember(d => d.FirstName, s => s.Ignore())
					.ForMember(d => d.LastName, s => s.Ignore())
					.ForMember(d => d.isReadOnly, s => s.Ignore());

				#endregion Approval

				#region ProgramHeader

				cfg.CreateMap<ProgramHeaderViewModel, ProgramHeaderModel>();

				cfg.CreateMap<ProgramHeaderModel, ProgramHeaderViewModel>()
					.ForMember(d => d.ProgramLineCount, s => s.Ignore())
					.ForMember(d => d.ShowLRRdetailReport, s => s.Ignore())
					.ForMember(d => d.ShowLRRfinancialReport, s => s.Ignore())
					.ForMember(d => d.ShowCalculationsReport, s => s.Ignore());

				cfg.CreateMap<ProgramHeader, ProgramHeaderViewModel>()
					.ForMember(d => d.Status, opt => opt.ResolveUsing((ProgramHeader s) =>
					{
						return (EnumProgramStatus) System.Enum.Parse(typeof(EnumProgramStatus), s.Status);
					}))
					.ForMember(d => d.BrandOwnerDescription, s => s.Ignore())
					.ForMember(d => d.BrandOwnerList, s => s.Ignore())
					.ForMember(d => d.TransitionTypeList, s => s.Ignore())
					.ForMember(d => d.PalletsList, s => s.Ignore())
					.ForMember(d => d.FreightTermsList, s => s.Ignore())
					.ForMember(d => d.NewProgramList, s => s.Ignore())
					.ForMember(d => d.isProgramLinesExist, s => s.Ignore())
					.ForMember(d => d.isProgramSubmitted, s => s.Ignore())
					.ForMember(d => d.isProgramApproved, s => s.Ignore())
					.ForMember(d => d.isProgramRejected, s => s.Ignore())
					.ForMember(d => d.ShowLRRdetailReport, s => s.Ignore())
					.ForMember(d => d.ShowLRRfinancialReport, s => s.Ignore())
					.ForMember(d => d.ShowCalculationsReport, s => s.Ignore());

				cfg.CreateMap<ProgramHeaderViewModel, ProgramHeader>()
					.ForMember(d => d.Status, opt => opt.ResolveUsing((ProgramHeaderViewModel s) =>
					{
						return s.Status.ToString();
					}))
					.ForMember(d => d.BrandOwner, s => s.Ignore())
					.ForMember(d => d.ProgramApproval, s => s.Ignore())
					.ForMember(d => d.ProgramLine, s => s.Ignore());

				cfg.CreateMap<ProgramHeader, ProgramHeaderFinanacialsViewModel>();

				#endregion ProgramHeader

				#region ProgramLine

				cfg.CreateMap<ProgramLineViewModel, ProgramLine>()
					.ForMember(d => d.UpdatedOn, s => s.Ignore())
					.ForMember(d => d.UpdatedBy, s => s.Ignore())
					.ForMember(d => d.Brand, s => s.Ignore())
					.ForMember(d => d.ContainerType, s => s.Ignore())
					.ForMember(d => d.PackagingType, s => s.Ignore())
					.ForMember(d => d.PackagingType1, s => s.Ignore())
					.ForMember(d => d.PackStyle, s => s.Ignore())
					.ForMember(d => d.ProductSize, s => s.Ignore())
					.ForMember(d => d.ProductType, s => s.Ignore())
					.ForMember(d => d.ProgramFeatureCost, s => s.Ignore())
					.ForMember(d => d.ProductType, s => s.Ignore())
					.ForMember(d => d.ProgramHeader, s => s.Ignore())
					.ForMember(d => d.Variant, s => s.Ignore());
				
				cfg.CreateMap<ProgramLineModel, ProgramLineViewModel>()
					.ForMember(d => d.FeatureToOptionIds, s => s.Ignore())
					.ForMember(d => d.VariantDescription, s => s.Ignore());

				cfg.CreateMap<ProgramLineViewModel, ProgramLineModel>();

				#endregion ProgramLine
			});

			Mapper.AssertConfigurationIsValid();
		}
	}
}