using System;
using Microsoft.Practices.Unity;
using dpc.Data.Interfaces;
using dpc.Data.Repositories;
using dpc.Business.Interfaces;
using dpc.Business;
using Microsoft.Practices.Unity.Mvc;
using System.Web.Mvc;
using dpc.Data;
using dpc.Web.Controllers;
using System.Data.Entity;
using Microsoft.Owin.Security;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using dpc.Web.Models;

namespace dpc.Web.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // container.RegisterType<IProductRepository, ProductRepository>();
        }

        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            #region Repositories

            //Identity
            container.RegisterType<IPasswordHistoryRepository, PasswordHistoryRepository>();

			//Product
			container.RegisterType<IPackStyleRepository, PackStyleRepository>();
			container.RegisterType<IProductCategoryRepository, ProductCategoryRepository>();
            container.RegisterType<IProductTypeRepository, ProductTypeRepository>();
            container.RegisterType<IProductSizeRepository, ProductSizeRepository>();
            container.RegisterType<IFeatureRepository, FeatureRepository>();
            container.RegisterType<IFeatureToOptionRepository, FeatureToOptionRepository>();
            container.RegisterType<IProductSizeToFeatureToOptionCostRepository, ProductSizeToFeatureToOptionCostRepository>();
			container.RegisterType<IVariantRepository, VariantRepository>();

			//Packaging
			container.RegisterType<IContainerTypeRepository, ContainerTypeRepository>();
            container.RegisterType<IQuantityRangeRepository, QuantityRangeRepository>();
            container.RegisterType<IPackagingTypeRepository, PackagingTypeRepository>();
            container.RegisterType<IPackagingCostRepository, PackagingCostRepository>();

            //Program
            container.RegisterType<ICustomerRepository, CustomerRepository>();
            container.RegisterType<IProgramHeaderRepository, ProgramHeaderRepository>();
            container.RegisterType<IProgramLineRepository, ProgramLineRepository>();
            container.RegisterType<IProgramFeatureCostRepository, ProgramFeatureCostRepository>();

            #endregion Repositories

            #region Managers
            container.RegisterType<IDataManager, DataManager>();
			container.RegisterType<IMaintenanceManager, MaintenanceManager>();
			container.RegisterType<IProgramManager, ProgramManager>();
            #endregion Managers

            #region AspNet Security
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType(typeof(IUserStore<ApplicationUser>), typeof(UserStore<ApplicationUser>));
            container.RegisterType(typeof(IRoleStore<ApplicationRole, string>), typeof(RoleStore<ApplicationRole>));
            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<IAuthenticationManager>(new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));
            #endregion AspNet Security

            container.RegisterType<IUnitOfWork, UnitOfWork>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}
