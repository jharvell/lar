﻿var webApiUri;

$(document).ready(function () {
	PopulateFinancialSummary();

	//Save & Re-calc
	$('#Save').click(function (e) {
		e.preventDefault();
		PostUpdateProgramHeader(e);
	});

	//Delete
	$('#Delete').click(function (e) {
		e.preventDefault();
		$("#Title").focus();

		alertify.confirm('<i class="fa fa-exclamation-triangle fa-lg"></i>&nbsp;&nbsp;<span>Attention</span>', 'Are you sure you want to delete <b>Program Header</b> and <b>Program Lines</b>?',
                        function () {
                            DeleteProgram(e);
                        },
                        function () {
                            alertify.error('Canceled');
                        })
                        .set({
                            labels: {
                                ok: "Delete",
                                cancel: "Cancel"
                            }
                        });
        return false;
    });

	//Submit Approval
	$('#SubmitForApproval').click(function (e) {
		e.preventDefault();

		alertify.confirm('<i class="fa fa-exclamation-triangle fa-lg"></i>&nbsp;&nbsp;<span>Attention</span>', 'Are you sure you want to Submit Program for Approval?',
					 function () {
						 $("#formSubmitForApproval").submit();
					 },
					 function () {
						 alertify.error('Canceled');
					 })
					.set({
						labels: {
							ok: "Yes",
							cancel: "No"
						}
					});
		return false;
	});

});  //end document.ready


/////////////////////////////////
// Update ProgramHeader and run Re-calc stored procedure
/////////////////////////////////
function PostUpdateProgramHeader(e) {
	var businessUnit = document.querySelector('input[name="BusinessUnit"]:checked').value;

	var data = {
		'ProgramHeaderId': $('#ProgramHeaderId').val(),
		'ProductCategoryId': 1,  //TODO: Get ProductCategoryId from Main menu selection (i.e. Baby or Adult)
		'BusinessUnit': businessUnit,
		'Title': $('#Title').val(),
		'BrandOwnerId': $('#BrandOwnerId').val(),
		'Status': $('#Status').val(),
		'RequestedShipDate': $('#RequestedShipDate').val(),
		'ContractEndDate': $('#ContractEndDate').val(),
		'Comments': $('#Comments').val(),
		'TransitionType': $('#TransitionType').val(),
		'Discounts': $('#Discounts').val(),
		'FeesPromotions': $('#FeesPromotions').val(),
		'BrokerFees': $('#BrokerFees').val(),
		'ReturnsDamage': $('#ReturnsDamage').val(),
		'Pallets': $('#Pallets').val(),
		'FreightTerms': $('#FreightTerms').val(),
		'FreightPerPad': $('#FreightPerPad').val(),
		'OceanFreight': $('#OceanFreight').val(),
		'NewProgram': $('#NewProgram').val(),
		'ProgramFee': $('#ProgramFee').val(),
		'ProgramFeeDescription': $('#ProgramFeeDescription').val()
	};

	$.postJSON({
		url: webApiUri + 'ProgramApi/PostUpdateProgramHeader',
		data: data,
		success: function () {
			PopulateFinancialSummary();
			alertify.success('Program Header updated');  //May not need this if using reload below
			window.location.reload();  //Added due to IE page not refreshing on Domtar server
		},
		error: function () {
			alertify.error('Error updating Program Header - notify IT');
		}
	});
}

/////////////////////////////////
// Delete ProgramHeader and ProgramLines
/////////////////////////////////
function DeleteProgram() {
	var data = {
		"ProgramHeaderId": $("#ProgramHeaderId").val()
	};

	$.postJSON({
		url: webApiUri + 'ProgramApi/PostDeleteProgram',
		data: data,
		success: function () {
			window.location.href = "/Program";
		},
		error: function () {
			alertify.error('Error deleting Program Header and Program Lines - notify IT');
		}
	});
}
