﻿//#region --- Ready ---
$(document).ready(function () {
	var code;
	$("#insert_Cost").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#insert_Cost").val() !== "") {
			$('#InsertBtn').click();
		}
	});
	$("#edit_Cost").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#edit_Cost").val() !== "") {
			$('#EditBtn').click();
		}
	});

	$('.modal').on('shown.bs.modal',
		function () {
			$(this).find('input:text:visible:first').focus();
			$("#ProductCategoryId").data("kendoDropDownList").focus();
		});

	$('.modal').on('hidden.bs.modal',
		function () {
			DestroyTooltips();
		});

	//Insert
	$('#CreateBtn').click(function (e) {
		e.preventDefault();
		ClearFields();
		$("#modalInsert").modal('toggle');
	});

	$('#InsertBtn').click(function (e) {
		e.preventDefault();
		var isValid = ValidateForm('#formInsert');
		if (isValid) {
			PostInsertItem();
		}
	});

	$("#CancelInsertBtn").click(function () {
		$("#modalInsert").modal('toggle');
	});

	//Edit
	$('#EditBtn').click(function (e) {
		e.preventDefault();

		var isValid = ValidateForm('#formEdit');
		if (isValid) {
			PostUpdateItem();
		}
	});

	$("#CancelEditBtn").click(function () {
		$("#modalEdit").modal('toggle');
	});

	//Delete
	$('#Delete').click(function (e) {
		e.preventDefault();
		$("#edit_Cost").focus();

		alertify.confirm('<i class="fa fa-exclamation-triangle fa-lg"></i>&nbsp;&nbsp;<span>Attention</span>',
				'Are you sure you want to delete <b>Option Cost</b>?',
				function () {
					DeleteItem(e);
				},
				function () {
					alertify.error('Canceled');
				})
			.set({
				labels: {
					ok: "Delete",
					cancel: "Cancel"
				}
			});
		return false;
	});

	//Insert Validations
	$.validateHelper({
		form: 'formInsert',
		rules: {
			'ProductCategoryId': {
				required: true
			},
			'ProductTypeId': {
				required: true
			},
			'ProductSizeId': {
				required: true
			},
			'FeatureId': {
				required: true
			},
			'FeatureToOptionId': {
				required: true
			},
			'insert_Cost': {
				required: true
			}
		},
		messages: {
			'ProductCategoryId': {
				required: 'Product Category required' + '--left'
			},
			'ProductTypeId': {
				required: 'Product Type required' + '--left'
			},
			'ProductSizeId': {
				required: 'Product Size required' + '--left'
			},
			'FeatureId': {
				required: 'Feature required' + '--left'
			},
			'FeatureToOptionId': {
				required: 'Feature Option required' + '--left'
			},
			'insert_Cost': {
				required: 'Cost required' + '--left'
			}
		}
	});

	//Edit Validations
	$.validateHelper({
		form: 'formEdit',
		rules: {
			'edit_Cost': {
				required: true
			}
		},
		messages: {
			'edit_Cost': {
				required: 'Cost required' + '--left'
			}
		}
	});

});  //end document.ready
//#endregion --- Ready ---

//#region --- Insert ---
function ClearFields() {
	$('#insert_Cost').val("");
}

function PostInsertItem() {
	var data = {
		'ProductSizeId': $('#ProductSizeId').val(),
		'FeatureToOptionId': $('#FeatureToOptionId').val(),
		'Cost': $('#insert_Cost').val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostInsertProductSizeToFeatureToOptionCost',
		data: data,
		success: function (json) {
			$("#modalInsert").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Create Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#insert_Cost").focus();
		}
	});
}
//#endregion --- Insert ---

//#region --- Edit ---
function EditItem(e) {
	e.preventDefault();
	PopulateItemForm(e);
}

function PopulateItemForm(e) {
	var grid = $("#grid").getKendoGrid();
	var item = grid.dataItem($(e.target).closest("tr"));
	var productSizeToFeatureToOptionCostId = item.ProductSizeToFeatureToOptionCostId;
	var productCategoryId = item.ProductCategoryId;
	var productCategoryDescription = item.ProductCategoryDescription;
	var productTypeId = item.ProductTypeId;
	var productTypeDescription = item.ProductTypeDescription;
	var productSizeId = item.ProductSizeId;
	var productSizeDescription = item.ProductSizeDescription;
	var featureId = item.FeatureId;
	var featureDescription = item.FeatureDescription;
	var featureToOptionId = item.FeatureToOptionId;
	var featureToOptionDescription = item.FeatureToOptionDescription;
	var cost = item.Cost;

	$("#modalEdit").modal('toggle');

	//Save id to use during Update
	$("#hidden_ProductSizeToFeatureToOptionCostId").val(productSizeToFeatureToOptionCostId);
	$("#hidden_ProductSizeId").val(productSizeId);
	$("#hidden_FeatureToOptionId").val(featureToOptionId);
	$("label[for='ProductCategoryDescription']").html(productCategoryDescription);
	$("label[for='ProductTypeDescription']").html(productTypeDescription);
	$("label[for='ProductSizeDescription']").html(productSizeDescription);
	$("label[for='FeatureDescription']").html(featureDescription);
	$("label[for='FeatureToOptionDescription']").html(featureToOptionDescription);
	$("#edit_Cost").val(cost);
}

function PostUpdateItem() {
	var productSizeToFeatureToOptionCostId = $("#hidden_ProductSizeToFeatureToOptionCostId").val();
	var productSizeId = $("#hidden_ProductSizeId").val();
	var featureToOptionId = $("#hidden_FeatureToOptionId").val();
	var cost = $("#edit_Cost").val();

	var data = {
		'ProductSizeToFeatureToOptionCostId': productSizeToFeatureToOptionCostId,
		'ProductSizeId': productSizeId,
		'FeatureToOptionId': featureToOptionId,
		'Cost': cost
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostUpdateProductSizeToFeatureToOptionCost',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Update Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#edit_Cost").focus();
		}
	});
}
//#endregion --- Edit ---

//#region --- Delete ---
function DeleteItem(e) {
	var data = {
		"ProductSizeToFeatureToOptionCostId": $("#hidden_ProductSizeToFeatureToOptionCostId").val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostDeleteProductSizeToFeatureToOptionCost',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Delete Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
		}
	});
}
//#endregion --- Delete ---

//#region --- Grid functions ---
function BindGridItemsEntered() {
	$('#grid').data('kendoGrid').dataSource.read();
	$('#grid').data('kendoGrid').refresh();
}

function OnGridItemsDataBound() {
	//Change button with text to font-awesome icon
	$(".k-grid-edit").addClass("x-icon fa fa-pencil fa-lg").removeClass("k-button k-button-icontext").text("");
}
//#endregion --- Grid functions ---