﻿//#region --- Ready ---
$(document).ready(function () {
	var code;
	$("#insert_Description").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#insert_Description").val() !== "") {
			$('#InsertBtn').click();
		}
	});
	$("#edit_Description").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#edit_Description").val() !== "") {
			$('#EditBtn').click();
		}
	});

	$('.modal').on('shown.bs.modal',
        function () {
			$(this).find('input:text:visible:first').focus();
			$("#ProductCategoryId").data("kendoDropDownList").focus();
        });

	$('.modal').on('hidden.bs.modal',
        function () {
			DestroyTooltips();
        });

	//Insert
	$('#CreateBtn').click(function (e) {
		e.preventDefault();
		ClearFields();
		$("#modalInsert").modal('toggle');
	});

	$('#InsertBtn').click(function (e) {
		e.preventDefault();
		var isValid = ValidateForm('#formInsert');
		if (isValid) {
			PostInsertItem();
		}
	});

	$("#CancelInsertBtn").click(function () {
		$("#modalInsert").modal('toggle');
	});

	//Edit
	$('#EditBtn').click(function (e) {
		e.preventDefault();

		var isValid = ValidateForm('#formEdit');
		if (isValid) {
			PostUpdateItem();
		}
	});

	$("#CancelEditBtn").click(function () {
		$("#modalEdit").modal('toggle');
	});

	//Delete
	$('#Delete').click(function (e) {
		e.preventDefault();
		$("#edit_Description").focus();

		alertify.confirm('<i class="fa fa-exclamation-triangle fa-lg"></i>&nbsp;&nbsp;<span>Attention</span>',
                'Are you sure you want to delete <b>Feature Option</b>?',
                function () {
					DeleteItem(e);
                },
                function () {
					alertify.error('Canceled');
                })
            .set({
				labels: {
					ok: "Delete",
					cancel: "Cancel"
				}
            });
		return false;
	});

	//Insert Validations
	$.validateHelper({
		form: 'formInsert',
		rules: {
			'ProductCategoryId': {
				required: true
			},
			'ProductTypeId': {
				required: true
			},
			'FeatureId': {
				required: true
			},
			'insert_Description': {
				required: true
			}
		},
		messages: {
			'ProductCategoryId': {
				required: 'Product Category required' + '--left'
			},
			'ProductTypeId': {
				required: 'Product Type required' + '--left'
			},
			'FeatureId': {
				required: 'Feature required' + '--left'
			},
			'insert_Description': {
				required: 'Description required' + '--left'
			}
		}
	});

	//Edit Validations
	$.validateHelper({
		form: 'formEdit',
		rules: {
			'edit_Description': {
				required: true
			}
		},
		messages: {
			'edit_Description': {
				required: 'Description required' + '--left'
			}
		}
	});

});  //end document.ready
//#endregion --- Ready ---

//#region --- Insert ---
function ClearFields() {
	$('#insert_Description').val("");
}

function PostInsertItem() {
	var data = {
		'ProductCategoryId': $('#ProductCategoryId').val(),
		'ProductTypeId': $('#ProductTypeId').val(),
		'FeatureId': $('#FeatureId').val(),
		'Description': $('#insert_Description').val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostInsertFeatureToOption',
		data: data,
		success: function (json) {
			$("#modalInsert").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Create Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#insert_Description").focus();
		}
	});
}
//#endregion --- Insert ---

//#region --- Edit ---
function EditItem(e) {
	e.preventDefault();
	PopulateItemForm(e);
}

function PopulateItemForm(e) {
	var grid = $("#grid").getKendoGrid();
	var item = grid.dataItem($(e.target).closest("tr"));
	var featureToOptionId = item.FeatureToOptionId;
	var featureId = item.FeatureId;
	var featureDescription = item.FeatureDescription;
	var productTypeId = item.ProductTypeId;
	var productTypeDescription = item.ProductTypeDescription;
	var productCategoryId = item.ProductCategoryId;
	var productCategoryDescription = item.ProductCategoryDescription;
	var description = item.Description;
	var productTypeToFeatureId = item.ProductTypeToFeatureId;
	
	//Save id to use during Update
	$("#hidden_ProductTypeToFeatureId").val(productTypeToFeatureId);
	$("#hidden_FeatureToOptionId").val(featureToOptionId);
	$("#hidden_FeatureId").val(featureId);
	$("label[for='FeatureDescription']").html(featureDescription);
	$("#hidden_ProductTypeId").val(productTypeId);
	$("label[for='ProductTypeDescription']").html(productTypeDescription);
	$('#hidden_ProductCategoryId').val(productCategoryId);
	$("label[for='ProductCategoryDescription']").html(productCategoryDescription);
	$("#edit_Description").val(description);

	$("#modalEdit").modal('toggle');
}

function PostUpdateItem() {
	var featureToOptionId = $("#hidden_FeatureToOptionId").val();
	var featureId = $("#hidden_FeatureId").val();
	var productTypeId = $("#hidden_ProductTypeId").val();
	var description = $("#edit_Description").val();

	var data = {
		'FeatureToOptionId': featureToOptionId,
		'FeatureId': featureId,
		'ProductTypeId': productTypeId,
		'Description': description
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostUpdateFeatureToOption',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Update Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#edit_Description").focus();
		}
	});
}
//#endregion --- Edit ---

//#region --- Delete ---
function DeleteItem(e) {
	var data = {
		'FeatureToOptionId': $("#hidden_FeatureToOptionId").val(),
		'ProductTypeToFeatureId': $("#hidden_ProductTypeToFeatureId").val(),
		'ProductTypeId': $("#hidden_ProductTypeId").val(),
		'FeatureId': $("#hidden_FeatureId").val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostDeleteFeatureToOption',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Delete Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
		}
	});
}
//#endregion --- Delete ---

//#region --- Grid functions ---
function BindGridItemsEntered() {
	$('#grid').data('kendoGrid').dataSource.read();
	$('#grid').data('kendoGrid').refresh();
}

function OnGridItemsDataBound() {
	//Change button with text to font-awesome icon
	$(".k-grid-edit").addClass("x-icon fa fa-pencil fa-lg").removeClass("k-button k-button-icontext").text("");
}
//#endregion --- Grid functions ---