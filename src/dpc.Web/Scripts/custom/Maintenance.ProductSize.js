﻿//#region --- Ready ---
$(document).ready(function () {
	var code;
	$("#insert_Abbreviation").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#insert_Abbreviation").val() !== "") {
			$('#InsertBtn').click();
		}
	});
	$("#edit_Abbreviation").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#edit_Abbreviation").val() !== "") {
			$('#EditBtn').click();
		}
	});

	$('.modal').on('shown.bs.modal',
        function () {
			$(this).find('input:text:visible:first').focus();
			$("#ProductCategoryId").data("kendoDropDownList").focus();
        });

	$('.modal').on('hidden.bs.modal',
        function () {
			DestroyTooltips();
        });

	//Insert
	$('#CreateBtn').click(function (e) {
		e.preventDefault();
		ClearFields();
		$("#modalInsert").modal('toggle');
	});

	$('#InsertBtn').click(function (e) {
		e.preventDefault();
		var isValid = ValidateForm('#formInsert');
		if (isValid) {
			PostInsertItem();
		}
	});

	$("#CancelInsertBtn").click(function () {
		$("#modalInsert").modal('toggle');
	});

	//Edit
	$('#EditBtn').click(function (e) {
		e.preventDefault();

		var isValid = ValidateForm('#formEdit');
		if (isValid) {
			PostUpdateItem();
		}
	});

	$("#CancelEditBtn").click(function () {
		$("#modalEdit").modal('toggle');
	});

	//Delete
	$('#Delete').click(function (e) {
		e.preventDefault();
		$("#edit_Description").focus();

		alertify.confirm('<i class="fa fa-exclamation-triangle fa-lg"></i>&nbsp;&nbsp;<span>Attention</span>',
                'Are you sure you want to delete <b>Product Size</b>?',
                function () {
					DeleteItem(e);
                },
                function () {
					alertify.error('Canceled');
                })
            .set({
				labels: {
					ok: "Delete",
					cancel: "Cancel"
				}
            });
		return false;
	});

	//Insert Validations
	$.validateHelper({
		form: 'formInsert',
		rules: {
			'ProductCategoryId': {
				required: true
			},
			'ProductTypeId': {
				required: true
			},
			'insert_Description': {
				required: true
			},
			'insert_Abbreviation': {
				required: true,
				maxlength: 5
			}
		},
		messages: {
			'ProductCategoryId': {
				required: 'Product Category required' + '--left'
			},
			'ProductTypeId': {
				required: 'Product Type required' + '--left'
			},
			'insert_Description': {
				required: 'Description required' + '--left'
			},
			'insert_Abbreviation': {
				required: 'Abbreviation required' + '--left',
				maxlength: 'Max length of 5' + '--right'
			}
		}
	});

	//Edit Validations
	$.validateHelper({
		form: 'formEdit',
		rules: {
			'edit_Description': {
				required: true
			},
			'edit_Abbreviation': {
				required: true,
				maxlength: 5
			}
		},
		messages: {
			'edit_Description': {
				required: 'Description required' + '--left'
			},
			'edit_Abbreviation': {
				required: 'Abbreviation required' + '--left',
				maxlength: 'Max length of 5' + '--right'
			}
		}
	});

});  //end document.ready
//#endregion --- Ready ---

//#region --- Insert ---
function ClearFields() {
	$('#insert_Description').val("");
	$('#insert_Abbreviation').val("");
}

function PostInsertItem() {
	var data = {
		'ProductTypeId': $('#ProductTypeId').val(),
		'Description': $('#insert_Description').val(),
		'Abbreviation': $('#insert_Abbreviation').val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostInsertProductSize',
		data: data,
		success: function (json) {
			$("#modalInsert").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Create Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#insert_Description").focus();
		}
	});
}
//#endregion --- Insert ---

//#region --- Edit ---
function EditItem(e) {
	e.preventDefault();
	PopulateItemForm(e);
}

function PopulateItemForm(e) {
	var grid = $("#grid").getKendoGrid();
	var item = grid.dataItem($(e.target).closest("tr"));
	var productSizeId = item.ProductSizeId;
	var productCategoryDescription = item.ProductCategoryDescription;
	var productTypeId = item.ProductTypeId;
	var productTypeDescription = item.ProductTypeDescription;
	var description = item.Description;
	var abbreviation = item.Abbreviation;
	
	//Save id to use during Update
	$("#hidden_ProductSizeId").val(productSizeId);
	$("#hidden_ProductTypeId").val(productTypeId);
	$("label[for='ProductCategoryDescription']").html(productCategoryDescription);
	$("label[for='ProductTypeDescription']").html(productTypeDescription);
	$("#edit_Description").val(description);
	$("#edit_Abbreviation").val(abbreviation);

	$("#modalEdit").modal('toggle');
}

function PostUpdateItem() {
	var productSizeId = $("#hidden_ProductSizeId").val();
	var productTypeId = $("#hidden_ProductTypeId").val();
	var description = $("#edit_Description").val();
	var abbreviation = $("#edit_Abbreviation").val();

	var data = {
		'ProductSizeId': productSizeId,
		'ProductTypeId': productTypeId,
		'Description': description,
		'Abbreviation': abbreviation
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostUpdateProductSize',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Update Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#edit_Description").focus();
		}
	});
}
//#endregion --- Edit ---

//#region --- Delete ---
function DeleteItem(e) {
	var data = {
		"ProductSizeId": $("#hidden_ProductSizeId").val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostDeleteProductSize',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Delete Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
		}
	});
}
//#endregion --- Delete ---

//#region --- Grid functions ---
function BindGridItemsEntered() {
	$('#grid').data('kendoGrid').dataSource.read();
	$('#grid').data('kendoGrid').refresh();
}

function OnGridItemsDataBound() {
	//Change button with text to font-awesome icon
	$(".k-grid-edit").addClass("x-icon fa fa-pencil fa-lg").removeClass("k-button k-button-icontext").text("");
}
//#endregion --- Grid functions ---