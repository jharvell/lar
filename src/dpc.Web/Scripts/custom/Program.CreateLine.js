﻿var webApiUri;

$(document).ready(function () {

	webApiUri = $('#WebApiUri').val();

	$("#ProductTypeId").data("kendoDropDownList").focus();

	$('#Create').show();
	$('#Update').hide();
	$('#Cancel').hide();

	//Insert ProgramLine
	$('#Create').click(function (e) {
		e.preventDefault();

		var isValid = ValidateForm('#formProgramLine');
		if (isValid) {
			PostInsertItem();
		}
	});

	//Update ProgramLine
	$('#Update').click(function (e) {
		e.preventDefault();

		var isValid = ValidateForm('#formProgramLine');
		if (isValid) {
			PostUpdateItem(e);
			$('#Create').show();
			$('#Update').hide();
			$('#Cancel').hide();
		}
	});

	//Cancel
	$('#Cancel').click(function (e) {
		e.preventDefault();
		$('#Create').show();
		$('#Update').hide();
		$('#Cancel').hide();

		alertify.error('Canceled');
	});
});  //end document.ready

/////////////////////////////////
// Insert ProgramLine
/////////////////////////////////
function PostInsertItem() {
	//Get feature values
	var featureToOptionArray = [];
	$("input[name*='FeatureToOptionId']").each(function () {
		featureToOptionArray.push($(this).val().toString());
	});

	var data = {
		'ProgramHeaderId': $('#ProgramHeader_ProgramHeaderId').val(),
		'ProductTypeId': $('#ProductTypeId').val(),
		'ProductSizeId': $('#ProductSizeId').val(),
		'PackStyleId': $('#PackStyleId').val(),
		'ContainerTypeId': $('#ContainerTypeId').val(),
		'BagTypeToPackagingTypeId': $('#BagTypeToPackagingTypeId').val(),
		'CaseTypeToPackagingTypeId': $('#CaseTypeToPackagingTypeId').val(),
		'BrandId': $('#BrandId').val(),
		'VariantId': $('#VariantId').val(),
		'BagCount': $('#ProgramLine_BagCount').val(),
		'BagsPerCase': $('#ProgramLine_BagsPerCase').val(),
		'AnnualCases': $('#ProgramLine_AnnualCases').val(),
		'CaseSellingPrice': $('#ProgramLine_CaseSellingPrice').val(),
		'FeatureToOptionIds': featureToOptionArray
	};

	$.postJSON({
		url: webApiUri + 'ProgramApi/PostInsertItem',
		data: data,
		success: function () {
			BindGridItemsEntered();
			alertify.success('Program Line created');
		},
		error: function (request) {
			if (request.responseJSON.ExceptionMessage == undefined) {
				alertify.error(request.responseJSON.Message);
			} else {
				alertify.error(request.responseJSON.ExceptionMessage);
			}
		}
	});
}

/////////////////////////////////
// Edit ProgramLine - combine with Update ProgramLine
/////////////////////////////////
function EditItem(e) {
	e.preventDefault();

	DestroyTooltips();

	$('#Create').hide();
	$('#Update').show();
	$('#Cancel').show();

	PopulateProductLineForm(e);
}

function PopulateProductLineForm(e) {
	//display drug info
	var grid = $("#grid").getKendoGrid();
	var item = grid.dataItem($(e.target).closest("tr"));
	var id = item.ProgramLineId;

	//Save id to use during Update
	$("#ProgramLineId_ToUpdate").val(id);

	//get data from Data layer
	$.getJSON(webApiUri + 'ProgramApi/GetGridProgramLineById/' + id)
        .done(function (data) {
        	$("#ProductTypeId").data('kendoDropDownList').value('' + data.ProductTypeId + '');
        	$("#ProductSizeId").data('kendoDropDownList').value('' + data.ProductSizeId + '');
        	$("#PackStyleId").data('kendoDropDownList').value('' + data.PackStyleId + '');
        	$("#ContainerTypeId").data('kendoDropDownList').value('' + data.ContainerTypeId + '');
        	$("#BagTypeToPackagingTypeId").data('kendoDropDownList').value('' + data.BagTypeToPackagingTypeId + '');
        	$("#CaseTypeToPackagingTypeId").data('kendoDropDownList').value('' + data.CaseTypeToPackagingTypeId + '');
        	$("#BrandId").data('kendoDropDownList').value('' + data.BrandId + '');
        	$("#VariantId").data('kendoDropDownList').value('' + data.VariantId + '');
        	$("#ProgramLine_BagCount").val(data.BagCount);
        	$("#ProgramLine_BagsPerCase").val(data.BagsPerCase);
        	$("#ProgramLine_AnnualCases").val(data.AnnualCases);
        	$("#ProgramLine_CaseSellingPrice").val(data.CaseSellingPrice);

        	$(data.FeatureOptions).each(function () {
        		$("#" + this.FeatureAbbreviation + "").data('kendoDropDownList').value('' + this.FeatureToOptionId + '');
        	});
        });
} //end function

/////////////////////////////////
// Update ProgramLine
/////////////////////////////////
function PostUpdateItem() {
	var id = $("#ProgramLineId_ToUpdate").val();

	var featureToOptionArray = [];
	$("input[name*='FeatureToOptionId']").each(function () {
		featureToOptionArray.push($(this).val().toString());
	});

	var data = {
		'ProgramLineId': id,
		'ProgramHeaderId': $('#ProgramHeader_ProgramHeaderId').val(),
		'ProductTypeId': $('#ProductTypeId').val(),
		'ProductSizeId': $('#ProductSizeId').val(),
		'PackStyleId': $('#PackStyleId').val(),
		'ContainerTypeId': $('#ContainerTypeId').val(),
		'BagTypeToPackagingTypeId': $('#BagTypeToPackagingTypeId').val(),
		'CaseTypeToPackagingTypeId': $('#CaseTypeToPackagingTypeId').val(),
		'BrandId': $('#BrandId').val(),
		'VariantId': $('#VariantId').val(),
		'BagCount': $('#ProgramLine_BagCount').val(),
		'BagsPerCase': $('#ProgramLine_BagsPerCase').val(),
		'AnnualCases': $('#ProgramLine_AnnualCases').val(),
		'CaseSellingPrice': $('#ProgramLine_CaseSellingPrice').val(),
		'FeatureToOptionIds': featureToOptionArray
	};

	$.postJSON({
		url: webApiUri + 'ProgramApi/PostUpdateItem',
		data: data,
		success: function () {
			BindGridItemsEntered();
			alertify.success('Program Line updated');
		},
		error: function (request) {
			if (request.responseJSON.ExceptionMessage == undefined) {
				alertify.error(request.responseJSON.Message);
			} else {
				alertify.error(request.responseJSON.ExceptionMessage);
			}
		}
	});
}

/////////////////////////////////
// Delete ProgramLine
/////////////////////////////////
function ConfirmDeleteItem(e) {
	e.preventDefault();

	DestroyTooltips();

	alertify.confirm('<i class="fa fa-exclamation-triangle fa-lg"></i>&nbsp;&nbsp;<span>Attention</span>', 'Are you sure you want to delete Program Line?',
                     function () {
                     	DeleteItem(e);
                     },
                     function () {
                     	alertify.error('Canceled');
                     })
                    .set({
                    	labels: {
                    		ok: "Delete",
                    		cancel: "Cancel"
                    	}
                    });
	return false;
}

function DeleteItem(e) {
	var grid = $('#grid').data('kendoGrid');
	var item = grid.dataItem($(e.target).closest('tr'));
	var programLineId = item.ProgramLineId;
	var programHeaderId = item.ProgramHeaderId;

	var data = {
		"ProgramLineId": programLineId,
		"ProgramHeaderId": programHeaderId
	};

	$.postJSON({
		url: webApiUri + 'ProgramApi/PostDeleteItem',
		data: data,
		success: function () {
			BindGridItemsEntered();
			alertify.success('Program Line deleted');
		},
		error: function (request) {
			if (request.responseJSON.ExceptionMessage == undefined) {
				alertify.error(request.responseJSON.Message);
			} else {
				alertify.error(request.responseJSON.ExceptionMessage);
			}
		}
	});
}

/////////////////////////////////
// Grid functions
/////////////////////////////////
function BindGridItemsEntered() {
	$('#grid').data('kendoGrid').dataSource.read();
	$('#grid').data('kendoGrid').refresh();
}

function OnGridItemsEnteredDataBound() {
	//Change button with text to font-awesome icon
	$(".k-grid-Edit").addClass("x-icon fa fa-pencil fa-lg").removeClass("k-button k-button-icontext").text("");
	$(".k-grid-Delete").addClass("x-icon fa fa-trash fa-lg").removeClass("k-button k-button-icontext").text("");

	//Autosize columns
	var grid = $("#grid").data("kendoGrid");
	for (var i = 0; i < grid.columns.length; i++) {
		grid.autoFitColumn(i);
	}
}

function excelExport(e) {
	var sheet = e.workbook.sheets[0];
	var headers = sheet.rows[0];

	for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {
		var row = sheet.rows[rowIndex];
		var header = "";
		for (var ci = 0; ci < row.cells.length; ci++) {
			header = headers.cells[ci].value;

			switch (header) {
				case "Bag Count":
				case "Bags Per Case":
				case "Annual Cases":
				case "Pieces":
					row.cells[ci].format = '#,##0';
					break;

				case "Case Selling Price":
					row.cells[ci].format = '$#,##0.00';
					break;

				case "Piece Price":
					row.cells[ci].format = '$0#.##0';
					break;

				case "Contribution Margin":
				case "Variable Margin":
				case "Net Sales":
					row.cells[ci].format = '$#,##0';
					break;

				case "Contribution Margin Percent":
				case "Variable Margin Percent":
					row.cells[ci].format = '#%';
					break;

				default:
			}
		}
	}
}