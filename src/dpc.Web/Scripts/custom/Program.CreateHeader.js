﻿//#region --- Ready ---
$(document).ready(function () {
	//Insert Validations
	$.validateHelper({
		form: 'formInsert',
		rules: {
			'Title': {
				required: true
			},
			'BrandOwnerId': {
				required: true
			}
		},
		messages: {
			'Title': {
				required: 'Title required' + '--left'
			},
			'BrandOwnerId': {
				required: 'Brand Owner required' + '--left'
			}
		}
	});

});  //end document.ready
//#endregion --- Ready ---
