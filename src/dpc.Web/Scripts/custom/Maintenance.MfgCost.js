//#region --- Ready ---
$(document).ready(function () {
	var code;
	$("#insert_Scrap").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#insert_Scrap").val() !== "") {
			$('#InsertBtn').click();
		}
	});
	$("#edit_Scrap").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#edit_Scrap").val() !== "") {
			$('#EditBtn').click();
		}
	});

	$('.modal').on('shown.bs.modal',
		function () {
			$(this).find('input:text:visible:first').focus();
			$("#ProductCategoryId").data("kendoDropDownList").focus();
		});

	$('.modal').on('hidden.bs.modal',
		function () {
			DestroyTooltips();
		});

	//Insert
	$('#CreateBtn').click(function (e) {
		e.preventDefault();
		ClearFields();
		$("#modalInsert").modal('toggle');
	});

	$('#InsertBtn').click(function (e) {
		e.preventDefault();
		var isValid = ValidateForm('#formInsert');
		if (isValid) {
			PostInsertItem();
		}
	});

	$("#CancelInsertBtn").click(function () {
		$("#modalInsert").modal('toggle');
	});

	//Edit
	$('#EditBtn').click(function (e) {
		e.preventDefault();

		var isValid = ValidateForm('#formEdit');
		if (isValid) {
			PostUpdateItem();
		}
	});

	$("#CancelEditBtn").click(function () {
		$("#modalEdit").modal('toggle');
	});

	//Delete
	$('#Delete').click(function (e) {
		e.preventDefault();
		$("#edit_VLandOverhead").focus();

		alertify.confirm('<i class="fa fa-exclamation-triangle fa-lg"></i>&nbsp;&nbsp;<span>Attention</span>',
				'Are you sure you want to delete <b>Mfg Cost</b>?',
				function () {
					DeleteItem(e);
				},
				function () {
					alertify.error('Canceled');
				})
			.set({
				labels: {
					ok: "Delete",
					cancel: "Cancel"
				}
			});
		return false;
	});

	//Insert Validations
	$.validateHelper({
		form: 'formInsert',
		rules: {
			'ProductCategoryId': {
				required: true
			},
			'ProductTypeId': {
				required: true
			},
			'insert_VLandOverhead': {
				required: true
			},
			'insert_FixedConversionCost': {
				required: true
			},
			'insert_Depreciation': {
				required: true
			},
			'insert_Scrap': {
				required: true
			}
		},
		messages: {
			'ProductCategoryId': {
				required: 'Product Category required' + '--left'
			},
			'ProductTypeId': {
				required: 'Product Type required' + '--left'
			},
			'insert_VLandOverhead': {
				required: 'VL and Overhead required' + '--left'
			},
			'insert_FixedConversionCost': {
				required: 'Fixed Conversion Cost required' + '--left'
			},
			'insert_Depreciation': {
				required: 'Depreciation required' + '--left'
			},
			'insert_Scrap': {
				required: 'Scrap required' + '--left'
			}
		}
	});

	//Edit Validations
	$.validateHelper({
		form: 'formEdit',
		rules: {
			'edit_VLandOverhead': {
				required: true
			},
			'edit_FixedConversionCost': {
				required: true
			},
			'edit_Depreciation': {
				required: true
			},
			'edit_Scrap': {
				required: true
			}
		},
		messages: {
			'edit_VLandOverhead': {
				required: 'VL and Overhead required' + '--left'
			},
			'edit_FixedConversionCost': {
				required: 'Fixed Conversion Cost required' + '--left'
			},
			'edit_Depreciation': {
				required: 'Depreciation required' + '--left'
			},
			'edit_Scrap': {
				required: 'Scrap required' + '--left'
			}
		}
	});

});  //end document.ready
//#endregion --- Ready ---

//#region --- Insert ---
function ClearFields() {
	$('#insert_VLandOverhead').val("");
	$('#insert_FixedConversionCost').val("");
	$('#insert_Depreciation').val("");
	$('#insert_Scrap').val("");
}

function PostInsertItem() {
	var data = {
		'ProductTypeId': $('#ProductTypeId').val(),
		'VLandOverhead': $('#insert_VLandOverhead').val(),
		'FixedConversionCost': $('#insert_FixedConversionCost').val(),
		'Depreciation': $('#insert_Depreciation').val(),
		'Scrap': $('#insert_Scrap').val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostInsertMfgCost',
		data: data,
		success: function (json) {
			$("#modalInsert").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Create Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#insert_VLandOverhead").focus();
		}
	});
}
//#endregion --- Insert ---

//#region --- Edit ---
function EditItem(e) {
	e.preventDefault();
	PopulateItemForm(e);
}

function PopulateItemForm(e) {
	var grid = $("#grid").getKendoGrid();
	var item = grid.dataItem($(e.target).closest("tr"));
	var mfgCostId = item.MfgCostId;
	var productCategoryId = item.productCategoryId;
	var productCategoryDescription = item.ProductCategoryDescription;
	var productTypeId = item.ProductTypeId;
	var productTypeDescription = item.ProductTypeDescription;
	var vLandOverhead = item.VLandOverhead;
	var fixedConversionCost = item.FixedConversionCost;
	var depreciation = item.Depreciation;
	var scrap = item.Scrap;

	$("#modalEdit").modal('toggle');

	//Save id to use during Update
	$("#hidden_MfgCostId").val(mfgCostId);
	$("#hidden_ProductTypeId").val(productTypeId);
	$("label[for='ProductCategoryDescription']").html(productCategoryDescription);
	$("label[for='ProductTypeDescription']").html(productTypeDescription);
	$("#edit_VLandOverhead").val(vLandOverhead);
	$("#edit_FixedConversionCost").val(fixedConversionCost);
	$("#edit_Depreciation").val(depreciation);
	$("#edit_Scrap").val(scrap);
}

function PostUpdateItem() {
	var mfgCostId = $("#hidden_MfgCostId").val();
	var productTypeId = $("#hidden_ProductTypeId").val();
	var vLandOverhead = $("#edit_VLandOverhead").val();
	var fixedConversionCost = $("#edit_FixedConversionCost").val();
	var depreciation = $("#edit_Depreciation").val();
	var scrap = $("#edit_Scrap").val();

	var data = {
		'MfgCostId': mfgCostId,
		'ProductTypeId': productTypeId,
		'VLandOverhead': vLandOverhead,
		'FixedConversionCost': fixedConversionCost,
		'Depreciation': depreciation,
		'Scrap': scrap
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostUpdateMfgCost',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Update Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#edit_VLandOverhead").focus();
		}
	});
}
//#endregion --- Edit ---

//#region --- Delete ---
function DeleteItem(e) {
	var data = {
		"MfgCostId": $("#hidden_MfgCostId").val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostDeleteMfgCost',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Delete Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
		}
	});
}
//#endregion --- Delete ---

//#region --- Grid functions ---
function BindGridItemsEntered() {
	$('#grid').data('kendoGrid').dataSource.read();
	$('#grid').data('kendoGrid').refresh();
}

function OnGridItemsDataBound() {
	//Change button with text to font-awesome icon
	$(".k-grid-edit").addClass("x-icon fa fa-pencil fa-lg").removeClass("k-button k-button-icontext").text("");
}
//#endregion --- Grid functions ---