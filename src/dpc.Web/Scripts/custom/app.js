﻿//Common functions included on ALL pages
var webApiUri;

$(document).ready(function () {
	webApiUri = $('#WebApiUri').val();

	alertify.defaults.transition = "slide";
	alertify.defaults.theme.ok = "btn btn-primary";
	alertify.defaults.theme.cancel = "btn btn-danger";
	alertify.defaults.theme.input = "form-control";

	//var inputTypes = "input, select, button, textarea, text";
	//$(document).on("keydown", inputTypes, function (e) {
	//	var form;
	//	var inputArray;
	//	var moveDirection;
	//	var moveTo;
	//	var newIndex;
	//	var self;
	//	var tabIndex;
	//	var enterKey = 13;
	//	var tabKey = 9;

	//	if (e.keyCode === tabKey || e.keyCode === enterKey) {
	//		self = $(this);

	//		// some controls should react as designed when pressing enter
	//		if (e.keyCode === enterKey && (self.prop("type") === "submit" || self.prop("type") === "textarea")) {
	//			return true;
	//		}

	//		form = self.parents("form:eq(0)");

	//		// Sort by tab indexes if they exist
	//		tabIndex = parseInt(self.attr("tabindex"));

	//		if (tabIndex) {
	//			inputArray = form.find("[tabindex]").sort(function (a, b) {
	//				return parseInt($(a).attr("tabindex")) - parseInt($(b).attr("tabindex"));
	//			});
	//		} else {
	//			inputArray = form.find(inputTypes);
	//		}

	//		// reverse the direction if using shift
	//		moveDirection = e.shiftKey ? -1 : 1;
	//		newIndex = inputArray.index(this) + moveDirection;

	//		// wrap around the controls
	//		if (newIndex === inputArray.length) {
	//			newIndex = 0;
	//		} else if (newIndex === -1) {
	//			newIndex = inputArray.length - 1;
	//		}

	//		moveTo = inputArray.eq(newIndex);

	//		if (moveTo.data("role") === "dropdownlist") {
	//			moveTo.data("kendoDropDownList").focus();
	//		} else {
	//			moveTo.focus();
	//			moveTo.select();
	//		}
	//		return false;
	//	}
	//});

	//Select contents when kendo TextBox focuses
	$("input[class=k-textbox]").focus(function () {
		this.select();
	});

	//Trim TextBox contents
	$("input[class=k-textbox]").change(function () {
		$(this).val($.trim($(this).val()));
	});

	//Toggle password
	$(".toggle-password").click(function () {
		$(this).toggleClass("fa-eye fa-eye-slash");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") === "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});

});  //end document.ready

// POST ajax Helper: called from pages using client side scripting
jQuery.extend({
	postJSON: function (params) {
		return jQuery.ajax(jQuery.extend(params, {
			type: "POST",
			data: JSON.stringify(params.data),
			dataType: "json",
			contentType: "application/json",
			processData: false
		}));
	}
});  //end document.ready

//Add to allow validation of Hidden elements
$.validator.setDefaults({
	ignore: []
});

// Validate Helper: tooltip right-align by default, add "--left" to display on left side of element
jQuery.extend({
	validateHelper: function (params) {
		if (params.onfocusout == "undefined" || params.onfocusout == null) {
			params.onfocusout = false;
		}
		if (params.onkeyup == "undefined" || params.onkeyup == null) {
			params.onkeyup = true;
		}
		$("#" + params.form).validate({
			submitHandler: function (form) {
				form.submit();
			},
			onfocusout: function (element, event) {
				if (params.onfocusout === false) {
					return false;
				} else {
					this.element(element);  // <- "eager validation"
				}
			},
			rules: params.rules,
			messages: params.messages,
			onkeyup: function (element, event) {
				if (params.onkeyup === false) {
					return false;
				} else {
					this.element(element);  // <- "eager validation"
				}
			},
			invalidHandler: function (form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					validator.errorList[0].element.focus();
				}
			},
			showErrors: function (errorMap, errorList) {
				$.each(this.successList, function (index, value) {
					//Remove error tooltips
					if ($('#' + value.id + '').parent().hasClass("k-dropdown")) {
						$('#' + value.id + '').parent().tooltip("destroy");
					} else {
						$('#' + value.id + '').tooltip("destroy");
					}
				});

				$.each(errorList, function (index, value) {
					var message = value.message;
					var place = 'left';

					var i = value.message.indexOf('--');
					if (i > 0) {
						place = value.message.substr(i + 2);
						message = value.message.substring(0, i);
					}

					//Check parent span for class k-dropdown(Kendo creates a span with the class k-dropdown)
					if ($('#' + value.element.id + '').parent().hasClass("k-dropdown")) {
						$('#' + value.element.id + '')
							.parent()
							.attr("data-original-title", message)
							.tooltip({
								placement: place,
								trigger: "manual",
								delay: {
									show: 500,
									hide: 5000
								},
								container: "body",
								html: true
							});

						$('#' + value.element.id + '').parent().tooltip("show");
					} else {
						$('#' + value.element.id + '')
							.attr("data-original-title", message)
							.tooltip({
								placement: place,
								trigger: "manual",
								delay: {
									show: 500,
									hide: 5000
								},
								container: "body",
								html: true
							});

						$('#' + value.element.id + '').tooltip("show");
					}
				});
			}  //end showErrors
		}); // end validate
	} //end validate helper
});  //jQuery.extend

/////////////////////////////////
// Tooltip
/////////////////////////////////
$(function () {
	$('[data-toggle="tooltip"]').tooltip({
		container: 'body', html: true
	});
});

//Clear all tooltip errors
function DestroyTooltips() {
	$('.tooltip').tooltip('destroy');
}

/////////////////////////////////
// Validation
/////////////////////////////////
function ValidateForm(formToValidate) {
	var form = $(formToValidate);

	form.validate();

	return form.valid();
}