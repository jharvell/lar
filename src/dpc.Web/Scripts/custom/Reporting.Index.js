﻿$(document).ready(function () {
	

});  //end document.ready


//#region --- Grid functions ---
function OnGridItemsDataBound() {
	//Hide or show column based on security
	var grid = $("#grid").getKendoGrid();

	var showLRRdetailReport = $('#ShowLRRdetailReport').val();
	var showLRRfinancialReport = $('#ShowLRRfinancialReport').val();
	var showCalculationsReport = $('#ShowCalculationsReport').val();

	if (showLRRdetailReport === 'False') {
		$('#LRRdetail').hide();
	}
	if (showLRRfinancialReport === 'False') {
		$('#LRRfinancial').hide();
	}
	if (showCalculationsReport === 'False') {
		$('#Calculations').hide();
	}

	//Add Tooltips to icons
	$('[data-toggle="tooltip"]').tooltip();
}
//#endregion --- Grid functions ---