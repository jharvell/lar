﻿//#region --- Ready ---
$(document).ready(function () {
	var code;
	$("#insert_Maximum").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#insert_Maximum").val() !== "") {
			$('#InsertBtn').click();
		}
	});
	$("#edit_Maximum").keyup(function (e) {
		code = e.keyCode ? e.keyCode : e.which;
		if (code === 13 && $("#edit_Maximum").val() !== "") {
			$('#EditBtn').click();
		}
	});

	$('.modal').on('shown.bs.modal',
        function () {
			$(this).find('input:text:visible:first').focus();
			$("#ProductCategoryId").data("kendoDropDownList").focus();
        });

	$('.modal').on('hidden.bs.modal',
        function () {
			DestroyTooltips();
        });

	//Insert
	$('#CreateBtn').click(function (e) {
		e.preventDefault();
		ClearFields();
		$("#modalInsert").modal('toggle');
	});

	$('#InsertBtn').click(function (e) {
		e.preventDefault();
		var isValid = ValidateForm('#formInsert');
		if (isValid) {
			PostInsertItem();
		}
	});

	$("#CancelInsertBtn").click(function () {
		$("#modalInsert").modal('toggle');
	});

	//Edit
	$('#EditBtn').click(function (e) {
		e.preventDefault();

		var isValid = ValidateForm('#formEdit');
		if (isValid) {
			PostUpdateItem();
		}
	});

	$("#CancelEditBtn").click(function () {
		$("#modalEdit").modal('toggle');
	});

	//Delete
	$('#Delete').click(function (e) {
		e.preventDefault();
		$("#edit_Description").focus();

		alertify.confirm('<i class="fa fa-exclamation-triangle fa-lg"></i>&nbsp;&nbsp;<span>Attention</span>',
                'Are you sure you want to delete <b>Quantity Range</b>?',
                function () {
					DeleteItem(e);
                },
                function () {
					alertify.error('Canceled');
                })
            .set({
					labels: {
					ok: "Delete",
					cancel: "Cancel"
				}
            });
		return false;
	});

	//Insert Validations
	$.validateHelper({
		form: 'formInsert',
		rules: {
			'ProductCategoryId': {
				required: true
			},
			'ContainerTypeId': {
				required: true
			},
			'insert_Minimum': {
				required: true
			},
			'insert_Maximum': {
				required: true
			}
		},
		messages: {
			'ProductCategoryId': {
				required: 'Product Category required' + '--left'
			},
			'ContainerTypeId': {
				required: 'Container Type required' + '--left'
			},
			'insert_Minimum': {
				required: 'Minimum required' + '--left'
			},
			'insert_Maximum': {
				required: 'Maximum required' + '--left'
			}
		}
	});

	//Edit Validations
	$.validateHelper({
		form: 'formEdit',
		rules: {
			'edit_Minimum': {
				required: true
			},
			'edit_Maximum': {
				required: true
			}
		},
		messages: {
			'edit_Minimum': {
				required: 'Minimum required' + '--left'
			},
			'edit_Abbreviation': {
				required: 'Maximum required' + '--left'
			}
		}
	});

});  //end document.ready
//#endregion --- Ready ---

//#region --- Insert ---
function ClearFields() {
	$('#insert_Minimum').val("");
	$('#insert_Maximum').val("");
}

function PostInsertItem() {
	var data = {
		'ProductCategoryId': $('#ProductCategoryId').val(),
		'ContainerTypeId': $('#ContainerTypeId').val(),
		'Minimum': $('#insert_Minimum').val(),
		'Maximum': $('#insert_Maximum').val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostInsertQuantityRange',
		data: data,
		success: function (json) {
			$("#modalInsert").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Create Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#insert_Minimum").focus();
		}
	});
}
//#endregion --- Insert ---

//#region --- Edit ---
function EditItem(e) {
	e.preventDefault();
	PopulateItemForm(e);
}

function PopulateItemForm(e) {
	var grid = $("#grid").getKendoGrid();
	var item = grid.dataItem($(e.target).closest("tr"));
	var quantityRangeId = item.QuantityRangeId;
	var productCategoryId = item.ProductCategoryId;
	var productCategoryDescription = item.ProductCategoryDescription;
	var containerTypeId = item.ContainerTypeId;
	var containerTypeDescription = item.ContainerTypeDescription;
	var minimum = item.Minimum;
	var maximum = item.Maximum;

	//Save id to use during Update
	$("#hidden_QuantityRangeId").val(quantityRangeId);
	$("#hidden_ProductCategoryId").val(productCategoryId);
	$("#hidden_ContainerTypeId").val(containerTypeId);
	$("label[for='ProductCategoryDescription']").html(productCategoryDescription);
	$("label[for='ContainerTypeDescription']").html(containerTypeDescription);
	$("#edit_Minimum").val(minimum);
	$("#edit_Maximum").val(maximum);

	$("#modalEdit").modal('toggle');
}

function PostUpdateItem() {
	var quantityRangeId = $("#hidden_QuantityRangeId").val();
	var productCategoryId = $("#hidden_ProductCategoryId").val();
	var containerTypeId = $("#hidden_ContainerTypeId").val();
	var maximum = $("#edit_Maximum").val();
	var minimum = $("#edit_Minimum").val();

	var data = {
		'QuantityRangeId': quantityRangeId,
		'ProductCategoryId': productCategoryId,
		'ContainerTypeId': containerTypeId,
		'Minimum': minimum,
		'Maximum': maximum
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostUpdateQuantityRange',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Update Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
			$("#edit_Description").focus();
		}
	});
}
//#endregion --- Edit ---

//#region --- Delete ---
function DeleteItem(e) {
	var data = {
		"QuantityRangeId": $("#hidden_QuantityRangeId").val()
	};

	$.postJSON({
		url: webApiUri + 'MaintenanceApi/PostDeleteQuantityRange',
		data: data,
		success: function (json) {
			$("#modalEdit").modal('toggle');
			BindGridItemsEntered();
			alertify.success('Delete Successful');
		},
		error: function (request, status, error) {
			alertify.error(request.responseJSON.Message);
		}
	});
}
//#endregion --- Delete ---

//#region --- Grid functions ---
function BindGridItemsEntered() {
	$('#grid').data('kendoGrid').dataSource.read();
	$('#grid').data('kendoGrid').refresh();
}

function OnGridItemsDataBound() {
	//Change button with text to font-awesome icon
	$(".k-grid-edit").addClass("x-icon fa fa-pencil fa-lg").removeClass("k-button k-button-icontext").text("");
}
//#endregion --- Grid functions ---