﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(dpc.Web.Startup))]
namespace dpc.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
