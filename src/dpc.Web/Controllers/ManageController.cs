﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using dpc.Web.Models;
using System.Configuration;
using dpc.Data;
using dpc.Business.Interfaces;

namespace dpc.Web.Controllers
{
	[Authorize]
	public class ManageController : BaseController
	{
		#region Contructors
		
		private IDataManager _dataManager;
		
		public ManageController(IDataManager dataManager)
		{
			_dataManager = dataManager;
		}

		#endregion Contructors

		//
		// GET: /Manage/Index
		public async Task<ActionResult> Index(ManageMessageId? message)
		{
			ViewBag.StatusMessage =
				message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
				: message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
				: message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
				: message == ManageMessageId.Error ? "An error has occurred."
				: message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
				: message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
				: "";

			var userId = User.Identity.GetUserId();
			var model = new IndexViewModel
			{
				HasPassword = HasPassword(),
				PhoneNumber = await UserManager.GetPhoneNumberAsync(userId),
				TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId),
				Logins = await UserManager.GetLoginsAsync(userId),
				BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId)
			};
			return View(model);
		}

		[AllowAnonymous]
		// GET: /Manage/ChangePassword
		public ActionResult ChangePassword()
		{
			if (string.IsNullOrEmpty((string)TempData["title"]))
			{
				ViewBag.Title = "Change Password";
			}
			else
			{
				ViewBag.Title = TempData["Title"].ToString();
			}

			return View(new ChangePasswordViewModel());
		}

		[AllowAnonymous]
		// POST: /Manage/ChangePassword
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			ApplicationUser user;
			if (User.Identity.IsAuthenticated)
			{
				user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
			}
			else
			{
				user = TempData["User"] as ApplicationUser;
				TempData.Keep();
			}

			if (user == null)
			{
				//TODO: Display error and return to view
			}

			IPasswordHasher passwordHasher = new PasswordHasher();
			var maxReusePreviousPasswords = Convert.ToInt32(ConfigurationManager.AppSettings["MaxReusePreviousPasswords"].ToString());

			if (_dataManager.GetPasswordHistory(user.Id)
				.Take(maxReusePreviousPasswords)
				.Where(p => passwordHasher.VerifyHashedPassword(p, model.NewPassword) != PasswordVerificationResult.Failed)
				.Any())
			{
				ModelState.AddModelError("", "Cannot reuse old password. Please try again.");
				return View(model);
			}

			var result = await UserManager.ChangePasswordAsync(user.Id, model.OldPassword, model.NewPassword);
			if (result.Succeeded)
			{
				if (user != null)
				{
					//Add user new password info to database used to compare previous password requirements
					PasswordHistory newPassword = new PasswordHistory();
					newPassword.UserId = user.Id;
					newPassword.PasswordHash = passwordHasher.HashPassword(model.NewPassword);
					_dataManager.InsertPasswordHistory(newPassword);

					await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
				}
				return RedirectToAction("Index", "Home");
			}
			AddErrors(result);
			return View(model);
		}

		#region Helpers
		// Used for XSRF protection when adding external logins
		private const string XsrfKey = "XsrfId";

		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return HttpContext.GetOwinContext().Authentication;
			}
		}

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError("", error);
			}
		}

		private bool HasPassword()
		{
			var user = UserManager.FindById(User.Identity.GetUserId());
			if (user != null)
			{
				return user.PasswordHash != null;
			}
			return false;
		}

		private bool HasPhoneNumber()
		{
			var user = UserManager.FindById(User.Identity.GetUserId());
			if (user != null)
			{
				return user.PhoneNumber != null;
			}
			return false;
		}

		public enum ManageMessageId
		{
			AddPhoneSuccess,
			ChangePasswordSuccess,
			SetTwoFactorSuccess,
			SetPasswordSuccess,
			RemoveLoginSuccess,
			RemovePhoneSuccess,
			Error
		}

		#endregion
	}
}