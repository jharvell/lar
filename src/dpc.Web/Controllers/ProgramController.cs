﻿using AutoMapper;
using dpc.Business.Interfaces;
using dpc.Data;
using dpc.Data.CustomModels;
using dpc.Web.Extensions;
using dpc.Web.ViewModel;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using dpc.Common;

namespace dpc.Web.Controllers
{
	[Authorize]
	[ClaimsAccess(ClaimType = ClaimType.Role, Value = ClaimValue.Sales)]
	public class ProgramController : BaseController
	{
		#region constructors 

		private readonly IProgramManager _programManager;

		public ProgramController(IProgramManager programManager)
		{
			_programManager = programManager;
		}

		#endregion constructors

		// GET: Program
		public ActionResult Index()
		{
			return View();
		}

		// GET: Program/CreateHeader
		public ActionResult CreateHeader()
		{
			var vm = new ProgramHeaderViewModel();

			vm.BrandOwnerList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetBrandOwnerList());
			vm.TransitionTypeList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetTransitionTypeList());
			vm.PalletsList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetYesNoList());
			vm.NewProgramList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetYesNoList());
			vm.FreightTermsList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetFreightTermsList());

			return View(vm);
		}

		// POST: Program/CreateHeader
		[HttpPost]
		public ActionResult CreateHeader(ProgramHeaderViewModel vm, FormCollection collection)
		{
			vm.BrandOwnerList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetBrandOwnerList());
			vm.TransitionTypeList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetTransitionTypeList());
			vm.PalletsList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetYesNoList());
			vm.NewProgramList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetYesNoList());
			vm.FreightTermsList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetFreightTermsList());

			if (collection["Create"] != null)
			{
				if (!ModelState.IsValid)
				{
					return View(vm);
				}

				vm.Status = _programManager.GetProgramStatus(vm.ProgramHeaderId);
				vm.ProductCategoryId = 1;  //TODO: Get ProductCategoryId from Main menu selection (i.e. Baby or Adult)
				var model = Mapper.Map<ProgramHeaderViewModel, ProgramHeader>(vm);
				_programManager.InsertProgramHeader(model);

				//Update ProgramHeader Status
				_programManager.UpdateProgramHeaderStatus(model.ProgramHeaderId);

				return RedirectToAction("Index");
			}

			return View(vm);
		}

		// GET: Program/EditHeader/5
		public ActionResult EditHeader(int id)
		{
			if (!(id > 0))
			{
				throw new ArgumentException(String.Format("{0} is an invalid parameter", id), "id");
			}

			var model = _programManager.GetProgramHeaderById(id);
			var vm = Mapper.Map<ProgramHeader, ProgramHeaderViewModel>(model);

			vm.BrandOwnerList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetBrandOwnerList());
			vm.TransitionTypeList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetTransitionTypeList());
			vm.PalletsList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetYesNoList());
			vm.NewProgramList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetYesNoList());
			vm.FreightTermsList = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetFreightTermsList());
			vm.ProgramLineCount = _programManager.GetProgramLinesByProgramHeaderId(id).Count();

			vm.isProgramLinesExist = false;
			if (vm.ProgramLineCount > 0)
			{
				vm.isProgramLinesExist = true;
			}

			//Define button status
			vm.isProgramSubmitted = _programManager.isProgramSubmitted(id);
			vm.isProgramRejected = _programManager.isProgramRejected(id);
			vm.isProgramApproved = _programManager.isProgramApproved(id);

			return View(vm);
		}

		// POST: Program/EditHeader
		[HttpPost]
		[AllowAnonymous]
		public async Task<ActionResult> EditHeader(ProgramHeaderViewModel vm)
		{
			var programStatus = _programManager.GetProgramStatus(vm.ProgramHeaderId);

			//Submit for Approval...
			var programHeader = _programManager.GetProgramHeaderById(vm.ProgramHeaderId);
			_programManager.UpdateSubmitForApproval(programHeader);

			//TODO: change "Approver-Retail" to use RoleId... also fix claims to use RoleId rather than string
			var programApproverList = Mapper.Map<IEnumerable<ProgramApproverModel>, IEnumerable<ProgramApproverViewModel>>(_programManager.GetProgramApproverList("Approver-" + vm.BusinessUnit));

			//Send email to Approvers
			var programHeaderInfo = _programManager.GetProgramHeaderInfoById(vm.ProgramHeaderId);
			var programApprovalPath = Convert.ToString(ConfigurationManager.AppSettings["ProgramApprovalPath"]);
			var callbackUrl = programApprovalPath + vm.ProgramHeaderId;
			foreach (var approver in programApproverList)
			{
				if (programStatus == EnumProgramStatus.Rejected || programStatus == EnumProgramStatus.Evaluating || programStatus == EnumProgramStatus.ReEvaluating)
				{
					//Send re-approve email if Program was rejected
					await UserManager.SendEmailAsync(approver.UserId, 
						"** TESTING ** - LAR Re-Approve Program Request: " + vm.ProgramHeaderId,
						"Program re-approval requested... " +
						"<br/>Title:       <b>" + programHeaderInfo.Title + "</b>" +
						"<br/>Brand Owner: <b>" + programHeaderInfo.BrandOwnerDescription + "</b>" +
						"<br/>Created on:  <b>" + programHeaderInfo.CreatedOn.ToShortDateString() + "</b> by: <b>" + programHeaderInfo.CreatedBy + "</b>" +
						"<br/>Comments:    <b>" + programHeaderInfo.Comments + "</b>" +
						"<br/><br/>Program was previously rejected and now needs to be re-viewed by clicking <a href=\"" + callbackUrl + "\">here</a>");
				}
				else
				{
					//Send approve email if Program was rejected
					await UserManager.SendEmailAsync(approver.UserId, 
						"** TESTING ** - LAR Approve Program Request: " + vm.ProgramHeaderId,
						"Program approval requested... " +
						"<br/>Title:       <b>" + programHeaderInfo.Title + "</b>" +
						"<br/>Brand Owner: <b>" + programHeaderInfo.BrandOwnerDescription + "</b>" +
						"<br/>Created on:  <b>" + programHeaderInfo.CreatedOn.ToShortDateString() + "</b> by: <b>" + programHeaderInfo.CreatedBy + "</b>" +
						"<br/>Comments:    <b>" + programHeaderInfo.Comments + "</b>" +
						"<br/><br/>Please review Program by clicking <a href=\"" + callbackUrl + "\">here</a>");

					//Insert Approvers into ProgramApproval table (user will update when approving Program)
					var programApproval = new ProgramApproval();
					programApproval.ProgramHeaderId = vm.ProgramHeaderId;
					programApproval.ApproverUserId = approver.UserId;
					_programManager.InsertProgramApproval(programApproval);
				}
			}

			//Update ProgramHeader Status
			_programManager.UpdateProgramHeaderStatus(vm.ProgramHeaderId);

			return RedirectToAction("Index");
		}

		[HttpGet]
		public async Task<ActionResult> GetProgramHeaderByIdAsync(string id)
		{
			var model = await _programManager.GetProgramHeaderByIdAsync(int.Parse(id));
			var vm = model.ToViewModel();

			return PartialView("_ProgramHeaderFinancials", vm);
		}

		// GET: Program/CreateLine
		public ActionResult CreateLine(int id)
		{
			var vm = new ProgramLineExtViewModel();
			vm.ProgramHeader = new ProgramHeaderViewModel();
			vm.ProgramHeader = Mapper.Map<ProgramHeaderViewModel>(_programManager.GetProgramHeaderById(id));

			IEnumerable<Feature> features = _programManager.GetFeaturesByProductCategory(vm.ProgramHeader.ProductCategoryId);

			vm.FeatureList = Mapper.Map<IEnumerable<Feature>, IEnumerable<FeatureViewModel>>(features);

			vm.FeatureOptions = _programManager.GetProgramLinesDataByProgramHeaderId(id);

			//Disable buttons (Create) if Submitted for Approval
			vm.isApprovalSubmitted = _programManager.isProgramSubmitted(id);

			return View(vm);
		}

		// POST: Program/CreateLine
		[HttpPost]
		public ActionResult CreateLine(ProgramLineExtViewModel vm)
		{
			return View(vm);
		}

		// GET: Program/Reports/1
		public void ReportXlsx(int id)
		{
			Telerik.Reporting.Report report = new Report.CostModel(id);

			ExportReportXlsx(report);
		}

		// GET: Program/Approval/1
		public ActionResult Approval(int id)
		{
			var vm = new ProgramApprovalViewModel();
			vm.ProgramHeader = Mapper.Map<ProgramHeaderViewModel>(_programManager.GetProgramHeaderById(id));

			//Get Approvers from Identity
			vm.ApproverList = Mapper.Map<IEnumerable<ProgramApproverModel>, IEnumerable<ProgramApproverViewModel>>(_programManager.GetProgramApproverListByProgramHeaderId("Approver-" + vm.ProgramHeader.BusinessUnit, id));

			//Check if logged in user is an Approver
			foreach (var approver in vm.ApproverList)
			{
				if (User.Identity.GetUserId() == approver.UserId)
				{
					approver.isReadOnly = false;
				}
				else
				{
					approver.isReadOnly = true;
				}
			}

			//Define button status
			vm.ProgramHeader.isProgramSubmitted = _programManager.isProgramSubmitted(id);
			vm.ProgramHeader.isProgramRejected = _programManager.isProgramRejected(id);
			vm.ProgramHeader.isProgramApproved = _programManager.isProgramApproved(id);

			return View(vm);
		}

		// POST: Program/Approval
		[HttpPost]
		public ActionResult Approval(ProgramApprovalViewModel vm)
		{
			//Save approved info
			var programApproval = _programManager.GetProgramApprovalById(vm.ProgramApprovalId);
			programApproval.Comment = vm.Comment;
			programApproval.Approved = vm.Approved;
			_programManager.UpdateProgamApproval(programApproval);

			//Update ProgramHeader Status
			_programManager.UpdateProgramHeaderStatus(vm.ProgramHeader.ProgramHeaderId);

			return RedirectToAction("Index");
		}

		#region Excel Export

		[HttpPost]
		public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
		{
			var fileContents = Convert.FromBase64String(base64);

			return File(fileContents, contentType, fileName);
		}

		#endregion Excel Export

		#region Program Header data

		[AllowAnonymous]
		public ActionResult ProgramHeader_Read([DataSourceRequest] DataSourceRequest request)
		{
			IEnumerable<ProgramHeaderViewModel> vm = Mapper.Map<IEnumerable<ProgramHeaderModel>, IEnumerable<ProgramHeaderViewModel>>(_programManager.GetProgramHeaderInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		public ActionResult GetTransitionTypeList_Read([DataSourceRequest] DataSourceRequest request)
		{

			IEnumerable<SelectViewModel> list = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetTransitionTypeList());

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetPalletsList_Read([DataSourceRequest] DataSourceRequest request)
		{

			IEnumerable<SelectViewModel> list = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetYesNoList());

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetFreightTermsList_Read([DataSourceRequest] DataSourceRequest request)
		{

			IEnumerable<SelectViewModel> list = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetFreightTermsList());

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetNewProgramList_Read([DataSourceRequest] DataSourceRequest request)
		{

			IEnumerable<SelectViewModel> list = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetYesNoList());

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		#endregion Program Header data

		#region Program Line data

		public JsonResult GetFeatureOptionList(int featureId, int productTypeId, int productSizeId)
		{
			IEnumerable<SelectViewModel> list = Mapper.Map<IEnumerable<SelectModel>, IEnumerable<SelectViewModel>>(_programManager.GetFeatureOptionList(featureId, productTypeId, productSizeId));

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		public ActionResult ProgramLine_Read([DataSourceRequest]DataSourceRequest request, int id)
		{
			var dataTable = _programManager.GetProgramLinesDataByProgramHeaderId(id);

			return Json(dataTable.ToDataSourceResult(request));
		}

		#endregion Grid data

	}
}
