﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using dpc.Web.Models;
using System;
using dpc.Business.Interfaces;
using dpc.Business;
using System.Configuration;
using System.Linq;
using dpc.Data;
using Microsoft.Owin.Security;

namespace dpc.Web.Controllers
{
	[Authorize]
	public class AccountController : BaseController
	{
		#region Contructors

		private IDataManager _dataManager;

		public AccountController()
		{
			_dataManager = new DataManager();
		}

		public AccountController(IDataManager dataManager)
		{
			_dataManager = dataManager;
		}

		#endregion Contructors

		//
		// GET: /Account/Login
		[AllowAnonymous]
		public ActionResult Login(string returnUrl)
		{
			ViewBag.ReturnUrl = returnUrl;
			return View();
		}

		//
		// POST: /Account/Login
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			model.UserName = model.UserName.Trim();
			ApplicationUser user = UserManager.FindByName(model.UserName);

			var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: true);

			switch (result)
			{
				case SignInStatus.Success:
					//Clear the access failed count used for lockout (NOTE: doesn't hurt to do even if LockoutEnabled = False)
					await UserManager.ResetAccessFailedCountAsync(user.Id);

					//Require password change after MaxDaysBeforePasswordChange days since last changed
					var maxDaysBeforePasswordChange = Convert.ToInt32(ConfigurationManager.AppSettings["MaxDaysBeforePasswordChange"].ToString());
					if (_dataManager.GetLastSavedPasswordDate(user.Id).AddDays(maxDaysBeforePasswordChange) < DateTime.Now)
					{
						TempData["User"] = user;
						TempData["Title"] = "Change Password - Expire";
						AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
						return RedirectToAction("ChangePassword", "Manage");
					}

					return RedirectToLocal(returnUrl);

				case SignInStatus.LockedOut:
					//Display how much time is left before account is unlocked
					var timeToUnlock = UserManager.GetLockoutEndDate(user.Id) - DateTime.UtcNow;

					if (timeToUnlock > TimeSpan.FromDays(100))
					{
						ModelState.AddModelError("",
							string.Format(
								"Your account has been locked. Please contact Admin for details."));
					}
					else
					{
						ModelState.AddModelError("",
							string.Format(
								"Your account has been locked out due to multiple failed login attempts. Please try again in {0:mm\\:ss} minutes.",
								timeToUnlock));
					}

					return View(model);

				case SignInStatus.RequiresVerification:  //NOT USED
					return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });

				case SignInStatus.Failure:
					//Check if valid user has LockoutEnabled = True
					if (user != null && await UserManager.GetLockoutEnabledAsync(user.Id))
					{
						var DefaultAccountLockoutTimeSpan = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultAccountLockoutTimeSpan"].ToString());
						int accessFailedCount = await UserManager.GetAccessFailedCountAsync(user.Id);

						int attemptsLeft =
							Convert.ToInt32(UserManager.MaxFailedAccessAttemptsBeforeLockout.ToString()) - accessFailedCount;

						ModelState.AddModelError("", string.Format(
							"Invalid credentials. You have {0} more attempt(s) before your account gets locked out for {1} minutes.", attemptsLeft, DefaultAccountLockoutTimeSpan));
					}
					else
					{
						ModelState.AddModelError("", "Invalid credentials. Please try again.");
					}
					return View(model);

				default:
					ModelState.AddModelError("", "Invalid credentials. Please try again.");
					return View(model);
			}
		}

		//
		// GET: /Account/ForgotPassword
		[AllowAnonymous]
		public ActionResult ForgotPassword()
		{
			return View();
		}

		//
		// POST: /Account/ForgotPassword
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
		{
			if (ModelState.IsValid)
			{
				var user = await UserManager.FindByEmailAsync(model.Email);
				if (user == null)
				{
					// Don't reveal that the user does not exist or is not confirmed
					//return View("ForgotPasswordConfirmation");
					ModelState.AddModelError("", "Unable to find email. Please try again.");
					return View(model);
				}

				// For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
				// Send an email with this link
				string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
				var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
				
				//TODO: change subject line for production
				await UserManager.SendEmailAsync(user.Id, "** TESTING ** - LAR Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
				return RedirectToAction("ForgotPasswordConfirmation", "Account");
			}

			// If we got this far, something failed, redisplay form
			return View(model);
		}

		//
		// GET: /Account/ForgotPasswordConfirmation
		[AllowAnonymous]
		public ActionResult ForgotPasswordConfirmation()
		{
			return View();
		}

		//
		// GET: /Account/ResetPassword
		[AllowAnonymous]
		public ActionResult ResetPassword(string code)
		{
			return code == null ? View("Error") : View(new ResetPasswordViewModel());
		}

		//
		// POST: /Account/ResetPassword
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}

			var user = await UserManager.FindByEmailAsync(model.Email);
			if (user == null)
			{
				// Don't reveal that the user does not exist
				//return RedirectToAction("ResetPasswordConfirmation", "Account");
				ModelState.AddModelError("", "Unable to find email. Please try again.");
				return View(model);
			}

			IPasswordHasher passwordHasher = new PasswordHasher();
			var maxReusePreviousPasswords = Convert.ToInt32(ConfigurationManager.AppSettings["MaxReusePreviousPasswords"].ToString());

			if (_dataManager.GetPasswordHistory(user.Id)
				.Take(maxReusePreviousPasswords)
				.Where(p => passwordHasher.VerifyHashedPassword(p, model.Password) != PasswordVerificationResult.Failed)
				.Any())
			{
				ModelState.AddModelError("", "Cannot reuse old password. Please try again.");
				return View(model);
			}

			var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
			if (result.Succeeded)
			{
				//Add user new password info to database used to compare previous password requirements
				PasswordHistory newPassword = new PasswordHistory();
				newPassword.UserId = user.Id;
				newPassword.PasswordHash = passwordHasher.HashPassword(model.Password);
				_dataManager.InsertPasswordHistory(newPassword);

				return RedirectToAction("ResetPasswordConfirmation", "Account");
			}
			AddErrors(result);
			return View(model);
		}

		//
		// GET: /Account/ResetPasswordConfirmation
		[AllowAnonymous]
		public ActionResult ResetPasswordConfirmation()
		{
			return View();
		}

		//
		// POST: /Account/LogOff
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult LogOff()
		{
			AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
			return RedirectToAction("Login", "Account");
		}

		#region Helpers
		// Used for XSRF protection when adding external logins
		private const string XsrfKey = "XsrfId";

		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return HttpContext.GetOwinContext().Authentication;
			}
		}

		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError("", error);
			}
		}

		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			return RedirectToAction("Index", "Home");
		}

		internal class ChallengeResult : HttpUnauthorizedResult
		{
			public ChallengeResult(string provider, string redirectUri)
				: this(provider, redirectUri, null)
			{
			}

			public ChallengeResult(string provider, string redirectUri, string userId)
			{
				LoginProvider = provider;
				RedirectUri = redirectUri;
				UserId = userId;
			}

			public string LoginProvider { get; set; }
			public string RedirectUri { get; set; }
			public string UserId { get; set; }

			public override void ExecuteResult(ControllerContext context)
			{
				var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
				if (UserId != null)
				{
					properties.Dictionary[XsrfKey] = UserId;
				}
				context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
			}
		}
		#endregion
	}
}