﻿using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace dpc.Web.Controllers
{
	[Authorize]
	public class HomeController : BaseController
	{
		[AllowAnonymous]
		public ActionResult Index()
		{
			if (!User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Login", "Account");
			}

			var rolesForUser = UserManager.GetRoles(User.Identity.GetUserId());
			ViewBag.isSales = rolesForUser.Contains("Sales");
			
			return View();
		}

		[ClaimsAccess(ClaimType = "Role", Value = "Admin")]
		public ActionResult AdminPage()
		{
			//TESTING...
			//throw new NotImplementedException();

			//ViewBag.CanShowFinancialSection = ClaimsAuthorization.CheckAccess("Show", "FinancialSection");

			//ViewBag.Message = "...you are an Admin!";

			return View();
		}
	}
}