﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using dpc.Web.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using dpc.Web.ViewModels;
using Microsoft.AspNet.Identity.EntityFramework;
using dpc.Business.Interfaces;
using dpc.Data;
using System.Configuration;
using AutoMapper;

namespace dpc.Web.Controllers
{
	[ClaimsAccess(ClaimType = "Role", Value = "Admin")]
	public class ApplicationUsersController : BaseController
	{
		#region Contructors
		
		private IDataManager _dataManager;

		public ApplicationUsersController(IDataManager dataManager)
		{
			_dataManager = dataManager;
		}

		#endregion Contructors

		// GET: ApplicationUsers
		public ActionResult Index()
		{
			//Get list of users
			var usersList = UserManager.Users.OrderBy(u => u.UserName).ToList();
			IEnumerable<ApplicationUserViewModel> vm = Mapper.Map<IEnumerable<ApplicationUserViewModel>>(usersList);

			////Get list of roles by user
			//IEnumerable<string> userRoles;
			//foreach (var user in usersList)
			//{
			//	userRoles = UserManager.GetRoles(user.Id);
			//}

			//model.RolesList1 = userRoles;

			//applicationUser.RolesList = RoleManager.Roles.ToList().Select(r => new SelectListItem
			//{
			//    Selected = userRoles.Contains(r.Name),
			//    Text = r.Name,
			//    Value = r.Name
			//});

			return View(vm);
		}

		// GET: ApplicationUsers/Create
		public ActionResult Create()
		{
			var vm = new ApplicationUserViewModel();
			vm.PasswordRequiredLength = ConfigurationManager.AppSettings["PasswordRequiredLength"];
			return View(vm);
		}

		// POST: ApplicationUsers/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create(ApplicationUserViewModel applicationUserViewModel)
		{
			if (ModelState.IsValid)
			{
				var user = new ApplicationUser { UserName = applicationUserViewModel.Username.ToLower(), Email = applicationUserViewModel.Email.ToLower(), FirstName = applicationUserViewModel.FirstName, LastName = applicationUserViewModel.LastName, LockoutEnabled = true };
				var result = await UserManager.CreateAsync(user, applicationUserViewModel.NewPassword);

				if (result.Succeeded)
				{
					//Add claims
					var context = HttpContext.GetOwinContext().Get<ApplicationDbContext>();
					var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
					await userManager.AddClaimAsync(user.Id, new System.Security.Claims.Claim("UserFullName", applicationUserViewModel.FirstName + " " + applicationUserViewModel.LastName));

					//Add user password info to database used to compare previous password requirements
					PasswordHistory newPassword = new PasswordHistory();
					newPassword.UserId = user.Id;
					newPassword.PasswordHash = user.PasswordHash;
					_dataManager.InsertPasswordHistory(newPassword);

					return RedirectToAction("Index", "ApplicationUsers");
				}
				AddErrors(result);
			}

			// If we got this far, something failed, redisplay form   
			var vm = new ApplicationUserViewModel();
			vm.PasswordRequiredLength = ConfigurationManager.AppSettings["PasswordRequiredLength"];
			return View(vm);
		}


		public async Task<ActionResult> Edit(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationUser applicationUser = await UserManager.FindByIdAsync(id);
			if (applicationUser == null)
			{
				return HttpNotFound();
			}

			var userRoles = await UserManager.GetRolesAsync(applicationUser.Id);
			applicationUser.RolesList = RoleManager.Roles.OrderBy(x => x.Name).ToList().Select(r => new SelectListItem
			{
				Selected = userRoles.Contains(r.Name),
				Text = r.Name,
				Value = r.Name
			});

			ApplicationUserViewModel model = Mapper.Map<ApplicationUserViewModel>(applicationUser);

			return View(model);
		}

		// POST: ApplicationUsers/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit([Bind(Include = "Id")] ApplicationUser applicationUser, params string[] rolesSelectedOnView)
		{
			if (ModelState.IsValid)
			{
				// If the user is currently stored having the Admin role,
				var rolesCurrentlyPersistedForUser = await UserManager.GetRolesAsync(applicationUser.Id);
				bool isThisUserAnAdmin = rolesCurrentlyPersistedForUser.Contains("Admin");

				// and the user did not have the Admin role checked,
				rolesSelectedOnView = rolesSelectedOnView ?? new string[] { };
				bool isThisUserAdminDeselected = !rolesSelectedOnView.Contains("Admin");

				// and the current stored count of users with the Admin role == 1,
				var role = await RoleManager.FindByNameAsync("Admin");
				bool isOnlyOneUserAnAdmin = role.Users.Count == 1;

				// (populate the roles list in case we have to return to the Edit view)
				applicationUser = await UserManager.FindByIdAsync(applicationUser.Id);
				applicationUser.RolesList = RoleManager.Roles.ToList().Select(x => new SelectListItem()
				{
					Selected = rolesCurrentlyPersistedForUser.Contains(x.Name),
					Text = x.Name,
					Value = x.Name
				});

				// then prevent the removal of the Admin role.
				if (isThisUserAnAdmin && isThisUserAdminDeselected && isOnlyOneUserAnAdmin)
				{
					ModelState.AddModelError("", "At least one user must retain the Admin role; you are attempting to delete the Admin role from the last user who has been assigned to it.");
					return View(applicationUser);
				}

				var result = await UserManager.AddToRolesAsync(
					applicationUser.Id,
					rolesSelectedOnView.Except(rolesCurrentlyPersistedForUser).ToArray());

				if (!result.Succeeded)
				{
					ModelState.AddModelError("", result.Errors.First());
					return View(applicationUser);
				}

				result = await UserManager.RemoveFromRolesAsync(
					applicationUser.Id,
					rolesCurrentlyPersistedForUser.Except(rolesSelectedOnView).ToArray());

				if (!result.Succeeded)
				{
					ModelState.AddModelError("", result.Errors.First());
					return View(applicationUser);
				}

				//Update Claims table with Roles selected
				//TODO: redesign so Roles are removed and Claims are used for Roles
				UpdateClaimsBasedOnRoles(applicationUser, rolesSelectedOnView);

				return RedirectToAction("Index");
			}

			ModelState.AddModelError("", "Something failed.");
			return View(applicationUser);
		}

		public void UpdateClaimsBasedOnRoles(ApplicationUser applicationUser, params string[] rolesSelectedOnView)
		{
			var claims = UserManager.GetClaims(applicationUser.Id);

			foreach (System.Security.Claims.Claim claimItem in claims)
			{
				if (claimItem.Type == "Role")
				{
					UserManager.RemoveClaim(applicationUser.Id, ExtendedClaimsProvider.CreateClaim("Role", claimItem.Value));
				}
			}

			foreach (string roleSelected in rolesSelectedOnView)
			{
				UserManager.AddClaim(applicationUser.Id, ExtendedClaimsProvider.CreateClaim("Role", roleSelected));
			}
		}

		public async Task<ActionResult> LockAccount([Bind(Include = "Id")] string id)
		{
			await UserManager.ResetAccessFailedCountAsync(id);
			await UserManager.SetLockoutEndDateAsync(id, DateTime.UtcNow.AddYears(100));
			return RedirectToAction("Index");
		}


		public async Task<ActionResult> UnlockAccount([Bind(Include = "Id")] string id)
		{
			await UserManager.ResetAccessFailedCountAsync(id);
			await UserManager.SetLockoutEndDateAsync(id, DateTime.UtcNow.AddYears(-1));
			return RedirectToAction("Index");
		}

		////////// REMOVED DELETE USER CAPABILITY - Verify user does not have a dependency (what tables??) before deleting ///////////
		//// GET: ApplicationUsers/Delete/5
		//public ActionResult Delete(string id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	ApplicationUser applicationUser = UserManager.FindById(id);
		//	if (applicationUser == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(applicationUser);
		//}

		//// POST: ApplicationUsers/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public ActionResult DeleteConfirmed(string id)
		//{
		//	ApplicationUser applicationUser = UserManager.FindById(id);

		//	//Check if user is in Admin role
		//	var rolesCurrentlyPersistedForUser = UserManager.GetRoles(applicationUser.Id);
		//	bool isThisUserAnAdmin = rolesCurrentlyPersistedForUser.Contains("Admin");

		//	//Check if user is the only Admin
		//	var role = RoleManager.FindByName("Admin");
		//	bool isOnlyOneUserAnAdmin = role.Users.Count == 1;

		//	if (isThisUserAnAdmin && isOnlyOneUserAnAdmin)
		//	{
		//		ModelState.AddModelError("", "Error: Cannot delete user - this is the last user assigned to the Admin role");
		//		return View(applicationUser);
		//	}
		//	else
		//	{
		//		UserManager.Delete(applicationUser);
		//		return RedirectToAction("Index");
		//	}

		//}

		#region Helpers
		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError("", error);
			}
		}
		#endregion Helpers
	}
}
