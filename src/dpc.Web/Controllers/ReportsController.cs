﻿using System;
using Telerik.Reporting.Services.WebApi;

namespace dpc.Web.Controllers
{
	//NOTE: NOT CURRENTLY BEING USED - USED ONLY WITH REPORTVIEWER
	public class ReportsController : ReportsControllerBase
	{
#pragma warning disable CS0672 // Member overrides obsolete member
		protected override Telerik.Reporting.Cache.Interfaces.ICache CreateCache()
#pragma warning restore CS0672 // Member overrides obsolete member
		{
			return Telerik.Reporting.Services.Engine.CacheFactory.CreateFileCache();
		}

#pragma warning disable CS0672 // Member overrides obsolete member
		protected override Telerik.Reporting.Services.Engine.IReportResolver CreateReportResolver()
#pragma warning restore CS0672 // Member overrides obsolete member
		{
			return new CustomReportResolver();
		}
	}

	class CustomReportResolver : Telerik.Reporting.Services.Engine.IReportResolver
	{
		public Telerik.Reporting.ReportSource Resolve(string reportId)
		{
			Telerik.Reporting.Report report = new Report.CostModel(Int32.Parse(reportId));

			var irs = new Telerik.Reporting.InstanceReportSource();
			irs.ReportDocument = report;

			return irs;
		}
	}
}
