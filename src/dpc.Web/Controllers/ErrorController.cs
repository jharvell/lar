﻿using System.Web.Mvc;

namespace dpc.Web.Controllers
{
	public class ErrorController : BaseController
	{
		// GET: Error
		public ActionResult Index()
		{
			return View("Error");
		}

		public ActionResult Error()
		{
			return View();
		}

		public ActionResult ErrorNotFound()
		{
			return View();
		}

		public ActionResult ErrorNotAuthorized()
		{
			return View();
		}
	}
}