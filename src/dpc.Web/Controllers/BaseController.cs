﻿using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Web.Mvc;
using Telerik.Reporting.Processing;

namespace dpc.Web.Controllers
{
	public abstract class BaseController : Controller
	{
		private ApplicationUserManager _userManager;
		private ApplicationRoleManager _roleManager;
		private ApplicationSignInManager _signInManager;

		public ApplicationUserManager UserManager
		{
			get
			{
				//Get directly from database rather than cache
				return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
			}
		}

		public ApplicationRoleManager RoleManager
		{
			get
			{
				return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
			}
		}
		
		public ApplicationSignInManager SignInManager
		{
			get
			{
				return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (_userManager != null)
				{
					_userManager.Dispose();
					_userManager = null;
				}

				if (_roleManager != null)
				{
					_roleManager.Dispose();
					_roleManager = null;
				}

				if (_signInManager != null)
				{
					_signInManager.Dispose();
					_signInManager = null;
				}
			}

			base.Dispose(disposing);
		}

		public void ExportReportXlsx(Telerik.Reporting.Report reportToExport)
		{
			Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
			instanceReportSource.ReportDocument = reportToExport;
			ReportProcessor reportProcessor = new ReportProcessor();
			RenderingResult result = reportProcessor.RenderReport("XLSX", instanceReportSource, null);

			string fileName = result.DocumentName + "." + result.Extension;

			Response.Clear();
			Response.ContentType = result.MimeType;
			Response.AddHeader("Content-Disposition", $"attachment;filename={fileName}");
			Response.OutputStream.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
			Response.Flush();
			Response.End();
		}

	}
}