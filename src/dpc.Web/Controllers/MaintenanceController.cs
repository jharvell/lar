﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using dpc.Data;
using dpc.Business.Interfaces;
using dpc.Data.CustomModels;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using dpc.Web.ViewModel;
using System;
using dpc.Common;

namespace dpc.Web.Controllers
{
	[Authorize]
	[ClaimsAccess(ClaimType = ClaimType.Role, Value = ClaimValue.Maintenance)]
	//[ClaimsAuthorize(ActionKey.Access, ResourceKey.ViewMaintenance)]
	public class MaintenanceController : BaseController
	{
		private IMaintenanceManager _maintenanceManager;

		#region constructors  

		public MaintenanceController(IMaintenanceManager maintenanceManager)
		{
			_maintenanceManager = maintenanceManager;
		}
		#endregion constructors

		// GET: Maintenance
		public ActionResult Index()
		{
			//throw new ApplicationException("testing elmah");
			return View();
		}

		#region Brand

		// GET: Brand
		public ActionResult Brand()
		{
			return View();
		}

		public ActionResult Brand_Read([DataSourceRequest]DataSourceRequest request)
		{
			IEnumerable<BrandViewModel> vm = new BrandViewModel[] { };

			try
			{
				vm = Mapper.Map<IEnumerable<BrandModel>, IEnumerable<BrandViewModel>>(_maintenanceManager.GetBrandInfo());
			}
			catch (Exception ex)
			{
				//TODO... Add try/catch to Controllers
				//var msg = "added additional text/variables here";
				//Elmah.ErrorSignal.FromCurrentContext().Raise(new Elmah.ApplicationException(msg, ex));
				Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
			}

			return Json(vm.ToDataSourceResult(request));
		}

		public JsonResult GetBrandOwnerList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetBrandOwnerList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		[AllowAnonymous]
		public JsonResult GetBrandListByBrandOwnerId(int brandOwnerId)
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetBrandListByBrandOwnerId(brandOwnerId);

			return Json(list, JsonRequestBehavior.AllowGet);
		}
		#endregion Brand

		#region BrandOwner

		// GET: BrandOwner
		public ActionResult BrandOwner()
		{
			return View();
		}

		public ActionResult BrandOwner_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<BrandOwner>, IEnumerable<BrandOwnerViewModel>>(_maintenanceManager.GetBrandOwner());

			return Json(vm.ToDataSourceResult(request));
		}

		#endregion BrandOwner 

		#region ContainerType

		// GET: ContainerType
		public ActionResult ContainerType()
		{
			return View();
		}

		public ActionResult ContainerType_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<ContainerType>, IEnumerable<ContainerTypeViewModel>>(_maintenanceManager.GetContainerType());

			return Json(vm.ToDataSourceResult(request));
		}

		public JsonResult GetContainerTypeList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetContainerTypeList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		#endregion ContainerType 

		#region Feature

		// GET: Feature
		public ActionResult Feature()
		{
			return View();
		}

		public ActionResult Feature_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<FeatureModel>, IEnumerable<FeatureViewModel>>(_maintenanceManager.GetFeatureInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		[AllowAnonymous]
		public JsonResult GetFeatureList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetFeatureList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		[AllowAnonymous]
		public JsonResult GetFeatureListByProductCategory(int productCategoryId)
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetFeatureListByProductCategory(productCategoryId);

			return Json(list, JsonRequestBehavior.AllowGet);
		}
		#endregion Feature

		#region FeatureToOption

		// GET: FeatureToOption
		public ActionResult FeatureToOption()
		{
			return View();
		}

		public ActionResult FeatureToOption_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<FeatureToOptionModel>, IEnumerable<FeatureToOptionViewModel>>(_maintenanceManager.GetFeatureToOptionInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		[AllowAnonymous]
		public JsonResult GetFeatureToOptionList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetFeatureToOptionList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		[AllowAnonymous]
		public JsonResult GetFeatureToOptionListByFeature(int featureId, int productTypeId)
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetFeatureToOptionListByFeature(featureId, productTypeId);

			return Json(list, JsonRequestBehavior.AllowGet);
		}
		#endregion FeatureToOption

		#region MfgCost

		// GET: MfgCost
		public ActionResult MfgCost()
		{
			return View();
		}

		public ActionResult MfgCost_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<MfgCostModel>, IEnumerable<MfgCostViewModel>>(_maintenanceManager.GetMfgCostInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		#endregion MfgCost

		#region PackagingCost

		// GET: PackagingCost
		public ActionResult PackagingCost()
		{
			return View();
		}

		public ActionResult PackagingCost_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<PackagingCostModel>, IEnumerable<PackagingCostViewModel>>(_maintenanceManager.GetPackagingCostInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		#endregion PackagingCost 

		#region PackagingType

		// GET: PackagingType
		public ActionResult PackagingType()
		{
			return View();
		}

		public ActionResult PackagingType_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<PackagingTypeModel>, IEnumerable<PackagingTypeViewModel>>(_maintenanceManager.GetPackagingTypeInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		[AllowAnonymous]
		public JsonResult GetPackagingTypeList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetPackagingTypeList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		[AllowAnonymous]
		public JsonResult GetPackagingTypeListByContainerType(int containerTypeId)
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetPackagingTypeListByContainerType(containerTypeId);

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		//TODO...convert this and the next JsonResult into one function (passing containerTypeId to pick Bag or Box)
		[AllowAnonymous]
		public JsonResult GetPackagingTypeListByContainerTypeProductSize(int containerTypeId, int productSizeId)
		{
			//containerTypeId -> 1=Bag 2=Box
			IEnumerable<SelectModel> list = _maintenanceManager.GetPackagingTypeListByContainerTypeProductSize(containerTypeId, productSizeId);

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		#endregion PackagingType

		#region PackStyle

		// GET: PackStyle
		public ActionResult PackStyle()
		{
			return View();
		}

		public ActionResult PackStyle_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<PackStyleModel>, IEnumerable<PackStyleViewModel>>(_maintenanceManager.GetPackStyleInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		[AllowAnonymous]
		public JsonResult GetPackStyleListByProductType(int productTypeId)
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetPackStyleListByProductType(productTypeId);

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		#endregion PackStyle 

		#region ProductCategory

		// GET: ProductCategory
		public ActionResult ProductCategory()
		{
			return View();
		}

		public ActionResult ProductCategory_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<ProductCategory>, IEnumerable<ProductCategoryViewModel>>(_maintenanceManager.GetProductCategory());

			return Json(vm.ToDataSourceResult(request));
		}

		public JsonResult GetProductCategoryList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetProductCategoryList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		#endregion ProductCategory 

		#region ProductSize

		// GET: ProductSize
		public ActionResult ProductSize()
		{
			return View();
		}

		public ActionResult ProductSize_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<ProductSizeModel>, IEnumerable<ProductSizeViewModel>>(_maintenanceManager.GetProductSizeInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		[AllowAnonymous]
		public JsonResult GetProductSizeList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetProductSizeList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		[AllowAnonymous]
		public JsonResult GetProductSizeListByProductType(int productTypeId)
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetProductSizeListByProductType(productTypeId);

			return Json(list, JsonRequestBehavior.AllowGet);
		}
		#endregion ProductSize

		#region ProductSizeToFeatureToOptionCost

		// GET: ProductSizeToFeatureToOptionCost
		public ActionResult ProductSizeToFeatureToOptionCost()
		{
			return View();
		}

		public ActionResult ProductSizeToFeatureToOptionCost_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<ProductSizeToFeatureToOptionCostModel>, IEnumerable<ProductSizeToFeatureToOptionCostViewModel>>(_maintenanceManager.GetProductSizeToFeatureToOptionCostInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		//[AllowAnonymous]
		//public JsonResult GetProductSizeToFeatureToOptionCostListByProductType(int productTypeId)
		//{
		//	IEnumerable<SelectModel> list = _maintenanceManager.GetPackStyleListByProductType(productTypeId);

		//	return Json(list, JsonRequestBehavior.AllowGet);
		//}

		#endregion ProductSizeToFeatureToOptionCost 

		#region ProductType

		// GET: ProductType
		public ActionResult ProductType()
		{
			return View();
		}

		public ActionResult ProductType_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<ProductTypeModel>, IEnumerable<ProductTypeViewModel>>(_maintenanceManager.GetProductTypeInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		[AllowAnonymous]
		public JsonResult GetProductTypeList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetProductTypeList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		[AllowAnonymous]
		public JsonResult GetProductTypeListByProductCategory(int productCategoryId)
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetProductTypeListByProductCategory(productCategoryId);

			return Json(list, JsonRequestBehavior.AllowGet);
		}
		#endregion ProductType

		#region QuantityRange

		// GET: QuantityRange
		public ActionResult QuantityRange()
		{
			return View();
		}

		public ActionResult QuantityRange_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<QuantityRangeModel>, IEnumerable<QuantityRangeViewModel>>(_maintenanceManager.GetQuantityRangeInfo());

			return Json(vm.ToDataSourceResult(request));
		}

		[AllowAnonymous]
		public JsonResult GetQuantityRangeList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetQuantityRangeList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		[AllowAnonymous]
		public JsonResult GetQuantityRangeListByContainerType(int containerTypeId)
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetQuantityRangeListByContainerType(containerTypeId);

			return Json(list, JsonRequestBehavior.AllowGet);
		}
		#endregion QuantityRange

		#region Variant

		// GET: Variant
		public ActionResult Variant()
		{
			return View();
		}

		public ActionResult Variant_Read([DataSourceRequest]DataSourceRequest request)
		{
			var vm = Mapper.Map<IEnumerable<Variant>, IEnumerable<VariantViewModel>>(_maintenanceManager.GetVariant());

			return Json(vm.ToDataSourceResult(request));
		}

		public JsonResult GetVariantList()
		{
			IEnumerable<SelectModel> list = _maintenanceManager.GetVariantList();

			return Json(list, JsonRequestBehavior.AllowGet);
		}

		#endregion Variant 
	}
}
