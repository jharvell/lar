﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using dpc.Business;
using dpc.Web.Models;
using dpc.Web.ViewModels;

namespace dpc.Web.Controllers
{
	[ClaimsAccess(ClaimType = "Role", Value = "Admin")]
	public class ApplicationRolesController : BaseController
	{
		private DataManager _dataManager;

		public ApplicationRolesController()
		{
		}
		
		public ApplicationRolesController(DataManager dataManager)
		{
			_dataManager = dataManager;
		}

		// GET: ApplicationRoles
		public async Task<ActionResult> Index()
		{
			return View(await RoleManager.Roles.OrderBy(x => x.Name).ToListAsync());
		}

		// GET: ApplicationRoles/Details/5
		public async Task<ActionResult> Details(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationRole applicationRole = await RoleManager.FindByIdAsync(id);
			if (applicationRole == null)
			{
				return HttpNotFound();
			}
			return View(applicationRole);
		}

		// GET: ApplicationRoles/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: ApplicationRoles/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Create([Bind(Include = "Name")] ApplicationRoleViewModel applicationRoleViewModel)
		{
			if (ModelState.IsValid)
			{
				ApplicationRole applicationRole = new ApplicationRole { Name = applicationRoleViewModel.Name };

				var roleResult = await RoleManager.CreateAsync(applicationRole);
				if (!roleResult.Succeeded)
				{
					ModelState.AddModelError("", roleResult.Errors.First());
					return View();
				}

				return RedirectToAction("Index");
			}

			return View();
		}

		// GET: ApplicationRoles/Edit/5
		public async Task<ActionResult> Edit(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationRole applicationRole = await RoleManager.FindByIdAsync(id);
			if (applicationRole == null)
			{
				return HttpNotFound();
			}

			ApplicationRoleViewModel applicationRoleViewModel = new ApplicationRoleViewModel
			{
				Id = applicationRole.Id,
				Name = applicationRole.Name
			};

			return View(applicationRoleViewModel);
		}

		// POST: ApplicationRoles/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Edit([Bind(Include = "Id,Name")] ApplicationRoleViewModel applicationRoleViewModel)
		{
			if (ModelState.IsValid)
			{
				ApplicationRole applicationRole = await RoleManager.FindByIdAsync(applicationRoleViewModel.Id);
				string originalName = applicationRole.Name;

				if (originalName == "Admin" && applicationRoleViewModel.Name != "Admin")
				{
					ModelState.AddModelError("", "You cannot change the name of the Admin role.");
					return View(applicationRoleViewModel);
				}

				if (originalName != "Admin" && applicationRoleViewModel.Name == "Admin")
				{
					ModelState.AddModelError("", "You cannot change the name of a role to Admin.");
					return View(applicationRoleViewModel);
				}

				applicationRole.Name = applicationRoleViewModel.Name;
				await RoleManager.UpdateAsync(applicationRole);

				return RedirectToAction("Index");
			}
			return View(applicationRoleViewModel);
		}

		// GET: ApplicationRoles/Delete/5
		public async Task<ActionResult> Delete(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationRole applicationRole = await RoleManager.FindByIdAsync(id);
			if (applicationRole == null)
			{
				return HttpNotFound();
			}
			return View(applicationRole);
		}

		// POST: ApplicationRoles/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> DeleteConfirmed(string id)
		{
			ApplicationRole applicationRole = await RoleManager.FindByIdAsync(id);

			if (applicationRole.Name.ToUpper() == "ADMIN")
			{
				ModelState.AddModelError("", "You cannot delete the Admin role.");
				return View(applicationRole);
			}

			//Verify user does not have a Claim before deleting
			bool isThisRoleUsed = _dataManager.isRoleInClaim(applicationRole.Name);

			if (isThisRoleUsed)
			{
				ModelState.AddModelError("", "Error: Cannot delete role - at least one user has this role");
				return View(applicationRole);
			}
			else
			{
				await RoleManager.DeleteAsync(applicationRole);
				return RedirectToAction("Index");
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				RoleManager.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
