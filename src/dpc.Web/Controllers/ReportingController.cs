﻿using dpc.Common;
using System.Web.Mvc;
using dpc.Web.ViewModel;
using Thinktecture.IdentityModel.Authorization;

namespace dpc.Web.Controllers
{
	[Authorize]
	public class ReportingController : BaseController
	{
		public ActionResult Index()
		{
			if (!ClaimsAuthorization.CheckAccess(ActionKey.Access, ResourceKey.ViewReporting))
			{
				return RedirectToAction($"ErrorNotAuthorized", $"Error");
			}

			var vm = new ProgramHeaderViewModel
			{
				ShowLRRdetailReport = ClaimsAuthorization.CheckAccess(ActionKey.Show, ResourceKey.ReportLRRdetails),
				ShowLRRfinancialReport = ClaimsAuthorization.CheckAccess(ActionKey.Show, ResourceKey.ReportLRRfinancials),
				ShowCalculationsReport = ClaimsAuthorization.CheckAccess(ActionKey.Show, ResourceKey.ReportCalculations)
			};

			return View(vm);
		}

		[ClaimsAccess(ClaimType = ClaimType.Role, Value = ClaimValue.Reporting_LRRdetails)]
		public void LrrdetailsXlsx(int id)
		{
			Telerik.Reporting.Report report = new Report.CostModelLRRdetails(id);

			ExportReportXlsx(report);
		}

		[ClaimsAccess(ClaimType = ClaimType.Role, Value = ClaimValue.Reporting_LRRfinancials)]
		public void LrrfinancialsXlsx(int id)
		{
			Telerik.Reporting.Report report = new Report.CostModelLRRfinancials(id);

			ExportReportXlsx(report);
		}

		[ClaimsAccess(ClaimType = ClaimType.Role, Value = ClaimValue.Reporting_Calculations)]
		public void CalculationsXlsx(int id)
		{
			Telerik.Reporting.Report report = new Report.Calculations(id);

			ExportReportXlsx(report);
		}
	}
}