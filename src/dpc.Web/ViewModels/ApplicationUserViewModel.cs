﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace dpc.Web.ViewModels
{
	public class ApplicationUserViewModel
	{
		public string Id { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a first name.")]
		[Display(Name = "First Name")]
		public string FirstName { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "You must enter a last name.")]
		[Display(Name = "Last Name")]
		public string LastName { get; set; }

		[Required]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		public string Name
		{
			get { return FirstName + " " + LastName; }
		}

		[Required]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 3)]
		public string Username { get; set; }
		
		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "New password")]
		public string NewPassword { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Confirm new password")]
		[Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
		public string ConfirmPassword { get; set; }

		private string _passwordRequiredLength;
		public string PasswordRequiredLength
		{
			get { return _passwordRequiredLength; }
			set { _passwordRequiredLength = value; }
		}

		public bool LockoutEnabled { get; set; }

		public DateTime LockoutEndDateUtc { get; set; }

		[Display(Name = "Failed Attempts")]
		private int _accessFailedCount;
		public int AccessFailedCount
		{
			get
			{
				//If user is temporarily locked out, display AccessFailedCount = MaxFailedAccessAttemptsBeforeLockout
				//(Workaround due to PasswordSignInAsync resets AccessFailedCount = 0 but adds mintues to PasswordSignInAsync when user is locked out for max failed logins)
				if (LockoutEndDateUtc != null && 
				    (DateTime.UtcNow < LockoutEndDateUtc && DateTime.UtcNow.AddYears(1) > LockoutEndDateUtc))
				{
					_accessFailedCount = Convert.ToInt32(ConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"].ToString());
				}

				return _accessFailedCount;
			}
			set { _accessFailedCount = value; }
		}

		public string LockedOut
		{
			get
			{
				//Use LockoutEndDateUtc to determine status of user
				var result = string.Empty;
				if (LockoutEndDateUtc == null || (DateTime.UtcNow > LockoutEndDateUtc))
				{
					result = "No";
				}
				else if (DateTime.UtcNow.AddYears(1) < LockoutEndDateUtc)
				{
					result = "Inactive";
				}
				else
				{
					result = "Temporary";
				}

				return result;
			}
		}

		//public IEnumerable<string> RolesList1 { get; set; }
		public IEnumerable<System.Web.Mvc.SelectListItem> RolesList { get; set; }

		public int MaxFailedAccessAttemptsBeforeLockout
		{
			get { return Convert.ToInt32(ConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"].ToString()); }
		}

	}
}